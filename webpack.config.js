/**
 * Created by vetal on 17.04.17.
 */
const webpack = require('webpack');
const path = require('path');
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const autoprefixer = require('autoprefixer');
module.exports = {
    entry: {
        router: './frontend/router.js',
        common: './frontend/common.js'
    },
    output: {
        path: path.resolve(__dirname, 'dist'),
        publicPath: '/wp-content/themes/html5blank-stable/dist/',
        filename: '[name].js',
        chunkFilename: "[id].[name].js"
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /(node_modules|bower_components)/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: [['es2015', {modules: false}]],
                        plugins: ['syntax-dynamic-import']
                    }
                }
            },
            {
                test: /\.less$/,
                use: ExtractTextPlugin.extract({
                    fallback: "style-loader",
                    use: ["css-loader", "postcss-loader", "less-loader"]
                })
            }
        ]
    },
    plugins: [
        new ExtractTextPlugin({
            filename: 'imageBox.css',
            allChunks: true
        }),
        new webpack.LoaderOptionsPlugin({
            options: {
                postcss: [
                    autoprefixer()
                ]
            }
        }),
        new webpack.optimize.CommonsChunkPlugin({
            name: 'common',
            minChunks: 3
        })
    ],
    devtool: 'eval',
    devServer: {
        contentBase: './',
        compress: false,
        port: 9000,
        hot: true
    },
    watch: true
};