/**
 * Created by vetal on 17.04.17.
 */

// classWrapper - класс родителя
// классы для блоков старого, нового контентов, классы активных переходов
// Таймаут для вылета нового контента
// Колбек перед тем как начать, коллбек как только пришли данные, коллбек как все готово

//TODO: onpopstate - event.state, document.location
export default class PageLoader {

    constructor(classWrapper, classOldContent, classNewContent, classSlide, classSlidePrev,
                timeoutSlide,
                callbackBeforeSend, callbackSend, onComplete) {
        this.url = '';
        this.action = '';
        this.title = '';
        this.classWrapper = classWrapper;
        this.classOldContent = classOldContent;
        this.classNewContent = classNewContent;
        this.classSlide = classSlide;
        this.classSlidePrev = classSlidePrev;

        this.callbackBeforeSend = callbackBeforeSend;
        this.callbackSend = callbackSend;
        this.onComplete = onComplete;

        this.$ajaxPaste = '';
        this.$newContent = '';
        this.$pasteHtml = '';

        this.timeout = timeoutSlide || 1000;
    }
    // @url: ссылка, откуда нужно взять контент;
    // @action: действие prev или next;
    // возвращается Title страницы
    getPage(url = location.href, action = 'next') {
        let self = this;
        this.url = url;
        this.action = action;
        let prev = this.isPrev(action);

        if(location.pathname === url) {
            location.reload();
            return false;
        }
        $.ajax({
            type: "POST",
            data: {
                ajax: true
            },
            url: url,
            dataType: 'html',
            beforeSend: () => { self.beforeSend(prev) },
            error: (e1) => { self.error(e1) },
            success: (cont) => { self.contentLoader(cont) }
        });

    }
    beforeSend(prev) {
        let self = this;
        if(prev) $(self.classNewContent).addClass(self.classSlidePrev);
        if($(window).scrollTop() > screen.height / 3) $('html, body').animate({scrollTop: 0}, 350, 'swing');
        if(self.isFunction(self.callbackBeforeSend)) self.callbackBeforeSend();
    }
    error(err) {
        this.title = 'Ajax запрос не выполнен: ' + err;
    }
    contentLoader(cont) {
        let self = this;
        self.title = $(cont).filter('title').html();
        self.$newContent = $('.' + self.classNewContent);
        self.$ajaxPaste = $('.' + self.classOldContent);
        self.$pasteHtml = $(cont).filter('.' + self.classWrapper).find('.' + self.classOldContent).html();
        self.$newContent.html(self.$pasteHtml);

        self.slideOldPage();
        self.slideNewPage();

        if(self.isFunction(self.callbackSend)) self.callbackSend(self.url);
        history.pushState(null, this.title, this.url);
        return 'Успешно';
    }
    slideOldPage() {
        let self = this;
        let prev = this.isPrev(this.action);
        if(prev) {
            self.$ajaxPaste.addClass(self.classSlidePrev);
            self.$newContent.addClass(self.classSlide)
                .removeClass(self.classSlidePrev);
        } else {
            self.$ajaxPaste.addClass(self.classSlide);
            self.$newContent.addClass(self.classSlide);
        }
    }
    slideNewPage() {
        let self = this;
        let prev = this.isPrev(this.action);
        let toggle = this.classNewContent + ' ' + this.classOldContent;
        setTimeout(function() {
            self.$ajaxPaste.html('');
            if(prev)
                self.$ajaxPaste.removeClass(self.classSlidePrev)
                    .toggleClass(toggle);
            else
                self.$ajaxPaste.removeClass(self.classSlide)
                    .toggleClass(toggle);

            self.$newContent.removeClass(self.classSlide)
                .toggleClass(toggle);
            if(self.isFunction(self.onComplete)) self.onComplete();
        }, self.timeout);
        //if(self.isFunction(self.callbackSend)) self.callbackSend(true);
    }
    isPrev(action) {
        return action === 'prev';
    }
    isFunction(func) {
        return typeof(func) === 'function';
    }
}