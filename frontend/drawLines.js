/**
 * Created by vetal on 17.04.17.
 */

window.requestAnimFrame = (function() {
    return  window.requestAnimationFrame       ||
        window.webkitRequestAnimationFrame ||
        window.mozRequestAnimationFrame    ||
        function(callback) {
            window.setTimeout(callback, 1000 / 60);
        };
})();

export default function CanvaLine(canvas) {
    this.points = {};
    this.pointsAnimate = {};
    this.style = {};
    this.canvas = document.getElementById(canvas) || document.querySelector('.' + canvas);
    this.context = this.canvas.getContext('2d');
    this.runtime = 0;
    this.t = 1;

    this.key = 0;
    this.stop = false;
}
CanvaLine.prototype = {
    setProp: function(style, size, points) {
        this.style = style;
        this.canvas.width = size.width;
        this.canvas.height = size.height;
        this.context.lineWidth = style.strokeWidth;
        this.context.strokeStyle = style.stroke;
        this.context.shadowColor = style.shadowColor;
        this.context.shadowBlur = style.shadowBlur;
        this.points = points;
        this.pointsAnimate = this.calcWaypoint(points);
    },
    drawStaticLine: function(newLine) {
        var points = this.points;
        var ctx = this.context;
        this.clear();
        ctx.beginPath();
        ctx.moveTo(points[0].x, points[0].y);
        $.each(points, function(key, val) {
            if(key == 0) return true;
            ctx.lineTo(val.x, val.y);
        });

        if(newLine !== undefined) {
            $.each(newLine, function(keyLine, line) {
                ctx.moveTo(line[0].x, line[0].y);
                $.each(line, function(key, val) {
                    if(key == 0) return true;
                    ctx.lineTo(val.x, val.y);
                });
            });
        }

        ctx.stroke();
    },
    drawAnimationLine: function() {
        var self = this;
        //var time = Date.now();
        //console.log(time);
        var points = this.points;

        var oldPoint = {};
        var differens = '';
        $.each(points, function(key, val) {
            if(key == 0) {
                oldPoint = val;
                return true;
            }
            if(oldPoint.x != val.x) differens = 'x';
            if(oldPoint.y != val.y) differens = 'y';
            if(oldPoint.y != val.y && oldPoint.x != val.x) differens = 'xy';
            self.setAnimFrame(0, oldPoint, val, differens, 100);
            oldPoint = val;
        });
    },
    setAnimFrame: function(now, startPoints, goPoints, differens, duration) {
        var self = this;
        var ctx = this.context;
        var last = now || Date.now();
        var speed = now % 100000 / 100;

        console.log(this.runtime);

        if(this.runtime > duration) console.log('this.runtime > duration');

        if(differens == 'x') {
            if(startPoints.x + speed >= goPoints.x) return true;
        }
        if(differens == 'y') {
            if(startPoints.y + speed >= goPoints.y) return true;
        }
        if(differens == 'xy') {
            if(startPoints.x + speed >= goPoints.x && startPoints.y + speed >= goPoints.y) return true;
        }


        ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);

        ctx.beginPath();
        ctx.moveTo(startPoints.x, startPoints.y);
        ctx.lineTo(startPoints.x, startPoints.y + speed);
        ctx.stroke();

        requestAnimFrame(function(now) {
            now = now || Date.now();
            self.runtime += last - now;
            self.setAnimFrame(now, startPoints, goPoints, differens, duration);
        });
    },
    animation: function(newLine) {
        var self = this;
        if(self.stop) return false;
        if(this.t < this.pointsAnimate.length - 1) {
            requestAnimationFrame(function() {
                self.animation(newLine);
            });
        }

        // draw a line segment from the last waypoint
        // to the current waypoint
        this.context.beginPath();
        this.context.moveTo(this.pointsAnimate[this.t-1].x, this.pointsAnimate[this.t-1].y);
        this.context.lineTo(this.pointsAnimate[this.t].x, this.pointsAnimate[this.t].y);
        this.context.stroke();
        // increment "t" to get the next waypoint
        this.t++;

        if(this.t > this.pointsAnimate.length - 1) {
            if(newLine !== undefined && (self.key < newLine.length)) {
                self.pointsAnimate = self.calcWaypoint(newLine[self.key]);
                self.t = 1;
                self.animation(newLine);
                self.key++;
            }
            return true;
        }
    },
    calcWaypoint: function (points) {
        var waypoints = [];
        for(var i=1;i<points.length;i++){
            var pt0=points[i-1];
            var pt1=points[i];
            var dx=pt1.x-pt0.x;
            var dy=pt1.y-pt0.y;
            for(var j=0;j<16;j++){
                var x=pt0.x+dx*j/15;
                var y=pt0.y+dy*j/15;
                waypoints.push({x:x,y:y});
            }
        }
        return(waypoints);
    },
    clear: function() {
        this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
    },
    stopDraw: function() {
        this.stop = true;
    }
};