/**
 * Created by vetal on 17.04.17.
 */

export default class SliderReviews {
    constructor($slider) {
        this.$slider = $slider;
        this.$current = this.$slider.find('.center');
    }
    init() {
        this.$current = this.$slider.find('.center');
    }
    nextSlide() {
        var $nextSlide = this.$current.next(),
            $prevSlide = this.$current.prev();
        var $goingNext = $nextSlide.next(),
            $goingPrev = $prevSlide.prev();
        if($goingPrev.prev().length < 1) {
            var $item = this.$slider.find('.item');
            var $endSlide = $item.eq($item.length - 1);
            $endSlide.detach()
                .prependTo(this.$slider)
                .toggleClass('left right toRight toLeft');
        }
        $nextSlide.toggleClass('visible hidden toRight');
        this.$current.toggleClass('center right');
        $prevSlide.toggleClass('left center');
        $goingPrev.toggleClass('visible hidden toLeft');
        this.init();
    }
    prevSlide() {
        var $nextSlide = this.$current.next(),
            $prevSlide = this.$current.prev();
        var $goingNext = $nextSlide.next(),
            $goingPrev = $prevSlide.prev();
        if($goingNext.next().length < 1) {
            var $firstSlide = this.$slider.find('.item').eq(0);
            $firstSlide.detach()
                .appendTo(this.$slider)
                .toggleClass('left right toRight toLeft');
        }
        $prevSlide.toggleClass('visible hidden toLeft');
        this.$current.toggleClass('center left');
        $nextSlide.toggleClass('right center');
        $goingNext.toggleClass('visible hidden toRight');
        this.init();
    }
}