/**
 * Created by vetal on 19.04.17.
 */

import {refreshAnimation, TIMEOUT_PAGE} from './common';
import {drawLineAction} from './draws';

export function init() {
    $(window).off('scroll');
    setTimeout(() => {
        refreshAnimation();
        drawLineAction(0);
    }, TIMEOUT_PAGE);

    if(location.pathname === '/portfolio/') {
        let $project = $('.portfolios');
        let fullText = [],
            smallText = [];
        $project.each(function(key, val) {
            fullText[key] = {};
            smallText[key] = {};
            let $shortText = $(val).find('.short_text');
            let fullText_short = $shortText.text();
            fullText[key].text = fullText_short;
            if(fullText_short.length > 40) smallText[key].text = fullText_short.slice(0, 40) + ' ...';
            else smallText[key].text = fullText_short;
            fullText[key].height = $shortText.height();
            $shortText.text(smallText[key].text);
            smallText[key].height = $shortText.height();
            $shortText.css('max-height', smallText[key].height);
        });

        $project.on('mouseenter', function () {
            expandCollapse(true, $(this));
        });
        $project.on('mouseleave', function() {
            expandCollapse(false, $(this));
        });

        function expandCollapse(expand = true, thisItem) {
            let $shortText = $(thisItem).find('.short_text'),
                thisFullText = expand ? fullText[$(thisItem).index()] : smallText[$(thisItem).index()],
                text = thisFullText.text;
            $shortText.css('max-height', thisFullText.height);
            if(expand) $shortText.text(text);
            else setTimeout(() => {
                $shortText.text(text);
            }, 250);
        }
    }
}
if(location.pathname === '/') $(window).off('scroll'); else init();