/**
 * Created by vetal on 17.04.17.
 */
import {refreshAnimation, TIMEOUT_PAGE} from './common';
import {drawLineAction, drawLineContacts, drawLinePartners} from './draws';
import './owl.carousel.min';
export function init() {
    $(window).off('scroll');
    ymaps.ready(initMap);
    setTimeout(() => {
        refreshAnimation();
        drawLineAction(0);
        drawLineContacts(0);
        drawLinePartners(800);
    }, TIMEOUT_PAGE);
    let $partners = $('.partners');
    let owl = $partners.owlCarousel({
        loop: true,
        items: 4,
        responsive: {
            0: {
                items: 1
            },
            580: {
                items: 2
            },
            840: {
                items: 3
            },
            1280: {
                items: 4
            }
        }
    });
    let $arrows = $('.arrows');
    $arrows.on('click', '.left', function() {
        owl.trigger('prev.owl.carousel');
    }).on('click', '.right', function() {
        owl.trigger('next.owl.carousel');
    });

    $arrows.css('height', $partners.height());
}
let myMap,
    myPlacemark;
export function initMap() {
    myMap = new ymaps.Map("map", {
        center: [55.15716057, 61.36968450],
        zoom: 16
    });

    myPlacemark = new ymaps.Placemark([55.15716057, 61.36968450], {
        hintContent: 'Мы тут',
        balloonContent: 'Ready Robot'
    }, {
        // Опции.
        // Необходимо указать данный тип макета.
        iconLayout: 'default#image',
        // Своё изображение иконки метки.
        iconImageHref: '/wp-content/themes/html5blank-stable/img/mark.png',
        // Размеры метки.
        iconImageSize: [30, 42],
        // Смещение левого верхнего угла иконки относительно
        // её "ножки" (точки привязки).
        iconImageOffset: [-13, -43]
    });

    myMap.geoObjects.add(myPlacemark);
    myMap.behaviors.disable('scrollZoom');
}

init();

module.exports = init;