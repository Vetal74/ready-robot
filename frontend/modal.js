/**
 * Created by vetal on 17.04.17.
 */
import CanvaLine from './drawLines';
export default class Modal {
    constructor(modal, layer) {
        this.modal = modal;
        this.layer = layer;
    }
    init() {
        let $modal = $(this.modal),
            $wrapper = $modal.find('.wrapper');
        let wrapperHeight = 0;
        wrapperHeight += $wrapper.find('header')
            .outerHeight(true);
        wrapperHeight += $wrapper.find('section')
            .outerHeight(true);
        wrapperHeight += +$wrapper.css('padding-top').replace('px', '');
        wrapperHeight += +$wrapper.css('padding-bottom').replace('px', '');
        if(wrapperHeight > screen.height * 0.95) $modal.find('.wrapper').css('overflow-y', 'auto');
        $modal.css('height', wrapperHeight);
        if($wrapper.find('section').outerHeight(true) > $wrapper.height()) this.addScroll();
        else this.removeScroll();
    }
    open() {
        this.init();
        this.layer.addClass('active');
        this.modal.addClass('active');
        this.drawLineModal(0.4);
    }
    close() {
        this.layer.removeClass('active');
        this.modal.removeClass('active');
    }
    swap() {

    }
    putContent(content, header) {
        let $wrapper = this.modal.find('.wrapper');
        $wrapper.find('header h3').html(header);
        $wrapper.find('section').html(content);
    }
    addScroll() {
        this.modal.find('.wrapper').css('overflow', 'auto');
    }
    removeScroll() {
        this.modal.find('.wrapper').css('overflow', 'hidden');
    }
    drawLineModal() {
        let idModalCanvas = $(this.modal).find('canvas').attr('id') || $(this.modal).find('canvas').attr('class');
        let line = new CanvaLine(idModalCanvas);
        let style = {
            stroke: '#00d7ff',
            strokeWidth: 2,
            shadowBlur: 20,
            shadowColor: '#00d7ff'
        };

        let $canvas = $('#' + idModalCanvas);
        if($canvas.length < 1) $canvas = $('.' + idModalCanvas);
        let width = $canvas.innerWidth(),
            height = $canvas.innerHeight();
        let fix = 5;

        let points = [
            {
                x: fix,
                y: fix
            },
            {
                x: fix,
                y: height - fix
            },
            {
                x: width - fix,
                y: height - fix
            },
            {
                x: width - fix,
                y: fix
            },
            {
                x: fix,
                y: fix
            }
        ];

        line.setProp(style, {width: width, height: height}, points);
        line.clear();
        setTimeout(function() {
            line.animation();
        }, 500);
    }
}