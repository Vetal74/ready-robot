/**
 * Created by vetal on 16.01.17.
 */


import {wscreen, sideMenu, refreshAnimation, resize} from './common';
import {draw, drawLineProfit, drawLineProduction, drawLineProfitrol, drawLineFaster, drawLineCheto, drawLineReviews, drawLineAction} from './draws';
import SliderAdvantages from './sliderAdvantages';
import SliderReviews from './sliderReviews';


let $wrapperProfit1 = $('#profitCanvas').parent(),
    $wrapperDevelop = $('.wrapperDevelop'),
    $wrapperProfit2 = $('#profitrolCanvas').parent(),
    $wrapperFaster = $('.wrapperFaster'),
    $wrapperReviews = $('.wrapperReviews'),
    $wrapperAction = $('.wrapperAction'),
    $wrapperProduction = $('.wrapperProduction'),
    $tabActive = $wrapperDevelop.find('.tabs .tab.active');
let $wrapperMain = $('.wrapperMain');

let $kuka = $wrapperMain.find('.kuka'),
    $iskr1 = $wrapperMain.find('.iscr1'),
    $iskr2 = $wrapperMain.find('.iscr2'),
    $iskr3 = $wrapperMain.find('.iscr3');




let scale = 1,
    koof = 1;
scale = 1 * wscreen.height / 1000;
$wrapperMain.hasClass('service') ? koof = 0.9 : koof = 1;
scale = koof * wscreen.height / 1000;
let centerX = $wrapperMain.width() / 2;
$wrapperMain.on('mousemove', function(event) {
    let diffX = centerX - event.clientX;
    let degX = 10 * (diffX / centerX);
    TweenMax.to($kuka, 0.5, {transform: 'rotateY(' + degX + 'deg) translateZ(' + $kuka.data('zindex') + 'px) scale('+scale+')', ease: Power1.easeOut});
    TweenMax.to($iskr1, 0.5, {x: (event.pageX * 0.04), ease: Power1.easeOut});
    TweenMax.to($iskr2, 0.5, {x: (event.pageX * 0.06), ease: Power1.easeOut});
    TweenMax.to($iskr3, 0.5, {x: (event.pageX * 0.09), ease: Power1.easeOut});
});


export function init() {
    TweenMax.from($kuka, 0.7, {x: -300, opacity: 0, ease: Power1.easeOut});
    TweenMax.set($kuka, { scale: scale });

    refreshAnimation();


    let tumbler = {
        develop: true,
        profit: true,
        production: true,
        profitrol: true,
        cheto: true,
        advaicon: true,
        reviews: true,
        action: true
    };

    let endOfAnimation = {
        production: false
    };

    let screenBreak = {
        develop: 1010,
        profit: 800,
        production: 800,
        profitrol: 800,
        faster: 800,
        cheto: 1050,
        reviews: 800,
        action: 0
    };
    let lines = {
        develop: 0,
        profit: 0,
        production: 0,
        profitrol: 0,
        faster: 0,
        cheto: 0,
        reviews: 0,
        action: 0
    };

    let activeTab = $tabActive.data('tab') || 'tab1';
    let $adva = $('.advantages.first .advaBlock'),
        $advaIcon = $adva.find('.icon'),
        $advaText = $adva.find('.description'),
        pointAdvaIcon = $advaIcon.offset().top + $advaIcon.height() / 2;

    let $advaVisible = $('.advaWrap .swapTab.visible');
    $(window).on('scroll', function() {
        let $this = $(this);
        let scroll = $this.scrollTop();

        if((scroll > wscreen.height / 2) && tumbler.develop) {
            tumbler.develop = false;
            if($wrapperDevelop.find('.active').length === 0) {
                $wrapperDevelop.find('.tabs .tab1').addClass('active');
                $wrapperDevelop.find('.tabsContent[data-tab=tab1]').addClass('active');
            }
            $wrapperDevelop.find('.karetka').removeClass('hidden');
            updateTab();
            karetkaGo();
            adaptiveHeightTabsDev();
        }

        if((scroll > $wrapperProfit1.offset().top - wscreen.height / 2) && tumbler.profit) {
            tumbler.profit = false;
            swapTabs($wrapperProfit1);
            setTimeout(function() { lines.profit = drawLineProfit(screenBreak.profit); }, 400);
        }
        if((scroll > $wrapperProfit1.offset().top - wscreen.height / 2) && tumbler.production && !tumbler.profit) {
            tumbler.production = false;
            $advaVisible = $('.advaWrap .swapTab.visible');
            $advaVisible.find('.default').addClass('active');
            $('.wrapperProduction .' + activeTab + 'default').addClass('active activeLine');
            //adaptiveHeightAdvaSlider(true);
            setTimeout(function() {
                lines.production = drawLineProduction(screenBreak.production);
                setTimeout(function() {
                    endOfAnimation.production = true;
                }, 1500);
            }, 1000);
        }
        if((scroll > $wrapperProfit2.offset().top - wscreen.height / 2) && tumbler.profitrol) {
            tumbler.profitrol = false;
            swapTabs($wrapperProfit2);
        }
        if((scroll > $wrapperProfit2.offset().top - wscreen.height / 2) && endOfAnimation.production) {
            endOfAnimation.production = false;
            lines.profitrol = drawLineProfitrol(screenBreak.profitrol);
        }

        if((scroll > $wrapperFaster.offset().top - wscreen.height / 2) && tumbler.cheto) {
            tumbler.cheto = false;
            swapTabs($wrapperFaster);
            lines.faster = drawLineFaster(screenBreak.faster);
            $wrapperFaster.find('.cheto').addClass('active');
            setTimeout( function() { lines.cheto = drawLineCheto(screenBreak.cheto, activeTab); }, 600);
        }
        if((scroll > $wrapperReviews.offset().top - wscreen.height / 2) && tumbler.reviews) {
            tumbler.reviews = false;
            lines.reviews = drawLineReviews(screenBreak.reviews);
        }
        if((scroll > $wrapperAction.offset().top - wscreen.height / 1.5) && tumbler.action) {
            tumbler.action = false;
            lines.action = drawLineAction(screenBreak.action);
        }
    });

    function adaptiveHeightTabsDev() {
        if(wscreen.height > 900) return false;
        let heightActiveTab = $wrapperDevelop.find('.tabsContent.active').height();
        $wrapperDevelop.css('min-height', 'calc(' + heightActiveTab + 'px + 8rem + 110px)');
    }
    function swapTabs($obj) {
        $obj.find('.swapTab').each(function (key, val) {
            if(($(val).data('tab') === activeTab) || ($(val).data('tab') === 'all')) {
                $(val).addClass('visible');
            } else {
                $(val).removeClass('visible');
            }
        });
    }

    // Сдвиг каретки на табах
    function karetkaGo() {
        let $tabActive = $('.wrapperDevelop .tabs .active').length ? $('.wrapperDevelop .tabs .active') : $('.wrapperDevelop .tabs .tab.tab1'),
            tabActiveLeft = $tabActive.offset().left - sideMenu || 0,
            tabActiveWidth = $tabActive.width() || 0,
            $karetkaTabs = $('.wrapperDevelop .tabs .karetka');

        if(wscreen.width < 1150) tabActiveLeft = $tabActive.offset().left;

        TweenMax.to($karetkaTabs, 0.4, {left: tabActiveLeft, width: tabActiveWidth});
        setTimeout(function() { lines.develop = draw(screenBreak.develop); }, 500);
    }

    $(window).on('resize', function () {
        if(screen.width > 1366){
            resize();
            karetkaGo();
            updateTab();
        }
    });

    $('.wrapperDevelop .tabs').on('click', '.tab a', function(e) {
        e.preventDefault();

        let $this = $(this).parent();

        if($this.hasClass('active')) return false;

        $this.parent()
            .find('.active')
            .removeClass('active');
        $this.addClass('active');

        let tab = $this.data('tab');
        $('.tabsContent').removeClass('active');
        $('.tabsContent[data-tab='+tab+']').addClass('active');

        $wrapperDevelop.find('.karetka').removeClass('hidden');

        $('html, body').animate({scrollTop: wscreen.height*0.8}, 500, 'swing');
        tumbler.develop = false;

        updateTab();
        karetkaGo();
        adaptiveHeightTabsDev();
    });

    function updateTab() {
        $tabActive = $wrapperDevelop.find('.tabs .tab.active');
        activeTab = $tabActive.data('tab');
        $advaVisible.find('.advaBlock.active').removeClass('active');
        $wrapperFaster.find('.cheto').removeClass('active');
        $adva = $('.advantages.first .advaBlock');
        $advaIcon = $adva.find('.icon');
        $advaText = $adva.find('.description');
        pointAdvaIcon = $advaIcon.offset().top + $advaIcon.height() / 2;
        $('.swapTab').removeClass('visible');
        $('.wrapperProduction .active').removeClass('active activeLine');
        tumbler.profit = true;
        tumbler.production = true;
        tumbler.profitrol = true;
        tumbler.cheto = true;
        tumbler.reviews = true;
        tumbler.action = true;
        if(isObject(lines.develop)) stopAndClear(lines.develop);
        if(isObject(lines.production)) stopAndClear(lines.production);
        if(isObject(lines.profit)) stopAndClear(lines.profit);
        if(isObject(lines.profitrol)) stopAndClear(lines.profitrol);
        if(isObject(lines.faster)) stopAndClear(lines.faster);
        if(isObject(lines.cheto)) stopAndClear(lines.cheto);
        if(isObject(lines.reviews)) stopAndClear(lines.reviews);
        if(isObject(lines.action)) stopAndClear(lines.action);

        //adaptiveHeightAdvaSlider(true);
    }

    function stopAndClear(line) {
        line.stopDraw();
        line.clear();
    }
    function isObject(obj) {
        return typeof(obj) === 'object';
    }


    let $slider = $('.wrapperProduction .slider');
    let $activeSlide = $slider.find('.' + activeTab + 'default');
    function adaptiveHeightAdvaSlider(ifFirst) {
        let height = $activeSlide.height();
        if(ifFirst) height += 100;
        TweenMax.set($slider, {'min-height': height});
    }
    $wrapperProduction.on('click', '.mobileManipulate .arrow', function() {
        let $this = $(this);
        $activeSlide = $slider.find('.activeLine');
        let props = {
            slider: $slider,
            type: 'arrows',
            action: $this.data('slide'),
            classes: {
                add: 'active',
                add2: 'activeLine',
                remove: 'hide'
            },
            activeSlide: $activeSlide
        };
        let slider = new SliderAdvantages(props);
        let response = slider.init();
    });
    $('.wrapperProfit .advantages.first').on('click', '.advaBlock', function() {
        let $this = $(this);
        if($this.hasClass('active')) return false;

        $this.parent().find('.advaBlock').removeClass('active');
        $this.addClass('active');
        $activeSlide = $slider.find('.activeLine');
        TweenMax.to('#productionCanvas', 0.3, {opacity: 0});
        setTimeout(function() {
            if(isObject(lines.production)) lines.production.stopDraw();
            lines.production = drawLineProduction(screenBreak.production);
        }, 600);


        let props = {
            slider: $slider,
            type: 'icons',
            action: $this.data('block'),
            classes: {
                add: 'active',
                add2: 'activeLine',
                remove: 'hide'
            },
            activeSlide: $activeSlide
        };
        let slider = new SliderAdvantages(props);
        let response = slider.init();
    });

    function setImageToPosition($obj) {
        $obj.each((key, val) => {
            let toX = '',
                toY = '';
            if($(val).hasClass('top')) toY = 'top';
            if($(val).hasClass('bottom')) toY += 'bottom';
            //if($(val).hasClass('left')) toX = 'left';
            if($(val).hasClass('right')) toX = 'right';
            if(toY === '') return true;
            let $item = $(val).closest('.item');
            let imageOffsets = {
                top: $(val).position().top,
                left: $(val).position().left,
                width: $(val).width(),
                height: $(val).height()
            };
            if(toY === 'topbottom') {
                let scalable = $item.outerHeight() / imageOffsets.height;
                $(val).css('transform', `scale(${scalable}`);
                return true;
            }
            let translateX = 0,
                translateY = 0;
            if(toY === 'top') translateY = (-1)*imageOffsets.top;
            if(toY === 'bottom') translateY = $item.height() - imageOffsets.top - imageOffsets.height;
            if(toX === 'right') translateX = $item.width() - imageOffsets.left - imageOffsets.width;
            $(val).css('transform', `translate3d(${translateX}px, ${translateY}px, 0)`);
        });
    }
    setImageToPosition($('.wrapperProduction .item img'));


    let $objSlider = $('.reviews');
    let slider = new SliderReviews($objSlider.find('.slider'));
    $objSlider.on('click', '.next', function() {
        slider.prevSlide();
    });
    $objSlider.on('click', '.prev', function() {
        slider.nextSlide();
    });

    let $footerBlock = $('.mainSection > footer .block'),
        heightBlock = null;
    $.each($footerBlock, function (key, val) {
        (key === 0) ?
            heightBlock = $(val).height() :
            $(val).css('height', heightBlock);
    });
}
init();