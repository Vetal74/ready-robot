/**
 * Created by vetal on 17.04.17.
 */

import { wscreen, sideMenu } from './common';
import CanvaLine from './drawLines';


export function draw(screenBreak) {
    if(wscreen.width < screenBreak) return false;
    let myLine = new CanvaLine('developCanvas');
    let $karetka = $('.wrapperDevelop .tabs .karetka'),
        $tabsContent = $('.wrapperDevelop .tabsContent.active'),
        $leftSide = $tabsContent.find('.leftSide'),
        $rightSide = $tabsContent.find('.rightSide'),
        $developCanvas = $('#developCanvas'),
        $h3 = $rightSide.find('h3');

    let karetkaProperties = {
        width: $karetka.width(),
        height: $karetka.height(),
        top: $karetka.position().top,
        left: $karetka.position().left
    };

    let isTabs3 = $('.wrapperDevelop .tab3').length;

    let width = $developCanvas.innerWidth(),
        height = $developCanvas.innerHeight();

    let style = {
        stroke: '#00d7ff',
        strokeWidth: 2,
        shadowBlur: 20,
        shadowColor: '#00d7ff'
    };


    let heightWrapperMain = $('.wrapperMain').outerHeight(true);

    let points = [];

    let sideOffsetTop = $rightSide.offset().top;
    if($rightSide.height() < $leftSide.height() && wscreen.width > 1920) sideOffsetTop = $leftSide.offset().top;
    if($('.wrapperDevelop .tab3').hasClass('active')) {
        points = [
            {
                x: karetkaProperties.left + karetkaProperties.width / 2,
                y: karetkaProperties.top + (karetkaProperties.height / 2) + 5
            },
            {
                x: karetkaProperties.left + karetkaProperties.width / 2,
                y: karetkaProperties.top + karetkaProperties.height
            },
            {
                x: width - width / 3.2,
                y: karetkaProperties.top + karetkaProperties.height
            },
            {
                x: width - width / 2.5,
                y: sideOffsetTop - heightWrapperMain - 70
            },
            {
                x: $rightSide.position().left + 60,
                y: sideOffsetTop - heightWrapperMain - 70
            },
            {
                x: $rightSide.position().left + 60,
                y: $rightSide.offset().top + $rightSide.outerHeight() - 10 - heightWrapperMain
            },
            {
                x: $rightSide.position().left + $rightSide.innerWidth() - 35,
                y: $rightSide.offset().top + $rightSide.outerHeight() - 10 - heightWrapperMain
            },
            {
                x: $rightSide.position().left + $rightSide.innerWidth() - 35,
                y: $rightSide.offset().top - heightWrapperMain + ($h3.height() / 2)
            },
            {
                x: $rightSide.position().left + 60,
                y: $rightSide.offset().top - heightWrapperMain + ($h3.height() / 2)
            }
        ];
    }

    function widthTab2(step) {
        if(step === 1) if(!isTabs3) return width - width / 3.2; else return width / 2;
        if(step === 2) if(!isTabs3) return width - width / 2.5; else return width / 1.9;
    }

    if($('.wrapperDevelop .tab2').hasClass('active')) {
        points = [
            {
                x: karetkaProperties.left + karetkaProperties.width / 2,
                y: karetkaProperties.top + (karetkaProperties.height / 2) + 5
            },
            {
                x: karetkaProperties.left + karetkaProperties.width / 2,
                y: karetkaProperties.top + karetkaProperties.height
            },
            {
                x: widthTab2(1),
                y: karetkaProperties.top + karetkaProperties.height
            },
            {
                x: widthTab2(2),
                y: sideOffsetTop - heightWrapperMain - 70
            },
            {
                x: $rightSide.position().left + 60,
                y: sideOffsetTop - heightWrapperMain - 70
            },
            {
                x: $rightSide.position().left + 60,
                y: $rightSide.offset().top + $rightSide.outerHeight() - 10 - heightWrapperMain
            },
            {
                x: $rightSide.position().left + $rightSide.innerWidth() - 35,
                y: $rightSide.offset().top + $rightSide.outerHeight() - 10 - heightWrapperMain
            },
            {
                x: $rightSide.position().left + $rightSide.innerWidth() - 35,
                y: $rightSide.offset().top - heightWrapperMain + ($h3.height() / 2)
            },
            {
                x: $rightSide.position().left + 60,
                y: $rightSide.offset().top - heightWrapperMain + ($h3.height() / 2)
            }
        ];
    }
    if($('.wrapperDevelop .tab1').hasClass('active')) {
        points = [
            {
                x: karetkaProperties.left + karetkaProperties.width / 2,
                y: karetkaProperties.top + (karetkaProperties.height / 2) + 5
            },
            {
                x: karetkaProperties.left + karetkaProperties.width / 2,
                y: karetkaProperties.top + karetkaProperties.height
            },
            {
                x: width / 3.2,
                y: karetkaProperties.top + karetkaProperties.height
            },
            {
                x: width / 2.5,
                y: sideOffsetTop - heightWrapperMain - 70
            },
            {
                x: $rightSide.position().left + 60,
                y: sideOffsetTop - heightWrapperMain - 70
            },
            {
                x: $rightSide.position().left + 60,
                y: $rightSide.offset().top + $rightSide.outerHeight() - 10 - heightWrapperMain
            },
            {
                x: $rightSide.position().left + $rightSide.innerWidth() - 35,
                y: $rightSide.offset().top + $rightSide.outerHeight() - 10 - heightWrapperMain
            },
            {
                x: $rightSide.position().left + $rightSide.innerWidth() - 35,
                y: $rightSide.offset().top - heightWrapperMain + ($h3.height() / 2)
            },
            {
                x: $rightSide.position().left + 60,
                y: $rightSide.offset().top - heightWrapperMain + ($h3.height() / 2)
            }
        ];
    }

    myLine.setProp(style, {width: width, height: height}, points);
    myLine.clear();
    myLine.animation();
    return myLine;
}
export function drawLineProfit(screenBreak) {
    if(wscreen.width < screenBreak) return false;
    let myLine = new CanvaLine('profitCanvas');
    let $profitCanvas = $('#profitCanvas'),
        $wrapperProfit = $profitCanvas.parent(),
        $leftSide = $('.wrapperDevelop .leftSide'),
        $header = $wrapperProfit.find('header'),
        $advantages = $wrapperProfit.find('.advaWrap'),
        $advaBlock = $advantages.find('.visible .advaBlock');

    let fix = 20;

    let leftSideWidth = $leftSide.width() + 35,
        betweenBlocks = $advantages.position().top - fix;

    let size = {
        width: $profitCanvas.innerWidth(),
        height: $profitCanvas.innerHeight()
    };
    let style = {
        stroke: '#00d7ff',
        strokeWidth: 2,
        shadowBlur: 20,
        shadowColor: '#00d7ff'
    };

    let points = [
        {
            x: 0,
            y: fix + 2
        },
        {
            x: leftSideWidth,
            y: fix + 2
        },
        {
            x: leftSideWidth,
            y: betweenBlocks
        }
    ];

    myLine.setProp(style, size, points);

    let halfWidthAdvantage = $advaBlock.width() / 2,
        lastAdvantage = $advaBlock.eq($advaBlock.length - 1),
        lastAdvantageCoord = lastAdvantage.position().left + halfWidthAdvantage,
        firstAdvantage = $advaBlock.eq(0),
        firstAdvantageCoord = firstAdvantage.position().left + halfWidthAdvantage;
    let additionally = [
        [
            {
                x: lastAdvantageCoord,
                y: betweenBlocks
            },
            {
                x: firstAdvantageCoord,
                y: betweenBlocks
            }
        ]
    ];

    $advaBlock.each(function(key, val) {
        let points = [
            [
                {
                    x: $(val).position().left + halfWidthAdvantage,
                    y: betweenBlocks
                },
                {
                    x: $(val).position().left + halfWidthAdvantage,
                    y: $advantages.position().top + fix + 44
                }
            ]
        ];
        additionally = additionally.concat(points);
    });
    myLine.animation(additionally);
    return myLine;
}
export function drawLineProduction(screenBreak) {
    if(wscreen.width < screenBreak) return false;
    let myLine = new CanvaLine('productionCanvas');
    let $productionCanvas = $('#productionCanvas'),
        $wrapperProduction = $productionCanvas.parent(),
        $activeAdvaBlock = $('.wrapperProfit .visible .advaBlock.active'),
        $activeProduct = $wrapperProduction.find('.item.activeLine');

    let fix = 90;

    let halfWidthAdvaBlock = $activeAdvaBlock.width() / 2,
        positionAdvaBlock = $activeAdvaBlock.position().left + halfWidthAdvaBlock,
        productAura = 4,
        halfHeightProduct = fix + ($activeProduct.innerHeight() / 2);

    let size = {
        width: $productionCanvas.innerWidth(),
        height: $productionCanvas.innerHeight()
    };
    let style = {
        stroke: '#00d7ff',
        strokeWidth: 2,
        shadowBlur: 20,
        shadowColor: '#00d7ff'
    };

    let points = [
        {
            x: positionAdvaBlock,
            y: 0
        },
        {
            x: positionAdvaBlock,
            y: fix - productAura
        },
        {
            x: $activeProduct.position().left - productAura,
            y: fix - productAura
        },
        {
            x: $activeProduct.position().left - productAura,
            y: fix + $activeProduct.height() + productAura
        },
        {
            x: size.width / 2,
            y: fix + $activeProduct.height() + productAura
        }
    ];

    let additionalPoints = [
        [
            {
                x: size.width / 2,
                y: fix + $activeProduct.height() + productAura
            },
            {
                x: $activeProduct.position().left + $activeProduct.width() + productAura,
                y: fix + $activeProduct.height() + productAura
            },
            {
                x: $activeProduct.position().left + $activeProduct.width() + productAura,
                y: fix - productAura
            },
            {
                x: positionAdvaBlock,
                y: fix - productAura
            }
        ]
    ];
    myLine.setProp(style, size, points);
    myLine.clear();
    TweenMax.set($productionCanvas, {opacity: 1});
    myLine.animation(additionalPoints);
    return myLine;
}
export function drawLineProfitrol(screenBreak) {
    if(wscreen.width < screenBreak) return false;
    let myLine = new CanvaLine('profitrolCanvas');
    let $profitrolCanvas = $('#profitrolCanvas'),
        $wrapperProfitrol = $profitrolCanvas.parent(),
        $header = $wrapperProfitrol.find('header'),
        $advantages = $wrapperProfitrol.find('.advaWrap'),
        $advaBlock = $advantages.find('.visible .advaBlock');

    let fix = 3,
        betweenBlocks = $advantages.position().top -
            +$header.css('padding-top').replace('px', '') / 2,
        halfWidthAdvaBlock = $advaBlock.width() / 2;


    betweenBlocks += 27;

    let size = {
        width: $profitrolCanvas.innerWidth(),
        height: $profitrolCanvas.innerHeight()
    };
    let style = {
        stroke: '#00d7ff',
        strokeWidth: 2,
        shadowBlur: 20,
        shadowColor: '#00d7ff'
    };

    let points = [
        {
            x: size.width / 2,
            y: 0
        },
        {
            x: size.width / 2,
            y: betweenBlocks
        }
    ];

    let additionalPoints = [
        [
            {
                x: $advaBlock.eq(2).position().left + halfWidthAdvaBlock,
                y: betweenBlocks
            },
            {
                x: $advaBlock.eq(0).position().left + halfWidthAdvaBlock,
                y: betweenBlocks
            }
        ],
        [
            {
                x: $advaBlock.eq(2).position().left + halfWidthAdvaBlock,
                y: betweenBlocks
            },
            {
                x: $advaBlock.eq(2).position().left + halfWidthAdvaBlock,
                y: $advantages.position().top + $advaBlock.eq(2).height() / 2
            }
        ],
        [
            {
                x: $advaBlock.eq(1).position().left + halfWidthAdvaBlock,
                y: betweenBlocks
            },
            {
                x: $advaBlock.eq(1).position().left + halfWidthAdvaBlock,
                y: $advantages.position().top + $advaBlock.eq(1).height() / 2
            }
        ],
        [
            {
                x: $advaBlock.eq(0).position().left + halfWidthAdvaBlock,
                y: betweenBlocks
            },
            {
                x: $advaBlock.eq(0).position().left + halfWidthAdvaBlock,
                y: $advantages.position().top + $advaBlock.eq(0).height() / 2
            }
        ]
    ];

    myLine.setProp(style, size, points);
    myLine.animation(additionalPoints);
    return myLine;
}
export function drawLineFaster(screenBreak) {
    if(wscreen.width < screenBreak) return false;
    let myLine = new CanvaLine('fasterCanvas');
    let $fasterCanvas = $('#fasterCanvas'),
        $wrapperFaster = $fasterCanvas.parent(),
        $header = $wrapperFaster.find('header'),
        $h3 = $header.find('h3');

    let size = {
        width: $fasterCanvas.innerWidth(),
        height: $fasterCanvas.innerHeight()
    };
    let style = {
        stroke: '#00d7ff',
        strokeWidth: 2,
        shadowBlur: 20,
        shadowColor: '#00d7ff'
    };

    let points = [
        {
            x: 0,
            y: $h3.position().top + $h3.outerHeight(true) / 2
        },
        {
            x: size.width,
            y: $h3.position().top + $h3.outerHeight(true) / 2
        }
    ];
    let additionalPoints = [
        [
            {
                x: 0,
                y: size.height - 20
            },
            {
                x: size.width,
                y: size.height - 20
            }
        ]
    ];
    myLine.setProp(style, size, points);
    myLine.animation(additionalPoints);
    return myLine;
}
export function drawLineCheto(screenBreak, activeTab) {
    if(wscreen.width < screenBreak) return false;
    let myLine = new CanvaLine('chetoCanvas');
    let $chetoCanvas = $('#chetoCanvas'),
        $cheto = $chetoCanvas.parent(),
        $icons = $cheto.find('.icon[data-tab='+activeTab+'] img');
    if (!$icons.length) $icons = $cheto.find('.icon[data-tab=all] img');

    let size = {
        width: $chetoCanvas.innerWidth(),
        height: $chetoCanvas.innerHeight()
    };
    let style = {
        stroke: '#ffffff',
        strokeWidth: 2,
        shadowBlur: 20,
        shadowColor: '#00d7ff'
    };

    let offsetsParent = {
        left: $cheto.offset().left,
        top: $cheto.offset().top
    };
    let offsetsIcons = {
        leftIcon_left: $icons.eq(0).offset().left - offsetsParent.left,
        leftIcon_top: $icons.eq(0).offset().top - offsetsParent.top,
        topIcon_top: $icons.eq(2).offset().top - offsetsParent.top,
        bottomIcon_top: $icons.eq(1).offset().top - offsetsParent.top
    };

    let halfWidthIcon = $icons.width() / 2;

    let oneTwoTop = $icons.eq(0).position().top,
        bottomTop = $icons.eq(1).offset().top - $cheto.offset().top,
        topTop = $icons.eq(2).offset().top - $cheto.offset().top;

    let points = [
        {
            x: offsetsIcons.leftIcon_left + halfWidthIcon * 2,
            y: offsetsIcons.leftIcon_top + halfWidthIcon
        },
        {
            x: $icons.eq($icons.length - 1).offset().left - offsetsParent.left,
            y: offsetsIcons.leftIcon_top + halfWidthIcon
        }
    ];


    let additionalPoints = [
    ];

    let lengthIcons = $icons.length - 1;
    $icons.each(function(key, val) {
        if(key === 0 || key === lengthIcons) return true;
        let coordY = $(val).offset().top - offsetsParent.top;
        if(key % 2 === 0) {
            coordY += halfWidthIcon * 2;
        }
        let points2 = [
            [
                {
                    x: $(val).offset().left - offsetsParent.left + halfWidthIcon,
                    y: offsetsIcons.leftIcon_top + halfWidthIcon
                },
                {
                    x: $(val).offset().left - offsetsParent.left + halfWidthIcon,
                    y: coordY
                }
            ]
        ];
        additionalPoints = additionalPoints.concat(points2);
    });


    myLine.setProp(style, size, points);
    myLine.animation(additionalPoints);
    return myLine;
}
export function drawLineReviews(screenBreak) {
    if(wscreen.width < screenBreak) return false;
    let myLine = new CanvaLine('reviewsCanvas');
    let $reviewsCanvas = $('#reviewsCanvas'),
        $wrapperReviews = $reviewsCanvas.parent(),
        $header = $wrapperReviews.find('header'),
        $p = $header.find('p'),
        $item = $wrapperReviews.find('.slider .item.visible'),
        $itemLeft = $wrapperReviews.find('.slider .item.visible.left'),
        $itemCenter = $wrapperReviews.find('.slider .item.visible.center'),
        $itemRight = $wrapperReviews.find('.slider .item.visible.right');

    let size = {
        width: $reviewsCanvas.innerWidth(),
        height: $reviewsCanvas.innerHeight()
    };
    let style = {
        stroke: '#00d7ff',
        strokeWidth: 2,
        shadowBlur: 20,
        shadowColor: '#00d7ff'
    };

    let points = [
        {
            x: size.width / 2,
            y: 0
        },
        {
            x: size.width / 2,
            y: $p.position().top + $p.outerHeight(true) / 2
        }
    ];

    let magick = $p.position().top + $p.outerHeight(true) / 2 +
        +$header.css('padding-bottom').replace('px', '') +
        +$wrapperReviews.find('.slider').css('padding-top').replace('px', '') + 35;

    let additionalPoints = [
        [
            {
                x: $itemLeft.offset().left - sideMenu + $item.width() / 2,
                y: $p.position().top + $p.outerHeight(true) / 2
            },
            {
                x: $itemRight.offset().left - sideMenu + $item.width() / 2,
                y: $p.position().top + $p.outerHeight(true) / 2
            }
        ],
        [
            {
                x: $itemLeft.offset().left - sideMenu + $item.width() / 2,
                y: $p.position().top + $p.outerHeight(true) / 2
            },
            {
                x: $itemLeft.offset().left - sideMenu + $item.width() / 2,
                y: magick
            }
        ],
        [
            {
                x: $itemCenter.offset().left - sideMenu + $item.width() / 2,
                y: $p.position().top + $p.outerHeight(true) / 2
            },
            {
                x: $itemCenter.offset().left - sideMenu + $item.width() / 2,
                y: magick
            }
        ],
        [
            {
                x: $itemRight.offset().left - sideMenu + $item.width() / 2,
                y: $p.position().top + $p.outerHeight(true) / 2
            },
            {
                x: $itemRight.offset().left - sideMenu + $item.width() / 2,
                y: magick
            }
        ]
    ];

    myLine.setProp(style, size, points);
    myLine.animation(additionalPoints);
    return myLine;
}
export function drawLineAction(screenBreak) {
    if(wscreen.width < screenBreak) return false;
    let myLine = new CanvaLine('actionCanvas');
    let $actionCanvas = $('#actionCanvas'),
        $wrapperAction = $('.wrapperAction'),
        $header = $wrapperAction.find('header'),
        $submit = $wrapperAction.find('form button');

    let size = {
        width: $actionCanvas.innerWidth(),
        height: $actionCanvas.innerHeight()
    };
    let style = {
        stroke: '#00d7ff',
        strokeWidth: 2,
        shadowBlur: 20,
        shadowColor: '#00d7ff'
    };

    let proc10w = size.width,
        proc10h = size.height / 10;
    // if(screen.width < 800) {
    //     proc10w = size.width - 20;
    // }
    let points = [
        {
            x: proc10w,
            y: $header.position().top
        },
        {
            x: 0,
            y: $header.position().top
        }
    ];

    let additionalPoints = [
        [
            {
                x: proc10w,
                y: $wrapperAction.innerHeight() + 1
            },
            {
                x: 0,
                y: $wrapperAction.innerHeight() + 1
            }
        ]
    ];

    myLine.setProp(style, size, points);
    myLine.animation(additionalPoints);
}
export function drawLineContacts(screenBreak) {
    if(wscreen.width < screenBreak) return false;
    let myLine = new CanvaLine('mapCanvas');
    let $mapCanvas = $('#mapCanvas'),
        $mapContacts = $('.map-contacts'),
        $unitedText = $mapContacts.find('.united-text'),
        $map = $mapContacts.find('#map');

    let size = {
        width: $mapCanvas.innerWidth(),
        height: $mapCanvas.innerHeight()
    };
    let style = {
        stroke: '#00d7ff',
        strokeWidth: 2,
        shadowBlur: 20,
        shadowColor: '#00d7ff'
    };

    let fix = 90;
    let aura = 4;

    let points = [
        {
            x: fix + $unitedText.position().left + $unitedText.width() / 2,
            y: $unitedText.position().top + $unitedText.height() / 2
        },
        {
            x: fix + $unitedText.position().left + $unitedText.width() / 2,
            y: $map.position().top + 20
        },
        {
            x: fix - 20,
            y: $map.position().top + 20
        },
        {
            x: fix - 20,
            y: $map.position().top + 225 + 20
        },
        {
            x: fix + $map.position().left - aura,
            y: $map.position().top + 225 + 20
        },
        {
            x: fix + $map.position().left - aura,
            y: $map.position().top - aura + 40
        },
        {
            x: fix + $map.position().left + $map.width() + aura,
            y: $map.position().top - aura + 40
        },
        {
            x: fix + $map.position().left + $map.width() + aura,
            y: $map.position().top + 450 + aura + 40
        },
        {
            x: fix + $map.position().left - aura,
            y: $map.position().top + 450 + aura + 40
        },
        {
            x: fix + $map.position().left - aura,
            y: $map.position().top + 225 + 20
        }
    ];


    myLine.setProp(style, size, points);
    myLine.animation();
}
export function drawLinePartners(screenBreak = 800) {
    if(wscreen.width < screenBreak) return false;
    let myLine = new CanvaLine('canvasPartners');
    let $canvasPartners = $('#canvasPartners'),
        $pagePartners = $canvasPartners.parent(),
        $h3 = $pagePartners.find('header h3');

    if(!$h3.length) {
        $h3 = $pagePartners.find('header p');
    }

    let size = {
        width: $canvasPartners.innerWidth(),
        height: $canvasPartners.innerHeight()
    };
    let style = {
        stroke: '#00d7ff',
        strokeWidth: 2,
        shadowBlur: 20,
        shadowColor: '#00d7ff'
    };


    let points = [
        {
            x: 0,
            y: $h3.position().top + $h3.height() / 2
        },
        {
            x: size.width,
            y: $h3.position().top + $h3.height() / 2
        }
    ];


    myLine.setProp(style, size, points);
    myLine.animation();
}
export function drawLineAbout(screenBreak) {
    if(wscreen.width < screenBreak) return false;
    let myLine = new CanvaLine('aboutPartners');
    let $profitCanvas = $('#aboutPartners'),
        $wrapperProfit = $profitCanvas.parent(),
        $header = $wrapperProfit.find('header'),
        $advantages = $wrapperProfit.find('.wrapSect'),
        $advaBlock = $advantages.find('.advaBlock');

    let fix = 0;

    let betweenBlocks = $advantages.position().top - fix;
    betweenBlocks = 45 + $header.find('h3').position().top + 10 + 52;

    let size = {
        width: $profitCanvas.innerWidth(),
        height: $profitCanvas.innerHeight()
    };
    let style = {
        stroke: '#00d7ff',
        strokeWidth: 2,
        shadowBlur: 20,
        shadowColor: '#00d7ff'
    };

    let points = [
        {
            x: size.width / 2,
            y: $header.find('h3').position().top + 10
        },
        {
            x: size.width / 2,
            y: betweenBlocks
        }
    ];

    myLine.setProp(style, size, points);

    let halfWidthAdvantage = $advaBlock.width() / 2,
        lastAdvantage = $advaBlock.eq($advaBlock.length - 1),
        lastAdvantageCoord = lastAdvantage.position().left + halfWidthAdvantage,
        firstAdvantage = $advaBlock.eq(0),
        firstAdvantageCoord = firstAdvantage.position().left + halfWidthAdvantage;
    let additionally = [
        [
            {
                x: lastAdvantageCoord,
                y: betweenBlocks
            },
            {
                x: firstAdvantageCoord,
                y: betweenBlocks
            }
        ]
    ];

    $advaBlock.each(function(key, val) {
        let points = [
            [
                {
                    x: $(val).position().left + halfWidthAdvantage,
                    y: betweenBlocks
                },
                {
                    x: $(val).position().left + halfWidthAdvantage,
                    y: $advantages.position().top + fix + 44
                }
            ]
        ];
        additionally = additionally.concat(points);
    });
    myLine.animation(additionally);
    return myLine;
}

