/**
 * Created by vetal on 17.04.17.
 */


console.log("%c   vetal@lapastilla.ru    ", "background-color: #ffdd2d; color: red;");

let pages = {
    // main: {
    //     route: '/',
    //     js: () => {return Router.importing('pages');},
    //     obj: null
    // },
    svarka: {
        route: '/',
        js: () => {return Router.importing('svarka');},
        obj: null
    },
    service_machine: {
        route: '/service_machine/',
        js: () => {return Router.importing('svarka');},
        obj: null
    },
    contacts: {
        route: '/contacts/',
        js: () => {return Router.importing('contacts');},
        obj: null
    },
    pages: {
        route: 'def',
        js: () => {return Router.importing('pages');},
        obj: null
    },
    portfolio: {
        route: '/portfolio/',
        js: () => {return Router.importing('pages');},
        obj: null
    },
    port: {
        route: '/portfolio/port/',
        js: () => {return Router.importing('port');},
        obj: null
    },
    mebel: {
        route: '/portfolio/mebel/',
        js: () => {return Router.importing('port');},
        obj: null
    },
    sharov: {
        route: '/portfolio/sharov/',
        js: () => {return Router.importing('port');},
        obj: null
    },
    about: {
        route: '/about/',
        js: () => {return Router.importing('about');},
        obj: null
    }
};

export default class Router {
    static getPage(url) {
        let def = true;
        $.each(pages, (key, value) => {
            if(value.route === url) {
                value.obj = value.js();
                def = false;
                return false;
            }
        });
        if(def) {
            pages.pages.obj = pages.pages.js();
        }
    }

    static ajaxReload(url) {
        let def = true;
        $.each(pages, (key, value) => {
            if(value.route === url) {
                if(this.isFunction(value.obj)) value.obj.init();
                else value.obj = value.js();
                def = false;
                return false;
            }
        });
        if(def) {
            if(this.isFunction(pages.def.obj)) pages.def.obj.init();
            else pages.def.obj = pages.def.js();
        }
    }

    static importing(req) {
        return import(`./${req}`)
            .then(request => this.requestListener(request, req))
            .catch(err => this.errorsCatch(err));
    }

    static requestListener(request, script) {
        console.log(`Загружено: ${script}`);
        return request;
    }

    static isFunction(func) {
        return typeof(func === 'Function');
    }

    static errorsCatch(err) {
        new Error(err);
    }
}

Router.getPage(location.pathname);
