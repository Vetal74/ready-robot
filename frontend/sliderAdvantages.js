/**
 * Created by vetal on 17.04.17.
 */
export default class SliderAdvantages {
    constructor(props) {
        this.$slider = props.slider;
        this.$activeSlide = props.activeSlide;
        this.type = props.type;
        this.action = props.action || false;
        this.classes = props.classes;
    }
    init() {
        if(this.type === 'arrows') {
            return this.arrows();
        }
        if(this.type === 'icons') {
            return this.icons();
        }
    }
    arrows() {
        var $nextSlide = '';
        if(this.action === 'left') $nextSlide = this.$activeSlide.prev();
        if(this.action === 'right') $nextSlide = this.$activeSlide.next();
        if($nextSlide.length < 1 || $nextSlide.hasClass('mobileManipulate')) return 'Больше слайдов нет';
        this.slide($nextSlide);
        return 'Успешно';
    }
    icons() {
        if(this.action === undefined) return 'Ошибка';
        if(this.$activeSlide.hasClass(this.action)) return false;
        var $nextSlide = this.$slider.find('.' + this.action);
        this.slide($nextSlide);
        return $nextSlide;
    }
    slide($nextSlide) {
        var self = this;
        this.$activeSlide.addClass(this.classes.remove);
        $nextSlide.addClass(this.classes.add + ' ' + this.classes.add2);
        this.$activeSlide.removeClass(this.classes.add2);
        setTimeout(function() {
            self.$activeSlide.removeClass(self.classes.add);
            setTimeout(function() {
                self.$activeSlide.removeClass(self.classes.remove);
            }, 500);
        }, 600);
    }
}
