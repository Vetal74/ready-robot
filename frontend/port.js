/**
 * Created by vetal on 25.04.17.
 */


const common = require('./common');
const draws = require('./draws');
import ImageBox from './imageBox/imageBox';

function init() {
    $(window).off('scroll');
    setTimeout(() => {
        common.refreshAnimation();
        draws.drawLineAction(0);
    }, common.TIMEOUT_PAGE);

    const KEY_ESC = 27;
    const KEY_LEFT = 37;
    const KEY_RIGHT = 39;

    let imageBox = new ImageBox();
    $('.imageBoxClose').on('click', () => {imageBox.close()});
    $('.layoutImageBox').on('click', () => {imageBox.close()});
    $('.imageBoxPrev').on('click', () => {imageBox.slide(false)});
    $('.imageBoxNext').on('click', () => {imageBox.slide(true)});
    $('.content-image img').on('click', () => {imageBox.slide(true)});
    $(document).on('keyup', (e) => {
        if(imageBox.isOpened()) {
            switch(e.keyCode) {
                case KEY_ESC:
                    imageBox.close();
                    break;
                case KEY_LEFT:
                    imageBox.slide(false);
                    break;
                case KEY_RIGHT:
                    imageBox.slide(true);
                    break;
            }
        }
    });

    let $itemWrapper = $('.item-wrapper a');
    $itemWrapper.on('click', function () {
        imageBox.open($(this));
        return false;
    });
}
init();