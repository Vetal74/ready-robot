/**
 * Created by vetal on 17.04.17.
 */

import Router from './router';
import PageLoader from './pageLoader';
import Modal from './modal.js';


export let $body = $('body');

export let $aside = $('aside.mainMenu');

let $asideLinks = $aside.find('.navigation a');
$asideLinks.removeClass('active');
$asideLinks.each((key, val) => {
    if($(val).attr('href') === location.pathname) $(val).addClass('active');
});

export const TIMEOUT_PAGE = 1000;

export let wscreen = {
    height: $(window).height(),
    width: $(window).width()
};
export let sideMenu = $aside.width();
if(wscreen.width < 1050) sideMenu = 0;

export function refreshAnimation() {
    let $refreshAnimation = $('.refreshAnimation');
    $refreshAnimation.find('.fade').addClass('go');
}

$('input[type=tel]').mask('+7 (999) 999-99-99',{autoclear: false});

let $layer = $('.layout');
let modalSendClaim = new Modal($('.mainModal'), $layer);
let modalIndivid = new Modal($('.modalIndivid'), $layer);
let modalSuccess = new Modal($('.modalSuccess'), $layer);

let gibContent = `
На современном производстве в технологической цепочке обработки листового металла распространенной операцией является гибка. При подаче материала в ручном режиме возникает несколько неблагоприятных факторов:<br>
<ul>
<li>не всегда можно достичь требуемых параметров точности,</li>
<li>отсутствие повторяемости получаемой заготовки,</li>
<li>низкая производительность,</li>
<li>быстрая утомляемость рабочего, особенно это актуально при работе с заготовками большой массы и при большом количестве гиба.</li>
</ul>
Поэтому роботизация процесса гибки позволяет устранить все эти факторы и значительно увеличить производительность и качество производимой заготовки и при правильной организации технологического процесса ускорить последующие сборочно-сварочные операции при производстве изделий из листового металла.<br><br>

Современная роботизированная гибка металла является полностью автоматическим процессом. Все операции осуществляются с помощью специализированных станков-листогибов, прессов, гибочных машин, промышленных роботов. Использование пневмозахватов и дополнительного оборудование позволяет обеспечить точное перемещение и позиционирование заготовки в прессе.<br><br>

Преимущества роботизированной гибки:<br>
<ul>
<li>высокая производительность процесса;</li>
<li>высокая повторяемость благодаря специализированному программному обеспечению;</li>
<li>возможность оффлайн программирования;</li>
<li>минимальное время переналадки с одного изделия на другое;</li>
<li>единое управление роботом и прессом посредством контроллера робота;</li>
</ul><br><br>
Специалисты нашей компании могут разработать для вас оптимальное решение для задач на вашем производстве, как по новому роботизированному гибочному комплексу, так и рекомендации по роботизации гибки на уже существующем оборудовании. Также мы можем разработать и изготовить дополнительное оборудование - транспортеры, накопители заготовок, автоматические склады, магазины инструментов и другое, позволяющее увеличить производительность.
`;
let pressContent = `
Работа с прессами и формовочными машинами – это трудоемкая и ответственная операция, которая при использовании ручного труда требует большого умения, аккуратности и высокой квалификации рабочего-формовщика. Многие вспомогательные процессы литейного производства, такие как складирование и своевременная подача инструмента требуют только надлежащего исполнения.<br>
Данные операции робот производит с высокой точностью:<br>
<ul>
<li>простановка стержней в форму;</li>
<li>обдув;</li>
<li>покраска формы;</li>
<li>простановка грузов;</li>
<li>заливка и дозирование расплава;</li>
<li>транспортировка и манипулирование отливок.</li>
</ul>
Применение роботов для выемки отливок из форм, их очистки, обрубки и удаления литников упрощает задачу сортировки отливок.<br><br>


Автоматизация измерения таких простых показателей, как температура отливки и влажность формовочной смеси, которая должна сохраняться в определенных пределах длительное время не допускает применения общетехнических датчиков и предполагает разработку и создание специальных датчиков контроля технологических параметров.<br><br>

Роботизация процесса в совокупности с автоматизацией измерений позволяет создать гибкое производство, эффективное как для крупных, так и для мелких серий.
`;
let tokarContent = `
Применение роботов в процессах механической обработки и обслуживания станков ЧПУ значительно сказывается на повышении производительности, снижении брака, освобождении рабочего от тяжелого монотонного труда, повышении технологичности и гибкости процессов и как следствие снижении себестоимости и увеличении прибыли производства.<br><br>

Существует большое количество операций, которые могут эффективно выполнять гибкие роботизированные ячейки в зависимости от задач и использования различных инструментов.
<ul>
<li>загрузка/выгрузка деталей в токарные и фрезерные станки</li>
<li>загрузка/выгрузка паллет с оснасткой и деталями в обрабатывающие центры</li>
<li>чистка и мойка деталей от СОЖ и стружки</li>
<li>удаление заусенцев и снятие фасок</li>
<li>сверловка и фрезеровка труднодоступных мест</li>
<li>крацевание и шлифовка</li>
<li>маркировка и гравировка</li>
<li>до и послеоперационный контроль и измерение ключевых параметров</li>
</ul>
Таким образом использование гибких роботизированных комплексов позволяет в рамках одной системы выполнить большое количество операций, что уменьшает технологический передел, увеличивает производительность и качество готового изделия, уменьшает риски производств и дает им серьезные конкурентные преимущества.
`;
export function openModalWithData(modal) {
    switch (modal) {
        case 'mainModal':
            modalSendClaim.open();
            break;
        case 'modalIndivid':
            modalIndivid.open();
            break;
        case 'gib':
            modalIndivid.putContent(gibContent, 'Гибочное оборудование');
            modalIndivid.open();
            break;
        case 'press':
            modalIndivid.putContent(pressContent, 'ПРЕССЫ И ФОРМОВОЧНЫЕ МАШИНЫ');
            modalIndivid.open();
            break;
        case 'tokar':
            modalIndivid.putContent(tokarContent, 'ТОКАРНО-ФРЕЗЕРНАЯ ГРУППА');
            modalIndivid.open();
            break;
    }
}
$body.on('click', 'a.openModal', function() {
    let modal = $(this).data('modal');
    openModalWithData(modal);
    return false;
});

$('.modal .close').on('click', closeAllModal);
$layer.on('click', closeAllModal);
function closeAllModal() {
    modalSendClaim.close();
    modalIndivid.close();
    modalSuccess.close();
}

$('.modal .mainModal').on('mousemove', function(event) {
    let $this = $(this);
    TweenMax.to($this.find('.decor.i1'), 0.5, {x: (event.pageX * 0.04), ease: Power1.easeOut});
    TweenMax.to($this.find('.decor.i2'), 0.5, {x: (event.pageX * 0.04), ease: Power1.easeOut});
});

$('.burger').on('click', function () {
    $aside.addClass('active');
});
$('.closeMobile').on('click', function () {
    $aside.removeClass('active');
});


let $navigation = $aside.find('.navigation'),
    $mainMenu = $navigation.find('nav.main'),
    $slideMenu = $navigation.find('nav.slideMenu');

$mainMenu.on('click', 'a', function() {
    let $this = $(this);
    if($this.data('menu')) {
        $mainMenu.addClass('hide');
        $navigation.find('.' + $this.data('menu'))
            .removeClass('hide');
        return false;
    }
});
$slideMenu.on('click', '.backToMenu', function() {
    $slideMenu.addClass('hide');
    $mainMenu.removeClass('hide');
});

$('form').submit(function(e) {
    let $this = $(this);
    let data = $this.serialize();
    let telInput = $this.find('input[type=tel]');
    if (telInput.val().indexOf('_') !== -1) {
        telInput.get(0).setCustomValidity("Заполните это поле");
        return false;
    }
    $.ajax({
        type: "POST",
        url: '/wp-content/themes/html5blank-stable/mailer.php',
        data: data,
        success: function(suc) {
            closeAllModal();
            modalSuccess.open();
            $this.trigger('reset');
        }
    });
    e.preventDefault();
});

// let href = '';
// let pageLoader = new PageLoader(
//     'mainSection',
//     'oldContent',
//     'newContent',
//     'slide',
//     'slide-prev',
//     TIMEOUT_PAGE, '', Router.ajaxReload);
// $body.on('click', 'a:not(.no-ajax)', function () {
//     href = $(this).attr('href');
//     pageLoader.getPage(href, 'next');
//     return false;
// });
// $(window).on('popstate', function() {
//     pageLoader.getPage(location.href, 'prev');
// });

export function resize() {
    if(wscreen.height < 700)
        $aside.find('.logo img')
            .attr('src', '/wp-content/themes/html5blank-stable/img/logo-mobile.png');
    else
        $aside.find('.logo img')
            .attr('src', '/wp-content/themes/html5blank-stable/img/logo.png');

    sideMenu = $aside.width();
    wscreen = {
        width: $(window).width(),
        height: $(window).height()
    };
    if(wscreen.width < 1050) sideMenu = 0;

    let $man = $('.wrapperAction .man'),
        $kuka = $('.wrapperAction .kuka'),
        $lineAction = $('.wrapperAction .lineAction');
    //$man.css('transform', `scale(${wscreen.width * 0.0005})`);
    //$kuka.css('transform', `scale(${wscreen.width * 0.0005})`);
    if(wscreen.width > 1280 && $man.length) $man.css('left', $lineAction.offset().left - sideMenu - 331);
}
resize();
