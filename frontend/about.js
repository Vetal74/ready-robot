/**
 * Created by vetal on 19.04.17.
 */

import {refreshAnimation, TIMEOUT_PAGE} from './common';
import {drawLineAction, drawLineAbout, drawLinePartners} from './draws';
import './owl.carousel.min';

export function init() {
    $(window).off('scroll');
    setTimeout(() => {
        refreshAnimation();
        drawLineAction(0);
        drawLineAbout(0);
        drawLinePartners(800);
    }, TIMEOUT_PAGE);
    let $partners = $('.partners');
    let owl = $partners.owlCarousel({
        loop: true,
        items: 4,
        responsive: {
            0: {
                items: 1
            },
            580: {
                items: 2
            },
            840: {
                items: 3
            },
            1280: {
                items: 4
            }
        }
    });
    let $arrows = $('.arrows');
    $arrows.on('click', '.left', function() {
        owl.trigger('prev.owl.carousel');
    }).on('click', '.right', function() {
        owl.trigger('next.owl.carousel');
    });

    $arrows.css('height', $partners.height());
}
init();