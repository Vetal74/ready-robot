/**
 * Created by vetal on 25.04.17.
 */
import CanvaLine from '../drawLines';
export default class ImageBox {
    /*
    * @data - json массив с параметрами
    * @data.classThumb - класс объекта, который должен увеличиться
    * @data.attrDataPopup - data-name аттрибута с ссылкой на крупное изображение
    * @data.modal.classModal - класс модального окна, в котором должно открываться окно
    * @data.modal.classOverlay - класс для подложки
    * @data.modal.classActive - класс появления модального окна
    * @data.modal.classArrowNext - класс для стрелочки галереи далее
    * @data.modal.classArrowPrev - класс для стрелочки галереи назад
    * @data.modal.classClose - класс закрытия
    * @data.modal.slidePrev - класс эффекта назад
    * @data.modal.slideNext - класс эффекта перехода вперед
    * @data.callbacks.onOpen - колбек открытия
    * @data.callbacks.onCloseModal - колбек закрытия модального окна
     */
    constructor(data) {
        const preventData = {
            classThumb: 'imageThumb',
            attrDataPopup: 'imageBox',
            modal: {
                classModal: 'imageBoxModal',
                classOverlay: 'layoutImageBox',
                classActive: 'active',
                classArrowNext: 'imageBoxNext',
                classArrowPrev: 'imageBoxPrev',
                classClose: 'imageBoxClose',
                slidePrev: 'imageBoxSlidePrev',
                slideNext: 'imageBoxSlideNext'
            },
            callbacks: {
                onOpen: () => {},
                onCloseModal: () => {}
            }
        };
        this.data = data || preventData;

        this.currentImage = '';
        this.gallery = ('.wrapper.page-innerPortfolio section');
        this.indexCurrent = 0;
        this.$modal = {};
        this.$image = {};
    }
    open(thisImage) {
        const url = $(thisImage).data('imagebox');
        this.currentImage = thisImage.parent();
        this.indexCurrent = $(this.currentImage).index();
        this.$modal = $('.' + this.data.modal.classModal);

        if(this.currentImage.hasClass('video')) {
            this.openTheVideo(url);
        } else {
            if(this.$image === 'iframe') this.openTheImage(url);
            else {
                this.$image = this.$modal.find('.content-image img');
                this.$image.length ? this.$image.attr('src', url) : this.$modal.find('.content-image').html(`<img src="${url}" alt="" >`);
                this.$image = this.$modal.find('.content-image img');
            }
            this.adaptive();
        }

        this.$modal.addClass('active');
        $('.' + this.data.modal.classOverlay).addClass('active');

        this.drawLine(500, true);
    }
    close() {
        $('.' + this.data.modal.classModal).removeClass('active');
        $('.' + this.data.modal.classOverlay).removeClass('active');
        this.$modal.find('.content-image').empty();
        this.cssReset();
    }
    slide(next = true) {
        let nextSlide = '',
        indexNextSlide = 0,
        itemWrappers = $(this.gallery).find('.item-wrapper'),
            infinite = itemWrappers.length - 1,
        url = '';
        if(next) {
            indexNextSlide = this.indexCurrent + 1 > infinite ? 0: this.indexCurrent + 1;
        } else {
            indexNextSlide = this.indexCurrent - 1 < 0 ? infinite: this.indexCurrent - 1;
        }
        nextSlide = itemWrappers.eq(indexNextSlide);
        this.indexCurrent = nextSlide.index();
        url = nextSlide.find('a').data('imagebox');
        if(nextSlide.hasClass('video')) {
            this.openTheVideo(url);
            this.cssReset();
        } else {
            if(this.$image === 'iframe') this.openTheImage(url);
            else this.$image.attr('src', url);
            this.cssReset();
            this.adaptive();
        }
        this.drawLine(0);
    }
    openTheImage(url) {
        let template = `<img src="${url}" alt="">`;
        let contentWrapper = this.$modal.find('.content-image');
        contentWrapper.find('iframe').remove();
        contentWrapper.append(template);
        this.$image = contentWrapper.find('img');
    }
    openTheVideo(url) {
        let self = this;
        let template = `<iframe src="${url}" frameborder="0" allowfullscreen></iframe>`;
        let contentWrapper = this.$modal.find('.content-image');
        if(contentWrapper.find('img').length) {
            contentWrapper.find('img').remove();
            pushIframe();
        } else {
            let iframe = contentWrapper.find('iframe');
            if(!iframe.length) {
                pushIframe();
            } else {
                iframe.attr('src', url);
            }
        }
        function pushIframe() {
            contentWrapper.append(template);
            self.$image = 'iframe';
        }
    }
    adaptive() {
        let naturalHeight = this.$image[0].naturalHeight;
        let naturalWidth = this.$image[0].naturalWidth;
        let overWidth = false;
        if(naturalHeight > naturalWidth) {
            this.$image.css({
                height: '100%',
                width: 'auto'
            });
        } else {
            this.$image.css({
                height: 'auto',
                width: '100%'
            });
            overWidth = true;
        }
        let height = this.$image.height() || this.$modal.find('iframe').height() || 500;
        let width = overWidth ? '100%' : this.$image.width() || 500;

        this.$modal.css({
            'height': height,
            'width': width
        });
    }
    drawLine(timeOut, staticLine = false) {
        let idModalCanvas = $(this.$modal).find('canvas').attr('id') || $(this.$modal).find('canvas').attr('class');
        let line = new CanvaLine(idModalCanvas);
        let style = {
            stroke: '#00d7ff',
            strokeWidth: 2,
            shadowBlur: 20,
            shadowColor: '#00d7ff'
        };

        let $canvas = $('#' + idModalCanvas);
        if($canvas.length < 1) $canvas = $('.' + idModalCanvas);
        let width = $canvas.innerWidth(),
            height = $canvas.innerHeight();
        let fix = 5;

        let points = [
            {
                x: fix,
                y: fix
            },
            {
                x: fix,
                y: height - fix
            },
            {
                x: width - fix,
                y: height - fix
            },
            {
                x: width - fix,
                y: fix
            },
            {
                x: fix,
                y: fix
            }
        ];

        line.setProp(style, {width: width, height: height}, points);
        //if (timeOut > 200) line.stopDraw();
        line.clear();
        setTimeout(function() {
            staticLine ? line.animation() : line.drawStaticLine();
        }, timeOut);
    }
    isOpened() {
        return $('.' + this.data.modal.classModal).hasClass('active');
    }
    cssReset() {
        this.$modal.removeAttr('style');
    }
    getIndex() {
        return {
            indexCurrent: this.indexCurrent,
            all: this.gallery.length
        }
    }
    reinit(data = 0) {
        if(data === 0) throw new Error('Вы не указали новые параметры для реинициализации');
        this.data = data;
    }
}