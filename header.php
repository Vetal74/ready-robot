<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title><?php blogInfo('name'); ?></title>
    <meta name="description" content="<?php blogInfo('description'); ?>">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700&amp;subset=cyrillic" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/style.css">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri(); ?>/img/favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/img/favicons/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/img/favicons/favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="<?php echo get_template_directory_uri(); ?>/img/favicons/manifest.json">
    <link rel="mask-icon" href="<?php echo get_template_directory_uri(); ?>/img/favicons/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="theme-color" content="#004d7c">
</head>
<body>
<div class="layout lighten layoutImageBox"></div>
<div class="modal mainModal">
    <canvas id="canvasModal1"></canvas>
    <div class="wrapper">
        <div class="close">
            <div class="cross"></div>
        </div>
        <header>
            <h3>Оставьте заявку</h3>
            <p>и мы свяжемся с вами в ближайшее время</p>
        </header>
        <section>
            <form action="/" method="post">
                <label><input type="text" name="name" placeholder="Имя"></label>
                <label><input type="tel" name="tel" placeholder="+7  (        )      -     -   " required></label>
                <button type="submit" class="button orange arrow">Отправить</button>
            </form>
        </section>
        <div class="decor i1"></div>
        <div class="decor i2"></div>
    </div>
</div>
<div class="modal modalSuccess">
    <div class="wrapper">
        <div class="close">
            <div class="cross"></div>
        </div>
        <header>
            <h3>Спасибо!</h3>
            <p>Мы свяжемся с вами в ближайшее время</p>
        </header>
        <section>
            <button class="orange button okmodal close">Ок</button>
        </section>
    </div>
</div>
<div class="modal modalIndivid">
    <canvas class="canvaIndivid"></canvas>
    <div class="wrapper">
        <div class="close">
            <div class="cross"></div>
        </div>
        <header>
            <h3 class="upp orange">Индивидуальные проекты</h3>
        </header>
        <section>
            <div class="numberBlock">
                <div class="number"><span>1</span></div>
                <p class="head">Предварительное решение.</p>
                <p class="desc">Когда вы приняли решение автоматизировать процесс сварки на своем предприятии, вы, в первую очередь, выбираете изделие (возможен выбор нескольких изделий) и присылаете нам чертежи, электронную модель изделия (по возможности), месячную программу изготовления и краткую характеристику по трудозатратам. Проведя предварительный расчёт возможности и эффективности внедрения автоматизированной сварки, времени сварки и сроков окупаемости оборудования мы делаем предварительное предложение по составу, количеству и стоимости роботизированного сварочного комплекса (РСК).</p>
            </div>
            <div class="numberBlock">
                <div class="number"><span>2</span></div>
                <p class="head">Промышленный аудит.</p>
                <p class="desc">Специалисты компании «Ready Robot» по предварительной договоренности приезжают на площадку предприятия, знакомятся с персоналом, заготовительным производством, существующим технологическим процессом, производственными мощностями. Обсуждается предполагаемое место установки РСК.
                    После проведения производственного аудита мы разрабатываем проект РСК и направляем вам окончательное коммерческое предложение. В случае принятия положительного решения подписывается договор на поставку оборудования и технологическое сопровождения проекта.</p>
            </div>
            <div class="numberBlock">
                <div class="number"><span>3</span></div>
                <p class="head">Производственный процесс.</p>
                <p class="desc">Мы поставляем, устанавливаем и запускаем оборудование на предприятии, обучаем специалистов работе на РСК.
                    В это же время специалисты компании «Ready Robot» разрабатывают и изготавливают оснастку для свариваемого изделия, разрабатывают технологию сварки и пишут программу для РСК.
                    После запуска оборудования, установки оснастки и отладки программы мы производим пробную сварку изделия, анализируем качества сварки и соответствие полученного изделия чертежам. При необходимости дорабатываем технологию сварки,  оптимизируем время, после чего оборудование и технология передается специалистам вашего предприятия.</p>
            </div>
            <div class="numberBlock">
                <div class="number"><span>4</span></div>
                <p class="head">Разработка рекомендаций.</p>
                <p class="desc">К моменту внедрения РТК на предприятии уже сформировался определенный подход к производству данного изделия: как правило, это происходит на нескольких сварочных постах и имеет предварительную сборку на отдельных рабочих местах. При внедрении технологии роботизированной сварки все логистические цепочки будут стекаться на один пост, что потребует другого подхода к обеспечению заготовкой поста РСК, а также более жестких требований к заготовительному производству. Поэтому мы подготавливаем подробные рекомендации по работе с РСК. </p>
            </div>
        </section>
    </div>
</div>
<div class="imageBox imageBoxModal">
    <canvas id="imageBox"></canvas>
    <div class="wrapper">
        <div class="close imageBoxClose">
            <div class="cross"></div>
        </div>
        <div class="content-image">
            <img src="<?php echo get_template_directory_uri(); ?>/img/portfolio/1.jpg" alt="">
        </div>
        <div class="arrow left imageBoxPrev">
            <img src="<?php echo get_template_directory_uri(); ?>/img/portfolio/arrow-left.png" alt="">
        </div>
        <div class="arrow right imageBoxNext">
            <img src="<?php echo get_template_directory_uri(); ?>/img/portfolio/arrow-right.png" alt="">
        </div>
    </div>
</div>
<aside class="mainMenu">
    <div class="wrapper">
        <div class="closeMobile">
            <svg viewBox="0,0 12,12">
                <path d="M0,0 12,12 M0,12 12,0" ></path>
            </svg>
        </div>
        <div class="gradiLine"></div>
        <div class="logo">
            <a href="/"><img src="<?php echo get_template_directory_uri(); ?>/img/logo.png" alt=""></a>
        </div>
        <div class="navigation">
            <nav class="main">
                <ul>
                    <li>
                        <a href="/about/"><span>О компании</span></a>
                    </li>
<!--                    <li>-->
<!--                        <a href="" data-menu="uslugi"><span>Услуги -></span></a>-->
<!--                    </li>-->
                    <li>
                        <a href="/"><span>Сварка</span></a>
                    </li>
                    <li>
                        <a href="/service_machine/"><span>Обслуживание машин</span></a>
                    </li>
                    <li>
                        <a href="/asu_pt/"><span>Системы управления</span></a>
                    </li>
                    <li>
                        <a href="/palletirovanie/"><span>Паллетирование</span></a>
                    </li>
<!--                    <li>-->
<!--                        <a href="/ready_job/"><span>Готовые решения</span></a>-->
<!--                    </li>-->
<!--                    <li>-->
<!--                        <a href="/individual/"><span>Индивидуальные проекты</span></a>-->
<!--                    </li>-->
                    <li>
                        <a href="/portfolio/"><span>Портфолио</span></a>
                    </li>
                    <li>
                        <a href="/contacts/"><span>Контакты</span></a>
                    </li>
                </ul>
            </nav>
            <nav class="slideMenu uslugi hide">
                <div class="backToMenu">
                    Назад в меню
                </div>
                <ul>
                    <li>
                        <a href=""><span>Сварка</span></a>
                    </li>
                    <li>
                        <a href=""><span>Обслуживание станков</span></a>
                    </li>
                    <li>
                        <a href=""><span>Паллетирование</span></a>
                    </li>
                </ul>
            </nav>
        </div>
        <!--<div class="block calc">-->
        <!--<button class="button orange calc">Калькулятор</button>-->
        <!--</div>-->
        <div class="block contact">
            <a href="tel:88005503074" class="tel no-ajax">8 800 550 30 74</a>
            <a href="" class="button blue openModal no-ajax" data-modal="mainModal">Отправить заявку</a>
            <p class="pod">Получите расчет по вашим параметрам</p>
        </div>
    </div>
</aside>
<section class="mainSection">
    <div class="burger">
        <svg viewBox="0,0 18,14">
            <path d="M0,0 18,0 M0,7 18,7 M0,14 18,14"></path>
        </svg>
    </div>
    <div class="oldContent">
