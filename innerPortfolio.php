<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>Ready-robot</title>
    <link rel="stylesheet" href="css/style.css">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="apple-touch-icon" sizes="180x180" href="img/favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" href="img/favicons/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="img/favicons/favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="img/favicons/manifest.json">
    <link rel="mask-icon" href="img/favicons/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="theme-color" content="#ffffff">
    <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
</head>
<body>
<div class="layout lighten"></div>
<div class="modal mainModal">
    <canvas id="canvasModal1"></canvas>
    <div class="wrapper">
        <div class="close">
            <div class="cross"></div>
        </div>
        <header>
            <h3>Оставьте заявку</h3>
            <p>и мы свяжемся с вами в ближайшее время</p>
        </header>
        <section>
            <form action="/" method="post">
                <label><input type="text" name="name" placeholder="Имя"></label>
                <label><input type="tel" name="tel" placeholder="+7  (        )      -     -   "></label>
                <button type="submit" class="button orange arrow">Отправить</button>
            </form>
        </section>
        <div class="decor i1"></div>
        <div class="decor i2"></div>
    </div>
</div>
<div class="imageBox">
    <canvas id="imageBox"></canvas>
    <div class="wrapper">
        <div class="close">
            <div class="cross"></div>
        </div>
        <div class="content-image">
            <img src="img/portfolio/1.jpg" alt="">
        </div>
        <div class="arrow left">
            <img src="img/portfolio/arrow-left.png" alt="">
        </div>
        <div class="arrow right">
            <img src="img/portfolio/arrow-right.png" alt="">
        </div>
    </div>
</div>
<aside class="mainMenu">
    <div class="wrapper">
        <div class="closeMobile">
            <svg viewBox="0,0 12,12">
                <path d="M0,0 12,12 M0,12 12,0" ></path>
            </svg>
        </div>
        <div class="gradiLine"></div>
        <div class="logo">
            <a href="/"><img src="img/logo.png" alt=""></a>
        </div>
        <div class="navigation">
            <nav class="main">
                <ul>
                    <li>
                        <a href=""><span>О компании</span></a>
                    </li>
                    <li>
                        <a href="" data-menu="uslugi"><span>Услуги -></span></a>
                    </li>
                    <li>
                        <a href=""><span>Системы управления (АСУ ТП)</span></a>
                    </li>
                    <li>
                        <a href="" class="active"><span>Сварка</span></a>
                    </li>
                    <li>
                        <a href=""><span>Обслуживание машин</span></a>
                    </li>
                    <li>
                        <a href=""><span>Паллетирование</span></a>
                    </li>
                    <li>
                        <a href=""><span>Готовые решения</span></a>
                    </li>
                    <li>
                        <a href=""><span>Индивидуальные проекты</span></a>
                    </li>
                    <li>
                        <a href=""><span>Портфолио</span></a>
                    </li>
                    <li>
                        <a href=""><span>Контакты</span></a>
                    </li>
                </ul>
            </nav>
            <nav class="slideMenu uslugi hide">
                <div class="backToMenu">
                    Назад в меню
                </div>
                <ul>
                    <li>
                        <a href=""><span>Сварка</span></a>
                    </li>
                    <li>
                        <a href=""><span>Обслуживание станков</span></a>
                    </li>
                    <li>
                        <a href=""><span>Паллетирование</span></a>
                    </li>
                </ul>
            </nav>
        </div>
        <!--<div class="block calc">-->
        <!--<button class="button orange calc">Калькулятор</button>-->
        <!--</div>-->
        <div class="block contact">
            <a href="tel:88005005072" class="tel no-ajax">8 800 500 50 72</a>
            <a href="" class="button blue openModal no-ajax" data-modal="mainModal">Отправить заявку</a>
            <p class="pod">Получите расчет по вашим параметрам</p>
        </div>
    </div>
</aside>
<section class="mainSection">
    <div class="burger">
        <svg viewBox="0,0 18,14">
            <path d="M0,0 18,0 M0,7 18,7 M0,14 18,14"></path>
        </svg>
    </div>
    <div class="oldContent">
        <div class="wrapper wrapperMain refreshAnimation">
            <div class="mainSlider contacts">
                <header>
                    <div class="breadCrumbs fade translateBottom"><a href="">Главная</a><span class="delimer">></span><a href="">Внедренные проекты</a></div>
                </header>
                <div class="slide planet" style="background-image: url(img/portfolio/inner-porfolio.jpg);">
                    <div class="headTitle">
                        <h1 class="fade translateBottom">Мебельное производство</h1>
                        <div class="line"></div>
                        <p class="fade translateBottom delay1">Линия упаковки мебельных заготовок</p>
                    </div>
                </div>
                <div class="bottomLine"></div>
            </div>
            <div class="calculator">
                <div class="top">
                    <h3>Расчет стоимости и сроков реализации проекта</h3>
                    <div class="params">
                        Параметры
                    </div>
                </div>
                <div class="bottom">
                    <p class="order">Примерная стоимость</p>
                    <p class="numbers">1 500 000 Р</p>
                    <p class="duration">2,5 месяца</p>
                    <a href="" class="button ultraBlue piu no-ajax">Прочитать подробнее</a>
                </div>
            </div>
        </div>
        <div class="wrapper page-innerPortfolio">
            <header>
                <div class="zakaz">
                    <div class="caption">заказчик</div>
                    <p>Производитель (современной и качественной корпусной,
                        кухонной и детской мебели(на юге россии)</p>
                </div>
                <div class="desc">
                    <div class="caption">Описание</div>
                    <div class="desc-left">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci alias aperiam blanditiis dolore dolores eveniet expedita, hic, illo inventore
                        iste minima molestiae nam nostrum, quidem repellat repellendus reprehenderit tempore. Amet mollitia obcaecati quam ratione. At est
                        nesciunt praesentium quia repudiandae.
                    </div>
                </div>
            </header>
            <section>
                <div class="item-wrapper image">
                    <a class="item" style="background-image: url(img/portfolio/1.jpg)">
                        <img src="img/portfolio/1.jpg" alt="">
                    </a>
                </div>
                <div class="item-wrapper video">
                    <a class="item">
                        <img src="img/portfolio/1.jpg" alt="">
                    </a>
                    <div class="caption-video">Описание видео не очень большое, но информативное</div>
                </div>
                <div class="item-wrapper image">
                    <a class="item" style="background-image: url(img/portfolio/2.jpg)">
                        <img src="img/portfolio/2.jpg" alt="">
                    </a>
                </div>
                <div class="item-wrapper video">
                    <a class="item">
                        <img src="img/portfolio/1.jpg" alt="">
                    </a>
                    <div class="caption-video">Описание  видео не очень очень большое, но информативное</div>
                </div>
                <div class="item-wrapper image">
                    <a class="item" style="background-image: url(img/portfolio/1.jpg)">
                        <img src="img/portfolio/1.jpg" alt="">
                    </a>
                </div>
                <div class="item-wrapper video">
                    <a class="item">
                        <img src="img/portfolio/2.jpg" alt="">
                    </a>
                    <div class="caption-video">Описание видео не очень очень сильно большое, но информативное</div>
                </div>
            </section>
        </div>
        <div class="wrapper wrapperAction innerPortfolio">
            <canvas id="actionCanvas"></canvas>
            <header>
                <h3 class="orange">Косультация специалиста</h3>
                <p>Оставьте заявку и мы свяжемся с вами в ближайшее время</p>
            </header>
            <form action="">
                <input type="text" name="name" placeholder="Имя">
                <input type="tel" name="tel" placeholder="+7  (      )      -    -    " required>
                <input type="file" name="file">
                <button type="submit" class="button orange piu">Отправить</button>
            </form>
            <div class="man">
                <img src="img/vasya_obrez.png" alt="">
            </div>
            <div class="kuka">
                <img src="img/kuka_obrez.png" alt="">
            </div>
        </div>
        <footer>
            <div class="content">
                <div class="block">
                    <a href="tel:88005005072" class="tel no-ajax">8 800 500 50 72</a>
                    <a href="" class="button orange openModal no-ajax" data-modal="mainModal">Отправить заявку</a>
                </div>
                <div class="block">
                    <nav>
                        <ul>
                            <li>
                                <a href="">Услуги</a>
                            </li>
                            <li>
                                <a href="">О компании</a>
                            </li>
                            <li>
                                <a href="">Паллетирование</a>
                            </li>
                            <li>
                                <a href="">Системы управления</a>
                            </li>
                            <li>
                                <a href="">Сварка</a>
                            </li>
                            <li>
                                <a href="">Механическая обработка</a>
                            </li>
                            <li>
                                <a href="">Индивидуальные проекты</a>
                            </li>
                            <li>
                                <a href="">Готовые решения</a>
                            </li>
                            <li>
                                <a href="">Портфолио</a>
                            </li>
                            <li>
                                <a href="">Контакты</a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </footer>
    </div>
   <div class="newContent"></div>
</section>

<script src="https://cdn.jsdelivr.net/lodash/4.17.4/lodash.min.js"></script>
<script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>
<script src="js/greensock-js/greensock-js/src/minified/TweenMax.min.js"></script>
<script src="js/greensock-js/greensock-js/src/minified/plugins/CSSRulePlugin.min.js"></script>
<script src="js/greensock-js/greensock-js/src/minified/plugins/ScrollToPlugin.min.js"></script>
<script src="dist/build.js"></script>
</body>
</html>