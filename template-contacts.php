<?php /* Template Name: Страница контактов */ get_header(); ?>
<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/owl.carousel.css">
<div class="wrapper wrapperMain refreshAnimation">
    <div class="mainSlider contacts">
        <header>
            <div class="breadCrumbs fade translateBottom"><a href="">Главная</a><span class="delimer">></span><a href="">Контакты</a></div>
        </header>
        <div class="slide planet" style="background-image: url(<?php echo get_template_directory_uri(); ?>/img/planet.jpg);">
            <div class="headTitle">
                <h1 class="fade translateBottom">Контактная информация</h1>
                <div class="line"></div>
                <p class="fade translateBottom delay1">Ready Robot всегда на связи</p>
            </div>
        </div>
        <div class="bottomLine"></div>
    </div>
    <div class="calculator">
        <div class="top">
            <h3>Расчет стоимости и сроков реализации проекта</h3>
            <div class="params">
                Параметры
            </div>
        </div>
        <div class="bottom">
            <p class="order">Примерная стоимость</p>
            <p class="numbers">1 500 000 Р</p>
            <p class="duration">2,5 месяца</p>
            <a href="" class="button ultraBlue piu no-ajax">Прочитать подробнее</a>
        </div>
    </div>
</div>
<div class="wrapper pageContacts">
    <div class="contacts">
        <div class="contact-info">
            <h4 class="orange">Контактная информация</h4>
            <div class="info">
                <p class="text-mini">Бесплатная линия</p>
                <p>8 800 550 30 74</p>
            </div>
            <div class="info">
                <p class="text-mini">Отдел продаж</p>
                <p>+7-922-014-49-77</p>
                <p>+7-951-794-85-57</p>
                <span>sale@readyrobot.ru</span>
            </div>
            <div class="info">
                <p class="text-mini">Сервисный отдел</p>
                <p>+7-922-758-94-70</p>
                <span>service@readyrobot.ru</span>
            </div>
        </div>
        <div class="contact-info">
<!--            <h4 class="orange">Реквизиты</h4>-->
<!--            <div class="info">-->
<!--                <p>ООО «Рэди Робот»</p>-->
<!--            </div>-->
<!--            <div class="info">-->
<!--                <p class="text-mini">ИНН</p>-->
<!--                <span>7449128908</span>-->
<!--            </div>-->
<!--            <div class="info">-->
<!--                <p class="text-mini">КПП</p>-->
<!--                <span>745301001</span>-->
<!--            </div>-->
            <a href="/wp-content/uploads/2017/05/Реквизиты-Ready-Robot.pdf" class="button orange no-ajax">скачать реквизиты</a>
        </div>
    </div>
    <div class="map-contacts">
        <div class="united-text">
            <p>Работаем по всей России</p>
            <span>454080, Челябинска область, г. Челябинск, ул. Тернопольская, дом 6, офис 210</span>
        </div>
        <div id="map"></div>
        <canvas id="mapCanvas"></canvas>
    </div>
</div>
<div class="wrapper pagePartners">
    <canvas id="canvasPartners"></canvas>
    <header>
        <h3 class="orange">Наши партнеры</h3>
    </header>
    <div class="partners owl-carousel">
        <div class="partner">
            <img src="<?php echo get_template_directory_uri(); ?>/img/advaLogo1.png" alt="">
        </div>
        <div class="partner">
            <img src="<?php echo get_template_directory_uri(); ?>/img/advaLogo2.png" alt="">
        </div>
        <div class="partner">
            <img src="<?php echo get_template_directory_uri(); ?>/img/advaLogo3.png" alt="">
        </div>
        <div class="partner">
            <img src="<?php echo get_template_directory_uri(); ?>/img/advaLogo4.png" alt="">
        </div>
        <div class="partner">
            <img src="<?php echo get_template_directory_uri(); ?>/img/advaLogo5.png" alt="">
        </div>
        <div class="partner">
            <img src="<?php echo get_template_directory_uri(); ?>/img/advaLogo6.png" alt="">
        </div>
    </div>
    <div class="arrows">
        <div class="arrow left"><img src="<?php echo get_template_directory_uri(); ?>/img/portfolio/arrow-left.png" alt=""></div>
        <div class="arrow right"><img src="<?php echo get_template_directory_uri(); ?>/img/portfolio/arrow-right.png" alt=""></div>
    </div>
</div>
<div class="wrapper wrapperAction">
    <canvas id="actionCanvas"></canvas>
    <header>
        <h3 class="orange">Косультация специалиста</h3>
        <p>Оставьте заявку и мы свяжемся с вами в ближайшее время</p>
    </header>
    <form action="">
        <div class="wrapinput"><input type="text" name="name" placeholder="Имя"></div>
        <div class="wrapinput"><input type="tel" name="tel" placeholder="+7  (      )      -    -    " required></div>
        <input type="file" name="file">
        <button type="submit" class="button orange piu">Отправить</button>
    </form>
    <div class="lineAction"></div>
    <div class="man">
        <img src="<?php echo get_template_directory_uri(); ?>/img/vasya_obrez.png" alt="">
    </div>
    <div class="kuka">
        <img src="<?php echo get_template_directory_uri(); ?>/img/kuka_obrez.png" alt="">
    </div>
</div>


<?php get_footer(); ?>
