           <footer>
            <div class="content">
                <div class="block">
                    <a href="tel:88005503074" class="tel no-ajax">8 800 550 30 74</a>
                    <a href="" class="button orange openModal no-ajax" data-modal="mainModal">Отправить заявку</a>
                </div>
                <div class="block">
                    <nav>
                        <ul>
                            <li>
                                <a href="/about/">О компании</a>
                            </li>
                            <li>
                                <a href="/">Сварка</a>
                            </li>
                            <li>
                                <a href="/">Системы управления</a>
                            </li>
                            <li>
                                <a href="/">Паллетирование</a>
                            </li>
                            <li>
                                <a href="/portfolio/">Портфолио</a>
                            </li>
                            <li>
                                <a href="/contacts/">Контакты</a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </footer>
    </div>
    <div class="newContent"></div>
</section>

<!--<script src="https://cdn.jsdelivr.net/lodash/4.17.4/lodash.min.js"></script>-->
<script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js" class="jq"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/greensock-js/greensock-js/src/minified/TweenMax.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/greensock-js/greensock-js/src/minified/plugins/CSSRulePlugin.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/greensock-js/greensock-js/src/minified/plugins/ScrollToPlugin.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/dist/common.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/dist/router.js"></script>
</body>
</html>