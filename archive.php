<?php get_header(); ?>
<div class="wrapper wrapperMain refreshAnimation">
    <div class="mainSlider contacts">
        <header>
            <?php if (function_exists('dimox_breadcrumbs')) dimox_breadcrumbs(); ?>
        </header>
        <div class="slide planet" style="background-image: url(<?php echo get_template_directory_uri(); ?>/img/portfolio/banner_kopia_2.jpg);">
            <div class="headTitle">
                <h1 class="fade translateBottom">Внедренные проекты</h1>
                <div class="line"></div>
                <p class="fade translateBottom delay1">Ready robot Ready worker</p>
            </div>
        </div>
        <div class="bottomLine"></div>
    </div>
    <div class="calculator">
        <div class="top">
            <h3>Расчет стоимости и сроков реализации проекта</h3>
            <div class="params">
                Параметры
            </div>
        </div>
        <div class="bottom">
            <p class="order">Примерная стоимость</p>
            <p class="numbers">1 500 000 Р</p>
            <p class="duration">2,5 месяца</p>
            <a href="" class="button ultraBlue piu no-ajax">Прочитать подробнее</a>
        </div>
    </div>
</div>
<div class="wrapper pagePortfolio">

            <?php if (have_posts()): while (have_posts()) : the_post(); ?>
            <div class="portfolios">
                <div class="project-portfolio">
                    <!-- post thumbnail -->
                    <?php if ( has_post_thumbnail()) : ?>
                    <a href="<?php the_permalink(); ?>"><div class="bg-port" style="background-image: url(<?php echo get_the_post_thumbnail_url(); ?>"></div></a>
                    <div class="desc-port">
                        <div class="short_text"><?php $short_text = get_the_title(); echo $short_text; ?></div>
                    </div>
                    <a href="<?php the_permalink(); ?>" class="button orange podrobnee">подробнее</a>
                </div>

            </div>
                    <?php endif; ?>
            <?php endwhile; ?>

            <?php else: ?>

                <!-- article -->
                <article>
                    <h2><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>
                </article>
                <!-- /article -->

            <?php endif; ?>
</div>
<div class="wrapper wrapperAction">
    <canvas id="actionCanvas"></canvas>
    <header>
        <h3 class="orange">Консультация специалиста</h3>
        <p>Оставьте заявку, и мы свяжемся с вами в ближайшее время</p>
    </header>
    <form action="">
        <div class="wrapinput"><input type="text" name="name" placeholder="Имя"></div>
        <div class="wrapinput"><input type="tel" name="tel" placeholder="+7  (      )      -    -    " required></div>
        <input type="file" name="file">
        <button type="submit" class="button orange piu">Отправить</button>
    </form>
    <div class="lineAction"></div>
    <div class="man">
        <img src="<?php echo get_template_directory_uri(); ?>/img/vasya_obrez.png" alt="">
    </div>
    <div class="kuka">
        <img src="<?php echo get_template_directory_uri(); ?>/img/kuka_obrez.png" alt="">
    </div>
</div>


<?php get_footer(); ?>
