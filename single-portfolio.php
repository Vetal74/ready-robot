<?php get_header(); the_post();?>

<?php
    $main_image = get_field('main_image')['url'];
    $short_title = get_field('short_title');
    $short_desc = get_field('short-desc');
    $videos = get_field('video');
    $sostav = get_field('sostav');
?>
<div class="wrapper wrapperMain refreshAnimation">
    <div class="mainSlider contacts">
        <header>
            <?php if (function_exists('dimox_breadcrumbs')) dimox_breadcrumbs(); ?>
        </header>
        <div class="slide planet" style="background-image: url(<?php echo $main_image; ?>);">
            <div class="headTitle">
                <h1 class="fade translateBottom">
                    <?php
                        if($short_title) echo $short_title;
                        else the_title();
                    ?>
                </h1>
                <div class="line"></div>
                <p class="fade translateBottom delay1">
                    <?php
                        if($short_desc) echo $short_desc;
                    ?>
                </p>
            </div>
        </div>
        <div class="bottomLine"></div>
    </div>
    <div class="calculator">
        <div class="top">
            <h3>Расчет стоимости и сроков реализации проекта</h3>
            <div class="params">
                Параметры
            </div>
        </div>
        <div class="bottom">
            <p class="order">Примерная стоимость</p>
            <p class="numbers">1 500 000 Р</p>
            <p class="duration">2,5 месяца</p>
            <a href="" class="button ultraBlue piu no-ajax">Прочитать подробнее</a>
        </div>
    </div>
</div>
<div class="wrapper page-innerPortfolio">
    <header>
        <div class="zakaz">
            <div class="caption">заказчик</div>
            <p><?php $zakaz = get_field('zakaz');
                echo $zakaz;
            ?></p>
        </div>
        <?php if($sostav) { ?>
            <div class="sostav">
                <div class="caption">Состав комплекса</div>
                <div class="desc-left">
                    <?php echo $sostav; ?>
                </div>
            </div>
        <?php } ?>
        <div class="desc">
            <div class="caption">Описание</div>
            <div class="desc-left">
                <?php the_content(); ?>
            </div>
        </div>
    </header>
    <section>
        <?php
        $images = get_field('gallery');
        if( $images ): ?>
            <?php foreach( $images as $image ):
                if($image['type'] == 'image') { ?>
                    <div class="item-wrapper image">
                        <a href="<?php echo $image['url']; ?>" class="item no-ajax" style="background-image: url(<?php echo $image['url']; ?>" data-imagebox="<?php echo $image['url']; ?>"></a>
                    </div>
                <?php } else { ?>
                    <div class="item-wrapper video">
                        <a href="<?php echo $image['url']; ?>" class="item no-ajax" data-imagebox="https://www.youtube.com/embed/WFEKe9WAPrM">
                            <video src="<?php echo $image['url']; ?>"></video>
                        </a>
                        <div class="caption-video"><?php echo $image['description'];  ?></div>
                    </div>
                <?php } ?>
            <?php endforeach; ?>
        <?php endif; ?>
        <?php
            if($videos) {
                foreach ($videos as $video) { ?>
                    <div class="item-wrapper video">
                        <a href="<?php echo $video['link-to-video']; ?>" class="item no-ajax" data-imagebox="<?php echo $video['link-to-video']; ?>">
                            <img src="http://img.youtube.com/vi/<?php echo str_replace('embed/', '', stristr($video['link-to-video'], 'embed/')); ?>/mqdefault.jpg" alt="<?php echo $video['video_desc']; ?>">
                        </a>
                        <div class="caption-video"><?php echo $video['video_desc']; ?></div>
                    </div>
                <?php }
            }
        ?>
    </section>
</div>
<div class="wrapper wrapperAction innerPortfolio">
    <canvas id="actionCanvas"></canvas>
    <header>
        <h3 class="orange">Консультация специалиста</h3>
        <p>Оставьте заявку, и мы свяжемся с вами в ближайшее время</p>
    </header>
    <form action="">
        <div class="wrapinput"><input type="text" name="name" placeholder="Имя"></div>
        <div class="wrapinput"><input type="tel" name="tel" placeholder="+7  (      )      -    -    " required></div>
        <input type="file" name="file">
        <button type="submit" class="button orange piu">Отправить</button>
    </form>
    <div class="lineAction"></div>
    <div class="man">
        <img src="<?php echo get_template_directory_uri(); ?>/img/vasya_obrez.png" alt="">
    </div>
    <div class="kuka">
        <img src="<?php echo get_template_directory_uri(); ?>/img/kuka_obrez.png" alt="">
    </div>
</div>

<?php get_footer(); ?>
