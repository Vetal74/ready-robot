<?php /* Template Name: Страница портфолио */ get_header(); ?>
        <div class="wrapper wrapperMain refreshAnimation">
            <div class="mainSlider contacts">
                <header>
                    <?php if (function_exists('dimox_breadcrumbs')) dimox_breadcrumbs(); ?>
                </header>
                <div class="slide planet" style="background-image: url(img/portfolio/porfolio.jpg);">
                    <div class="headTitle">
                        <h1 class="fade translateBottom">Внедренные проекты</h1>
                        <div class="line"></div>
                        <p class="fade translateBottom delay1">Ready Robot всегда на связи</p>
                    </div>
                </div>
                <div class="bottomLine"></div>
            </div>
            <div class="calculator">
                <div class="top">
                    <h3>Расчет стоимости и сроков реализации проекта</h3>
                    <div class="params">
                        Параметры
                    </div>
                </div>
                <div class="bottom">
                    <p class="order">Примерная стоимость</p>
                    <p class="numbers">1 500 000 Р</p>
                    <p class="duration">2,5 месяца</p>
                    <a href="" class="button ultraBlue piu no-ajax">Прочитать подробнее</a>
                </div>
            </div>
        </div>
        <div class="wrapper pagePortfolio">
            <div class="portfolios">
                <div class="project-portfolio">
                    <div class="bg-port" style="background-image: url(img/portfolio/1.jpg)"></div>
                    <div class="desc-port">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Recusandae, reiciendis.</p>
                        <a href="/innerPortfolio.php" class="button orange">подробнее</a>
                    </div>
                </div>
            </div>
            <div class="portfolios">
                <div class="project-portfolio">
                    <div class="bg-port" style="background-image: url(img/portfolio/1.jpg)"></div>
                    <div class="desc-port">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Recusandae, reiciendis.</p>
                        <a href="/innerPortfolio.php" class="button orange">подробнее</a>
                    </div>
                </div>
            </div>
            <div class="portfolios">
                <div class="project-portfolio">
                    <div class="bg-port" style="background-image: url(img/portfolio/1.jpg)"></div>
                    <div class="desc-port">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Recusandae, reiciendis.</p>
                        <a href="/innerPortfolio.php" class="button orange">подробнее</a>
                    </div>
                </div>
            </div>
            <div class="portfolios">
                <div class="project-portfolio">
                    <div class="bg-port" style="background-image: url(img/portfolio/1.jpg)"></div>
                    <div class="desc-port">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Recusandae, reiciendis.</p>
                        <a href="/innerPortfolio.php" class="button orange">подробнее</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="wrapper wrapperAction">
            <canvas id="actionCanvas"></canvas>
            <header>
                <h3 class="orange">Косультация специалиста</h3>
                <p>Оставьте заявку и мы свяжемся с вами в ближайшее время</p>
            </header>
            <form action="">
                <input type="text" name="name" placeholder="Имя">
                <input type="tel" name="tel" placeholder="+7  (      )      -    -    " required>
                <input type="file" name="file">
                <button type="submit" class="button orange piu">Отправить</button>
            </form>
            <div class="man">
                <img src="img/vasya_obrez.png" alt="">
            </div>
            <div class="kuka">
                <img src="img/kuka_obrez.png" alt="">
            </div>
        </div>

<?php get_footer(); ?>
