<?php /* Template Name: Обслуживание машин */ get_header(); ?>

<?php
$args = array(
    'post_type'=> 'portfolio',
    'category' => '',
    'posts_per_page' => -1,
    'offset'=> 0,
    'post_status' => 'publish'
);

$port = get_posts($args);
?>
    <div class="wrapper wrapperMain refreshAnimation service">
            <div class="mainSlider">
                <header>
                    <?php if (function_exists('dimox_breadcrumbs')) dimox_breadcrumbs(); ?>
                </header>
                <div class="slide" style="background-image: url(<?php echo get_template_directory_uri(); ?>/img/service_machine/fon.jpg);">
                    <div class="kuka" data-zindex="50"></div>
                    <div class="iscr1"></div>
                    <div class="iscr2"></div>
                    <div class="iscr3"></div>
                    <div class="headTitle service">
                        <h1 class="fade translateBottom">Решения для обслуживания машин</h1>
                        <div class="line"></div>
                        <p class="fade translateBottom delay1">Токарные, фрезерные, гибочные станки, прессы, формовочные машины</p>
                    </div>
                </div>
                <div class="bottomLine"></div>
            </div>
            <div class="calculator">
                <div class="top">
                    <h3>Расчет стоимости и сроков реализации проекта</h3>
                    <div class="params">
                        Параметры
                    </div>
                </div>
                <div class="bottom">
                    <p class="order">Примерная стоимость</p>
                    <p class="numbers">1 500 000 Р</p>
                    <p class="duration">2,5 месяца</p>
                    <a href="" class="button ultraBlue piu no-ajax">Прочитать подробнее</a>
                </div>
            </div>
        </div>
        <div class="wrapper wrapperDevelop service">
            <canvas id="developCanvas" style="width: 100%;height: 100%;"></canvas>
            <div class="tabs">
                <div class="tab tab1" data-tab="tab1">
                    <a href="" class="no-ajax">Токарно-фрезерная группа</a>
                </div>
                <div class="tab tab2" data-tab="tab2">
                    <a href="" class="no-ajax">Гибочное оборудование</a>
                </div>
                <div class="tab tab3" data-tab="tab3">
                    <a href="" class="no-ajax">Прессы и формовочные машины</a>
                </div>
                <div class="karetka hidden"></div>
            </div>
            <div class="tabsContent" data-tab="tab1">
                <div class="leftSide service">
                </div>
                <div class="rightSide service">
                    <div class="desc">
                        <h3>Токарно-фрезерная группа</h3>
                        <p>Применение роботов в процессах механической обработки и обслуживания станков ЧПУ значительно сказывается на повышении производительности, снижении брака, освобождении рабочего от тяжелого монотонного труда, повышении технологичности и гибкости процессов и как следствие снижении себестоимости и увеличении прибыли производства.</p>
                        <p>Существует большое количество операций, которые могут эффективно выполнять гибкие роботизированные ячейки в зависимости от задач и использования различных инструментов.</p>
                        <a href="" class="button orange openModal no-ajax" data-modal="tokar">Подробнее</a>
                    </div>
                </div>
            </div>
            <div class="tabsContent" data-tab="tab2">
                <div class="leftSide service tab2">
                </div>
                <div class="rightSide service">
                    <div class="desc">
                        <h3>Гибочное оборудование</h3>
                        <p>Современная роботизированная гибка металла является полностью автоматическим процессом и имеет следующие преимущества:</p>
                        <ul class="galochki">
                            <li>высокий уровень производительности;</li>
                            <li>стабильно высокая повторяемость изготовления;</li>
                            <li>возможность программирования оффлайн;</li>
                            <li>минимальное время переналадки с одного изделия на другое;</li>
                            <li>единое управление роботом и прессом посредством контроллера робота;</li>
                        </ul>
                        <a href="" class="button orange openModal no-ajax" data-modal="gib">Подробнее</a>
                    </div>
                </div>
            </div>
            <div class="tabsContent" data-tab="tab3">
                <div class="leftSide service tab3">
                </div>
                <div class="rightSide service">
                    <div class="desc">
                        <h3>Прессы и формовочные машины</h3>
                        <p>Работа с прессами и формовочными машинами – это трудоемкая и ответственная операция, которая при использовании ручного труда требует большого умения, аккуратности и высокой квалификации рабочего-формовщика. Многие вспомогательные процессы литейного производства, такие как складирование и своевременная подача инструмента требуют только надлежащего исполнения.</p>
                        <p>Роботизация процесса в совокупности с автоматизацией измерений позволяет создать гибкое производство, эффективное как для крупных, так и для мелких серий.</p>
                        <a href="" class="button orange openModal no-ajax" data-modal="press">Подробнее</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="wrapper wrapperProfit">
            <canvas id="profitCanvas"></canvas>
            <header>
                <h3 class="orange swapWrapper">
                    <span class="swapTab" data-tab="all">Наши преимущества </span>
                </h3>
                <p class="swapWrapper"><span class="swapTab" data-tab="all">Перевод на новый уровень развития производства</span></p>
            </header>

            <div class="advaWrap swapWrapper">
                <div class="advantages first swapTab" data-tab="tab1">
                    <div class="advaBlock block1 default" data-block="block1">
                        <div class="icon">
                            <svg viewBox="0 0 53.92 59.41">
                                <path d="M52.92,5.63V53.78a4.64,4.64,0,0,1-4.63,4.63H5.63A4.64,4.64,0,0,1,1,53.78V5.63A4.64,4.64,0,0,1,5.63,1H48.29a4.64,4.64,0,0,1,4.63,4.63M6.05,36.15H17.77M22.45,35v2.34M31.83,49v2.34m0-7V46.7m0-7V42m0-7v2.34M27.14,49v2.34m0-7V46.7m0-7V42m0-7v2.34M22.45,49v2.34m0-7V46.7m0-7V42M41.2,6.86h7.24m-3.62,3.51h3.62M36.51,45.52H48.23M36.51,40.84H48.23M36.51,36.15H48.23m-1.17,7v4.69m-4.69-9.37v4.69m-4.69-9.37v4.69M48.23,50.21H43.54m-2.34,0H36.51M41.2,26.78h3.62M41.2,23.26h7.24M41.2,19.75h7.24M6.05,50.21H17.77m-11.72-7H17.77m-8.2,4.69v4.69m4.69-11.72v4.69M8.12,6.27a2,2,0,1,1-2,2,2,2,0,0,1,2-2Zm0,10.55a2,2,0,1,1-2,2A2,2,0,0,1,8.12,16.82Z"/>
                                <path d="M36.51,8.13V27.85a1.87,1.87,0,0,1-1.86,1.86H17.51a1.87,1.87,0,0,1-1.86-1.86V8.13a1.87,1.87,0,0,1,1.86-1.86H34.65a1.87,1.87,0,0,1,1.86,1.86M31.3,11.54V25.26a.94.94,0,0,1-.93.93H21.79a.93.93,0,0,1-.93-.93v-10c.3.06-.54.09.93.09s4.76-2.13,4.76-4.76h3.81a.94.94,0,0,1,.93.93m-3.57,10A1.76,1.76,0,1,1,26,19.75a1.76,1.76,0,0,1,1.76,1.76"/>
                            </svg>
                        </div>
                        <div class="description">
                            <span>Интеграция роботов с различными ЧПУ</span>
                        </div>
                    </div>
                    <div class="advaBlock block2" data-block="block2">
                        <div class="icon">
                            <svg viewBox="0 0 81.91 84.56">
                                <path d="M63.38,32.22l-8.92,8.92-1.8-1.8M40.74,27.42l-1.87-1.87,8.92-8.92m33.12,4.46L66.64,35.35,44,13.56,56.53,1M34.82,29.6l-.75,16.34,16.34-.75,2.25-5.85L40.74,27.42Z"/>
                                <path d="M3.11,28.27a38.63,38.63,0,0,1,37.12,38.6,38.48,38.48,0,0,1-3.79,16.7m3.19-9.89s.1-3.51,4.6-3M38,80.17a6.23,6.23,0,0,1,4.73-4M40.21,65.91a4.85,4.85,0,0,1,3-4.74m-3.63-1.3s-.46-4.89,1-6.39M1,40.45h.59A26.43,26.43,0,0,1,28,66.87a26.31,26.31,0,0,1-3.35,12.88"/>
                            </svg>
                        </div>
                        <div class="description">
                            <span>Обработка после станка</span>
                        </div>
                    </div>
                    <div class="advaBlock block3" data-block="block3">
                        <div class="icon">
                            <svg viewBox="0 0 75 70.38">
                                <path d="M6.8,6.5H39.53a4.71,4.71,0,0,1,4.7,4.7v2.93H30.76a1.54,1.54,0,0,0-1.53,1.53v8.69a1.54,1.54,0,0,0,1.53,1.53H44.22V37.8a4.71,4.71,0,0,1-4.7,4.7H2.65C.57,34.24.06,24.9,3.48,16.36A85.63,85.63,0,0,0,6.8,6.5M18.22,24.88a6,6,0,1,1-6,6,6,6,0,0,1,6-6ZM37,30.38a2.25,2.25,0,1,1-2.25,2.25A2.25,2.25,0,0,1,37,30.38Z"/>
                                <path d="M66.72,60.38l-2-52.5L54.22,1M59.5,60.38v6.88a2.12,2.12,0,0,0,2.12,2.12M74,56.63V28.49a2.12,2.12,0,0,0-2.12-2.12H65.43m-11.21,30h3.5m-3.5-5h3.5m-3.5-5h3.5m-3.5-5h3.5m-3.5-5h3.5m-3.5-5h3.5m-3.5-5h3.5m-3.5-5h3.5m-3.5-5h3.5m-3.5-5h3.5M54.22,6.5v36m0,5.5V60.38h12.5M14.85,6.5H54.22V1L21.72,3,14.85,6.5Zm20,36H54.22V48l-12.5-2Z"/>
                            </svg>
                        </div>
                        <div class="description">
                            <span>Контроль и измерение</span>
                        </div>
                    </div>
                    <div class="advaBlock block4" data-block="block4">
                        <div class="icon">
                            <svg viewBox="0 0 45.62 56.53">
                                <path d="M22.81,37A15.73,15.73,0,1,1,38.54,21.23,15.73,15.73,0,0,1,22.81,37m2.76-20.5a5.5,5.5,0,0,1,2.72,4.16M27.59,24a5.51,5.51,0,0,1-7.53,2m2.76,5a9.76,9.76,0,1,1,9.76-9.76A9.76,9.76,0,0,1,22.81,31"/>
                                <rect x="1" y="1" width="43.62" height="54.53" rx="6.31" ry="6.31"/>
                            </svg>
                        </div>
                        <div class="description">
                            <span>Техническое зрение</span>
                        </div>
                    </div>
                    <div class="advaBlock block5" data-block="block5">
                        <div class="icon">
                            <svg viewBox="0 0 58.35 47.5">
                                <path d="M57.31,35H1M57.31,46.5V16M1,41.91H57.35M1,46.5V16m56.31,0H1"/>
                                <path d="M5.85,1h13V14h-13V1m17,19h13V33h-13V20m20,10.1L40,33m2.9-10.1L40,20M50,30.1l2.9,2.9M50,22.9l2.9-2.9m-13,0h13V33h-13V20m-14,10.1L23,33m2.9-10.1L23,20M33,30.1l2.9,2.9M33,22.9l2.9-2.9m7.05-8.9L40,14m2.9-10.1L40,1M50,11.1l2.9,2.9M50,3.9,52.88,1m-13,0h13V14h-13V1m-14,10.1L23,14m2.9-10.1L23,1M33,11.1l2.9,2.9M33,3.9,35.88,1m-13,0h13V14h-13V1m-14,10.1L6,14M8.88,3.9,6,1M16,11.1l2.9,2.9M16,3.9,18.85,1M8.75,3.9H16v7.2H8.75V3.9Zm17,19H33v7.2h-7.2V22.9Zm17,0H50v7.2h-7.2V22.9Zm0-19H50v7.2h-7.2V3.9Zm-17,0H33v7.2h-7.2Z"/>
                            </svg>
                        </div>
                        <div class="description">
                            <span>Стеллажи и конвейерные системы</span>
                        </div>
                    </div>
                    <div class="advaBlock block6" data-block="block6">
                        <div class="icon">
                            <svg viewBox="0 0 62.12 52.55">
                                <path d="M3,1H59.16a2,2,0,0,1,2,2V36a2,2,0,0,1-2,2H54.37M21.94,48.39H40.17V46.81m-13.44-9H21.94V48.39m-7.45,0H47.62v3.16H14.49V48.39M5.8,4H56.32A1.64,1.64,0,0,1,58,5.61V33.16a1.64,1.64,0,0,1-1.63,1.63h-1m-29.61,0H5.8a1.64,1.64,0,0,1-1.63-1.63V5.61A1.64,1.64,0,0,1,5.8,4M26.73,37.91H3a2,2,0,0,1-2-2V3A2,2,0,0,1,3,1"/>
                                <path d="M48.6,40.81l7.71,7.71M33.69,32.21a6.86,6.86,0,0,1,8.22-6.72m3.75,2.15a6.86,6.86,0,0,1,1.25,2m-6.2-8.57A11.07,11.07,0,1,1,29.63,32.14,11.07,11.07,0,0,1,40.7,21.07Z"/>
                            </svg>
                        </div>
                        <div class="description">
                            <span>Система мониторинга и документирования</span>
                        </div>
                    </div>
                    <div class="advaBlock block7" data-block="block7">
                        <div class="icon">
                            <svg viewBox="0 0 66.56 66.75">
                                <path d="M55.49,31.24,35.32,11.07,45.39,1,65.56,21.16,55.49,31.24m4.77-13.69-6.72,6.72L44,14.69,50.69,8"/>
                                <path d="M35.32,11.27S6.61,26.53,7.18,27.52,1,39.08,1,39.08l3,3.41,6.74-10.58,10.38.17-1.07-4.52,12.4-6.13,2.67,4.63L42.79,24l-2.1,7.67,4.63,2.67L39.19,46.7l-4.52-1.07L34.84,56,24.26,62.75l3.41,3S38.25,59,39.24,59.58,55.49,31.43,55.49,31.43M43,33.69,25.07,46.48l-6.95,6.07L14.61,49l6.2-6.94,13-17.63"/>
                            </svg>
                        </div>
                        <div class="description">
                            <span>Захватные устройства и системы инструмента</span>
                        </div>
                    </div>
                </div>
                <div class="advantages first swapTab service" data-tab="tab2">
                    <div class="advaBlock block8 default" data-block="block8">
                        <div class="icon">
                            <svg viewBox="0 0 65.77 69.89">
                                <path d="M6.39,60.27H21.51m22.54,0H59.17M5.31,66.11c1.19,0,5.22-.32,6.36-1.69.31-.37.4-.88.88-.88h2.81c.48,0,.57.51.88.88,1.14,1.38,5.17,1.69,6.36,1.69a1.13,1.13,0,0,1,1.13,1.13v.52a1.13,1.13,0,0,1-1.13,1.13H5.31a1.13,1.13,0,0,1-1.13-1.13v-.52a1.13,1.13,0,0,1,1.13-1.13Zm37.66,0c1.19,0,5.22-.32,6.36-1.69.31-.37.4-.88.88-.88H53c.48,0,.57.51.88.88,1.14,1.38,5.17,1.69,6.36,1.69a1.13,1.13,0,0,1,1.13,1.13v.52a1.13,1.13,0,0,1-1.13,1.13H43a1.13,1.13,0,0,1-1.13-1.13v-.52A1.13,1.13,0,0,1,43,66.11Z"/>
                                <path d="M12.41,51.49V39.14a.83.83,0,0,1,.83-.83H52.53a.83.83,0,0,1,.83.83V51.49M46.77,0V31.63A1.75,1.75,0,0,1,45,33.38H20.74A1.75,1.75,0,0,1,19,31.63V0m2.74,33.38v4.94m22.29,0V33.38M1.28,55.88H64.49a.28.28,0,0,0,.28-.28V51.76a.28.28,0,0,0-.28-.28H1.28a.28.28,0,0,0-.28.28V55.6A.28.28,0,0,0,1.28,55.88Z"/>
                            </svg>
                        </div>
                        <div class="description">
                            <span>Захватные устройства и магазин инструментов</span>
                        </div>
                    </div>
                    <div class="advaBlock block9" data-block="block9">
                        <div class="icon">
                            <svg viewBox="0 0 77.13 62.22">
                                <path d="M70.58,28.41v6.83M40.84,43.68V48.9H75.4V43.68h0M45.67,56.13V48.9m24.91,0v7.23M45.67,35.24V28.41m0,0H70.58L67.77,14.34H48.48L45.67,28.41Zm24.91,6.83,4.82,8.44H40.84l.16-.28,4.66-8.16H70.58Z"/>
                                <path d="M36,10.34H50.73l4.75,4H36M15,.85,30,10.1,15.69,34.66.44,27.09M36,10.34h0v4L25.7,39.41,22,38m0,0L36.13,49.84l6.19.53-16.62-11M13.75,38,32.17,6.4,36,10.34,20.64,40.59Z"/>
                                <path d="M11.11,61.22H72.66M76.54,56H7"/>
                            </svg>
                        </div>
                        <div class="description">
                            <span>Автоматическая смена ножей листогиба</span>
                        </div>
                    </div>
                    <div class="advaBlock block10" data-block="block10">
                        <div class="icon">
                            <svg viewBox="0 0 59.53 47.81">
                                <path d="M16.59,41.25a5,5,0,0,1,6.46-4.83L42.47,12.79a5,5,0,0,1-.65-2.47c0-.1,0-.2,0-.3m9.24-2.48a5,5,0,0,1-5.78,7.56L25.95,38.64a5,5,0,0,1,.73,2.61m21.63,0H10.68V43.1H6.53v3.71h8.3V45H44.15v1.85h8.3V43.1H48.3Z"/>
                                <rect x="0.83" y="8.64" width="57.88" height="4.54" transform="translate(-1.81 8.08) rotate(-15)"/>
                            </svg>
                        </div>
                        <div class="description">
                            <span>Системы перепозиционирования</span>
                        </div>
                    </div>
                    <div class="advaBlock block11" data-block="block11">
                        <div class="icon">
                            <svg viewBox="0 0 85.1 40.65">
                                <path d="M61.87,1V39.65H23.23V1H61.87M32.22,31l-8.62,8.62m8.62-30L23.61,1M53.25,31l8.62,8.62m-8.62-30L61.87,1M46.26,9.62V31m-7.81,0V9.62m14.8,0V31H31.85V9.62H53.25"/>
                                <path d="M67.23,28.57H84.1M1,12.08H17.87M11.6,17.31l6.27-5.23M11.6,6.85l6.27,5.23m60,21.72,6.28-5.23m-6.28-5.23,6.28,5.23"/>
                            </svg>
                        </div>
                        <div class="description">
                            <span>Системы загрузки, выгрузки</span>
                        </div>
                    </div>
                    <div class="advaBlock block12" data-block="block12">
                        <div class="icon">
                            <svg viewBox="0 0 67.34 68.79">
                                <path d="M18.21,22.83,44.95,49.16M34.88,39.24l-8,7.93a1.47,1.47,0,0,0,0,2.08L36,58.39l-8.09,8.09a4.52,4.52,0,0,1-6.37,0L4.9,49.85,1.16,36.41l4.59-2.88,12.46-10.7V14.61L42.91,39c3,3,2.38,7.44,1.55,9.65M5.75,33.53,4.9,49.85"/>
                                <path d="M10.25,21.92A2.92,2.92,0,1,1,13.17,19a2.92,2.92,0,0,1-2.92,2.92M53.59,41.86h13m-13,8h6.5m-6.5-4h13M45.17,32.92A2.92,2.92,0,1,1,48.09,30a2.92,2.92,0,0,1-2.92,2.92m1.59-5.36,5.32-5.69H67.34m-13.13-5.5h8.88M19.94,1h15M11.33,16.28,16.75,6.5h25"/>
                            </svg>
                        </div>
                        <div class="description">
                            <span>Offline программирования</span>
                        </div>
                    </div>
                    <div class="advaBlock block13" data-block="block13">
                        <div class="icon">
                            <svg viewBox="0 0 72.65 44.72">
                                <path d="M0,43.72H68.65m4-16.83h-9m-8.5,0H49.79l-2.64,2.5H38.77l-3.5,4h-4v-8L27,22.94l-3.29,1.9"/>
                                <path d="M37.68,4.93l6.77,3.5.87-2.14L37,1.13m26.7,42.26V30.9a.51.51,0,0,0-.51-.51H63m-3.89,0a.51.51,0,0,0-.51.51V43.39M45.76,10.25l.5,1.5,3,2.5m-3-5,4-.5,2,5m-25.6,30V38.39a1.34,1.34,0,0,0-1.33-1.33H4a1.34,1.34,0,0,0-1.33,1.33v5.33m5.51-6.66-.72-4.29m5-16.52,2.33-3.66m12.38-10L37,1.13l.72,3.8-8,3.23M20,16.39,18.69,19m.19,12.23,1.84,5.83m-4-30.67a5.33,5.33,0,1,1,2.94,6.94,5.33,5.33,0,0,1-2.94-6.94m-4.47,13A5.92,5.92,0,1,1,6.34,25.3a5.92,5.92,0,0,1,5.92-5.92M19.62,7.49a2.12,2.12,0,1,1,1.17,2.76,2.12,2.12,0,0,1-1.17-2.76Zm36.15,16.9H63a.61.61,0,0,1,.61.61v4.79a.61.61,0,0,1-.61.61H55.76a.61.61,0,0,1-.61-.61V25a.61.61,0,0,1,.61-.61ZM12.38,23.3a2.12,2.12,0,1,1-2.12,2.12A2.12,2.12,0,0,1,12.38,23.3Z"/>
                            </svg>
                        </div>
                        <div class="description">
                            <span>Роботизированная гибка труб</span>
                        </div>
                    </div>
                    <div class="advaBlock block14" data-block="block14">
                        <div class="icon">
                            <svg viewBox="0 0 62.12 52.55">
                                <path d="M3,1H59.16a2,2,0,0,1,2,2V36a2,2,0,0,1-2,2H54.37M21.94,48.39H40.17V46.81m-13.44-9H21.94V48.39m-7.45,0H47.62v3.16H14.49V48.39M5.8,4H56.32A1.64,1.64,0,0,1,58,5.61V33.16a1.64,1.64,0,0,1-1.63,1.63h-1m-29.61,0H5.8a1.64,1.64,0,0,1-1.63-1.63V5.61A1.64,1.64,0,0,1,5.8,4M26.73,37.91H3a2,2,0,0,1-2-2V3A2,2,0,0,1,3,1"/>
                                <path d="M48.6,40.81l7.71,7.71M33.69,32.21a6.86,6.86,0,0,1,8.22-6.72m3.75,2.15a6.86,6.86,0,0,1,1.25,2m-6.2-8.57A11.07,11.07,0,1,1,29.63,32.14,11.07,11.07,0,0,1,40.7,21.07Z"/>
                            </svg>
                        </div>
                        <div class="description">
                            <span>Система мониторинга и документирования</span>
                        </div>
                    </div>
                </div>
                <div class="advantages first swapTab" data-tab="tab3">
                    <div class="advaBlock block15 default" data-block="block15">
                        <div class="icon">
                            <svg viewBox="0 0 65.77 69.89">
                                <path d="M6.39,60.27H21.51m22.54,0H59.17M5.31,66.11c1.19,0,5.22-.32,6.36-1.69.31-.37.4-.88.88-.88h2.81c.48,0,.57.51.88.88,1.14,1.38,5.17,1.69,6.36,1.69a1.13,1.13,0,0,1,1.13,1.13v.52a1.13,1.13,0,0,1-1.13,1.13H5.31a1.13,1.13,0,0,1-1.13-1.13v-.52a1.13,1.13,0,0,1,1.13-1.13Zm37.66,0c1.19,0,5.22-.32,6.36-1.69.31-.37.4-.88.88-.88H53c.48,0,.57.51.88.88,1.14,1.38,5.17,1.69,6.36,1.69a1.13,1.13,0,0,1,1.13,1.13v.52a1.13,1.13,0,0,1-1.13,1.13H43a1.13,1.13,0,0,1-1.13-1.13v-.52A1.13,1.13,0,0,1,43,66.11Z"/>
                                <path d="M12.41,51.49V39.14a.83.83,0,0,1,.83-.83H52.53a.83.83,0,0,1,.83.83V51.49M46.77,0V31.63A1.75,1.75,0,0,1,45,33.38H20.74A1.75,1.75,0,0,1,19,31.63V0m2.74,33.38v4.94m22.29,0V33.38M1.28,55.88H64.49a.28.28,0,0,0,.28-.28V51.76a.28.28,0,0,0-.28-.28H1.28a.28.28,0,0,0-.28.28V55.6A.28.28,0,0,0,1.28,55.88Z"/>
                            </svg>
                        </div>
                        <div class="description">
                            <span>Захватные устройства и магазин инструментов</span>
                        </div>
                    </div>
                    <div class="advaBlock block16" data-block="block16">
                        <div class="icon">
                            <svg viewBox="0 0 81.91 84.56">
                                <path d="M63.38,32.22l-8.92,8.92-1.8-1.8M40.74,27.42l-1.87-1.87,8.92-8.92m33.12,4.46L66.64,35.35,44,13.56,56.53,1M34.82,29.6l-.75,16.34,16.34-.75,2.25-5.85L40.74,27.42Z"/>
                                <path d="M3.11,28.27a38.63,38.63,0,0,1,37.12,38.6,38.48,38.48,0,0,1-3.79,16.7m3.19-9.89s.1-3.51,4.6-3M38,80.17a6.23,6.23,0,0,1,4.73-4M40.21,65.91a4.85,4.85,0,0,1,3-4.74m-3.63-1.3s-.46-4.89,1-6.39M1,40.45h.59A26.43,26.43,0,0,1,28,66.87a26.31,26.31,0,0,1-3.35,12.88"/>
                            </svg>
                        </div>
                        <div class="description">
                            <span>Механическая обработка</span>
                        </div>
                    </div>
                    <div class="advaBlock block17" data-block="block17">
                        <div class="icon">
                            <svg viewBox="0 0 73.69 64.8">
                                <path d="M53.79,46.36a34.82,34.82,0,0,1,4.72.63c1.42.3,3.62.91,6.59,1.81,2.52.76,5.05,1.5,7.59,2.25l-7,1.62c-2.87-.83-5.74-1.66-8.58-2.5q-4.53-1.34-5.95-1.64a11.1,11.1,0,0,0-2.53-.27,16.61,16.61,0,0,0-3.08.48L44.38,49l4,3.35,4,3.35-5.89,1.35L37.16,49l-9.38-8.09m.07-1.28,0,1.11M17.2,63.8l-8.1-8.4L1,47l11.29-2.58a32,32,0,0,1,6.68-1,14.32,14.32,0,0,1,4.83.85,12,12,0,0,1,4.05,2.3c1.37,1.23,1.91,2.38,1.58,3.45s-1.63,2-3.85,2.78a30.87,30.87,0,0,1,4.51.68c1.35.32,3.4,1,6.17,1.91,2.35.8,4.72,1.58,7.1,2.37l-7,1.61q-4-1.31-8-2.63c-2.83-.94-4.68-1.51-5.58-1.72a9.75,9.75,0,0,0-2.42-.3,16.17,16.17,0,0,0-3.05.47l-1.15.26L19.6,59l3.48,3.48L17.2,63.8M41.31,46.44l4.13-.95L47.55,45a14.31,14.31,0,0,0,2.66-.84,1.1,1.1,0,0,0,.7-1,2.1,2.1,0,0,0-1-1.32,6.18,6.18,0,0,0-2.39-1.12,8,8,0,0,0-2.84-.15c-.49.06-1.84.35-4,.85l-4.25,1,.49.41,2,1.63,2.45,2ZM13.45,52.83l4.13-.95a27.35,27.35,0,0,0,4.79-1.35,1.32,1.32,0,0,0,.81-1.05,1.89,1.89,0,0,0-.77-1.37,5.08,5.08,0,0,0-2.18-1.17,7.13,7.13,0,0,0-2.74-.17c-.49.06-1.83.35-4,.85l-4.26,1,2.13,2.12Z"/>
                                <path d="M22.3,21l-1.74,3.75,7.22,7.92L35,24.75,33.34,21M27.85,37.38l0,1.11m0-3.33,0,1.11M20.56,1H35V21H20.56Z"/>
                            </svg>
                        </div>
                        <div class="description">
                            <span>Маркировка</span>
                        </div>
                    </div>
                    <div class="advaBlock block18" data-block="block18">
                        <div class="icon">
                            <svg viewBox="0 0 75 70.38">
                                <path d="M6.8,6.5H39.53a4.71,4.71,0,0,1,4.7,4.7v2.93H30.76a1.54,1.54,0,0,0-1.53,1.53v8.69a1.54,1.54,0,0,0,1.53,1.53H44.22V37.8a4.71,4.71,0,0,1-4.7,4.7H2.65C.57,34.24.06,24.9,3.48,16.36A85.63,85.63,0,0,0,6.8,6.5M18.22,24.88a6,6,0,1,1-6,6,6,6,0,0,1,6-6ZM37,30.38a2.25,2.25,0,1,1-2.25,2.25A2.25,2.25,0,0,1,37,30.38Z"/>
                                <path d="M66.72,60.38l-2-52.5L54.22,1M59.5,60.38v6.88a2.12,2.12,0,0,0,2.12,2.12M74,56.63V28.49a2.12,2.12,0,0,0-2.12-2.12H65.43m-11.21,30h3.5m-3.5-5h3.5m-3.5-5h3.5m-3.5-5h3.5m-3.5-5h3.5m-3.5-5h3.5m-3.5-5h3.5m-3.5-5h3.5m-3.5-5h3.5m-3.5-5h3.5M54.22,6.5v36m0,5.5V60.38h12.5M14.85,6.5H54.22V1L21.72,3,14.85,6.5Zm20,36H54.22V48l-12.5-2Z"/>
                            </svg>
                        </div>
                        <div class="description">
                            <span>Контроль и измерение</span>
                        </div>
                    </div>
                    <div class="advaBlock block19" data-block="block19">
                        <div class="icon">
                            <svg viewBox="0 0 64.05 63.81">
                                <path d="M15.7,24,1,15.7V47.54L26.71,62.09M1,15.7,26.71,1.15l15,8.5M38,23.88l3.7-2.1M52.36,29l.05,18.57L26.71,62.09V43.39"/>
                                <polyline points="26.84 43.46 15.7 37.15 15.7 23.36 26.84 17.05 37.97 23.36 37.97 37.15 26.84 43.46"/>
                                <path d="M52.36,15.73,41.68,9.68V22.92L52.36,29M41.68,9.68,52.36,3.63,63.05,9.68M52.36,15.73l.05,0,10.64-6V22.92L52.36,29Z"/>
                            </svg>
                        </div>
                        <div class="description">
                            <span>Сборка, сортировка и упаковка</span>
                        </div>
                    </div>
                    <div class="advaBlock block20" data-block="block20">
                        <div class="icon">
                            <svg viewBox="0 0 58.35 47.5">
                                <path d="M57.31,35H1M57.31,46.5V16M1,41.91H57.35M1,46.5V16m56.31,0H1"/>
                                <path d="M5.85,1h13V14h-13V1m17,19h13V33h-13V20m20,10.1L40,33m2.9-10.1L40,20M50,30.1l2.9,2.9M50,22.9l2.9-2.9m-13,0h13V33h-13V20m-14,10.1L23,33m2.9-10.1L23,20M33,30.1l2.9,2.9M33,22.9l2.9-2.9m7.05-8.9L40,14m2.9-10.1L40,1M50,11.1l2.9,2.9M50,3.9,52.88,1m-13,0h13V14h-13V1m-14,10.1L23,14m2.9-10.1L23,1M33,11.1l2.9,2.9M33,3.9,35.88,1m-13,0h13V14h-13V1m-14,10.1L6,14M8.88,3.9,6,1M16,11.1l2.9,2.9M16,3.9,18.85,1M8.75,3.9H16v7.2H8.75V3.9Zm17,19H33v7.2h-7.2V22.9Zm17,0H50v7.2h-7.2V22.9Zm0-19H50v7.2h-7.2V3.9Zm-17,0H33v7.2h-7.2Z"/>
                            </svg>
                        </div>
                        <div class="description">
                            <span>Стеллажи и конвейерные системы</span>
                        </div>
                    </div>
                    <div class="advaBlock block21" data-block="block21">
                        <div class="icon">
                            <svg viewBox="0 0 62.12 52.55">
                                <path d="M3,1H59.16a2,2,0,0,1,2,2V36a2,2,0,0,1-2,2H54.37M21.94,48.39H40.17V46.81m-13.44-9H21.94V48.39m-7.45,0H47.62v3.16H14.49V48.39M5.8,4H56.32A1.64,1.64,0,0,1,58,5.61V33.16a1.64,1.64,0,0,1-1.63,1.63h-1m-29.61,0H5.8a1.64,1.64,0,0,1-1.63-1.63V5.61A1.64,1.64,0,0,1,5.8,4M26.73,37.91H3a2,2,0,0,1-2-2V3A2,2,0,0,1,3,1"/>
                                <path d="M48.6,40.81l7.71,7.71M33.69,32.21a6.86,6.86,0,0,1,8.22-6.72m3.75,2.15a6.86,6.86,0,0,1,1.25,2m-6.2-8.57A11.07,11.07,0,1,1,29.63,32.14,11.07,11.07,0,0,1,40.7,21.07Z"/>
                            </svg>
                        </div>
                        <div class="description">
                            <span>Система мониторинга и документирования</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="wrapper wrapperProduction">
            <canvas id="productionCanvas"></canvas>
            <div class="slider">
                <div class="mobileManipulate">
                    <div class="arrow left" data-slide="left"></div>
                    <div class="arrow right" data-slide="right"></div>
                </div>
                <div class="item block1 tab1default">
                    <div class="wrap gradient">
                        <div class="description">
                            <div class="headTitle">
                                <h3>Интеграция роботов с различными ЧПУ</h3>
                            </div>
                            <ul>
                                <li>подключаем роботов к любым стойкам ЧПУ</li>
                                <li>делаем управление роботом от ЧПУ станка</li>
                                <li>расширяем стандартные возможности ЧПУ</li>
                            </ul>
                        </div>
                        <div class="image im25">
                            <div>
                                <img src="<?php echo get_template_directory_uri(); ?>/img/service_machine/advaBanner1cont1.jpg" alt="">
                            </div>
                            <div>
                                <img src="<?php echo get_template_directory_uri(); ?>/img/service_machine/advaBanner1cont2.jpg" alt="">
                            </div>
                            <div>
                                <img src="<?php echo get_template_directory_uri(); ?>/img/service_machine/advaBanner1cont3.jpg" alt="">
                            </div>
                            <div>
                                <img src="<?php echo get_template_directory_uri(); ?>/img/service_machine/advaBanner1cont4.jpg" alt="">
                            </div>
                            <div>
                                <img src="<?php echo get_template_directory_uri(); ?>/img/service_machine/advaBanner1cont5.jpg" alt="">
                            </div>
                            <div>
                                <img src="<?php echo get_template_directory_uri(); ?>/img/service_machine/advaBanner1cont6.jpg" alt="">
                            </div>
                            <div>
                                <img src="<?php echo get_template_directory_uri(); ?>/img/service_machine/advaBanner1cont7.jpg" alt="">
                            </div>
                            <div>
                                <img src="<?php echo get_template_directory_uri(); ?>/img/service_machine/advaBanner1cont8.jpg" alt="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item block2">
                    <div class="wrap gradient">
                        <div class="description">
                            <div class="headTitle">
                                <h3>Обработка после станка</h3>
                                <div class="line"></div>
                            </div>
                            <ul>
                                <li>Чистка и мойка</li>
                                <li>Удаление заусенцев</li>
                                <li>Крацевание и шлифовка</li>
                                <li>Прессовка</li>
                                <li>Маркировка и гравировка</li>
                                <li>Нанесение клея</li>
                            </ul>
                        </div>
                        <div class="image">
                            <img src="<?php echo get_template_directory_uri(); ?>/img/service_machine/advaBanner5.png" alt="">
                        </div>
                    </div>
                </div>
                <div class="item block3">
                    <div class="wrap gradient">
                        <div class="description">
                            <div class="headTitle">
                                <h3>Контроль и измерение</h3>
                                <div class="line"></div>
                                <p class="desc">Выполнение точных измерений и контроля технологического процесса</p>
                            </div>
                            <ul>
                                <li>Контроль геометрических размеров</li>
                                <li>Измерение качества поверхности</li>
                                <li>Проверка отклонения параметров от заданных значений</li>
                            </ul>
                        </div>
                        <div class="image">
                            <img class="top" src="<?php echo get_template_directory_uri(); ?>/img/service_machine/advaBanner6.png" alt="">
                        </div>
                    </div>
                </div>
                <div class="item block4">
                    <div class="wrap gradient">
                        <div class="description">
                            <div class="headTitle">
                                <h3>Техническое зрение</h3>
                                <div class="line"></div>
                            </div>
                            <ul>
                                <li>Распознает положение деталей для их точного захвата роботом</li>
                                <li>Измеряет геометрические параметры заготовки для сортировки и отбраковки</li>
                                <li>Определяет отклонения от норм и стандартов готовой продукции</li>
                                <li>Считывает и дешифрует маркировку и гравировку</li>
                            </ul>
                        </div>
                        <div class="image">
                            <img class="bottom" src="<?php echo get_template_directory_uri(); ?>/img/service_machine/advaBanner8.png" alt="">
                        </div>
                    </div>
                </div>
                <div class="item block5">
                    <div class="wrap gradient">
                        <div class="description">
                            <div class="headTitle">
                                <h3>Стеллажи и конвейерные системы</h3>
                                <div class="line"></div>
                                <p class="desc">Разрабатываем конвейерные и стеллажные системы различного уровня сложности</p>
                            </div>
                            <ul>
                                <li>Автоматизация процесса перемещения и хранения</li>
                                <li>Минимизация «человеческого фактора»</li>
                                <li>Увеличение межоперационной скорости</li>
                                <li>Снижение рисков повреждения изделий</li>
                            </ul>
                        </div>
                        <div class="image">
                            <img class="right bottom" src="<?php echo get_template_directory_uri(); ?>/img/service_machine/advaBanner3.png" alt="">
                        </div>
                    </div>
                </div>
                <div class="item block6">
                    <div class="wrap gradient">
                        <div class="description nomr">
                            <div class="headTitle head-max-width">
                                <h3>Система мониторинга и документирования</h3>
                                <div class="line"></div>
                                <p class="desc">Система мониторинга и документирования работы автоматических систем и роботизированных комплексов</p>
                            </div>
                            <ul>
                                <li>Мониторинг состояния оборудования от датчиков до контроллеров</li>
                                <li>Документирование технологических процессов с генерацией отчетов</li>
                                <li>Экспорт статистики работы автоматических систем</li>
                                <li>Хранение информации на сервере с возможностью быстрого доступа к данным</li>
                                <li>Администрирование работы оборудования с различных устройств: ПК, планшет, смартфон</li>
                            </ul>
                        </div>
                        <div class="image">
                            <img class="bottom" src="<?php echo get_template_directory_uri(); ?>/img/advaCont3.png" alt="">
                        </div>
                    </div>
                </div>
                <div class="item block7">
                    <div class="wrap gradient">
                        <div class="description">
                            <div class="headTitle">
                                <h3>Захватные устройства и системы смены инструмента</h3>
                                <div class="line"></div>
                                <p class="desc">Захваты различных типов и конструкций вместе с системой смены инструмента предоставляют гибкость и эффективность для любого автоматизированного процесса</p>
                            </div>
                            <ul>
                                <li>Пневматические, гидравлические, электрические и магнитные захваты</li>
                                <li>Большой диапазон усилий (от 1 до 3000 Н)</li>
                                <li>Высокая точность позиционирования изделий</li>
                                <li>Автоматическая смена инструмента роботом без переналадки</li>
                            </ul>
                        </div>
                        <div class="image">
                            <img src="<?php echo get_template_directory_uri(); ?>/img/service_machine/advaBanner7.png" alt="">
                        </div>
                    </div>
                </div>
                <div class="item block8 tab2default">
                    <div class="wrap gradient">
                        <div class="description">
                            <div class="headTitle">
                                <h3>Захватные устройства и магазин инструментов</h3>
                                <div class="line"></div>
                            </div>
                            <ul>
                                <li>Разрабатываем захваты с особым вниманием к качеству компонентов, их унификации и
                                    оптимизации веса
                                </li>
                                <li>Проектируем магазины инструментов для работы со всей номеклатурой изделий</li>
                                <li>Используем автоматические системы смены инструмента для непрерывного
                                    производственного процесса
                                </li>
                            </ul>
                        </div>
                        <div class="image">
                            <img class="right bottom" src="<?php echo get_template_directory_uri(); ?>/img/service_machine/advaBanner2tab2.png" alt="">
                        </div>
                    </div>
                </div>
                <div class="item block9">
                    <div class="wrap gradient">
                        <div class="description">
                            <div class="headTitle">
                                <h3>Автоматическая смена ножей листогиба</h3>
                                <div class="line"></div>
                            </div>
                            <ul>
                                <li>Сокращает производственный цикл и повышает качество гибки</li>
                                <li>Увеличивает экономический эффект от производства малых партий изделий</li>
                                <li>Исключает "человеческий фактор"</li>
                            </ul>
                        </div>
                        <div class="image">
                            <img class="right bottom" src="<?php echo get_template_directory_uri(); ?>/img/service_machine/advaBanner3tab2.png" alt="">
                        </div>
                    </div>
                </div>
                <div class="item block10">
                    <div class="wrap gradient">
                        <div class="description">
                            <div class="headTitle">
                                <h3>Система перепозиционирования</h3>
                                <div class="line"></div>
                                <p class="desc">Различные наклонные столы и вакуумные захваты увеличивают гибкость и расширяют возможности</p>
                            </div>
                            <ul>
                                <li>Подготовка листа к гибке с одинаковой точностью</li>
                                <li>Перепозиционирование листов при сложной гибке</li>
                            </ul>
                        </div>
                        <div class="image">
                            <img class="top" style="width: 135%;position: relative;right: 15%;" src="<?php echo get_template_directory_uri(); ?>/img/service_machine/advaBanner6tab2.png" alt="">
                        </div>
                    </div>
                </div>
                <div class="item block11">
                    <div class="wrap gradient">
                        <div class="description">
                            <div class="headTitle">
                                <h3>Системы загрузки, выгрузки</h3>
                                <div class="line"></div>
                            </div>
                            <ul>
                                <li>Позиционирование поддонов и заготовок</li>
                                <li>Стеллажные системы</li>
                                <li>Контроль толщины листа</li>
                                <li>Складирование на поддоны и конвейерные системы</li>
                            </ul>
                        </div>
                        <div class="image">
                            <img class="bottom top" src="<?php echo get_template_directory_uri(); ?>/img/service_machine/advaBlock7tab2.png" alt="">
                        </div>
                    </div>
                </div>
                <div class="item block12">
                    <div class="wrap gradient">
                        <div class="description">
                            <div class="headTitle">
                                <h3>Offline программирование</h3>
                                <div class="line"></div>
                                <p class="desc">Метод программирования, который не мешает производству</p>
                            </div>
                            <ul>
                                <li>Легкая интеграция в производственный процесс</li>
                                <li>Моделирование и симуляция процесса гибки для исключения ошибок перед запуском
                                    производства
                                </li>
                                <li>Уменьшение простоев производства до 1/10 по сравнению с Online-программированием,
                                    например, с 40 до 4 часов.
                                </li>
                            </ul>
                        </div>
                        <div class="image">
                            <img src="<?php echo get_template_directory_uri(); ?>/img/service_machine/advaBanner1tab2.png" alt="">
                        </div>
                    </div>
                </div>
                <div class="item block13">
                    <div class="wrap gradient">
                        <div class="description">
                            <div class="headTitle">
                                <h3>Роботизированная гибка труб</h3>
                                <div class="line"></div>
                                <p class="desc">Технология, с помощью которой возможно осуществить 3D гибку труб любой сложности</p>
                            </div>
                            <ul>
                                <li>Гибка труб высокой сложности конструкции недоступная для обычных гибочных станков</li>
                                <li>Скорость выполнения гибки, вдвое выше, чем у классического оборудования</li>
                                <li>Полностью автоматический процесс от подающего до приемного накопителей</li>
                            </ul>
                        </div>
                        <div class="image">
                            <img src="<?php echo get_template_directory_uri(); ?>/img/service_machine/advaBanner5tab2.png" alt="">
                        </div>
                    </div>
                </div>
                <div class="item block14">
                    <div class="wrap gradient">
                        <div class="description nomr">
                            <div class="headTitle head-max-width">
                                <h3>Система мониторинга и документирования</h3>
                                <div class="line"></div>
                                <p class="desc">Система мониторинга и документирования работы автоматических систем и роботизированных комплексов</p>
                            </div>
                            <ul>
                                <li>Мониторинг состояния оборудования от датчиков до контроллеров</li>
                                <li>Документирование технологических процессов с генерацией отчетов</li>
                                <li>Экспорт статистики работы автоматических систем</li>
                                <li>Хранение информации на сервере с возможностью быстрого доступа к данным</li>
                                <li>Администрирование работы оборудования с различных устройств: ПК, планшет, смартфон</li>
                            </ul>
                        </div>
                        <div class="image">
                            <img class="bottom" src="<?php echo get_template_directory_uri(); ?>/img/advaCont3.png" alt="">
                        </div>
                    </div>
                </div>
                <div class="item block15 tab3default">
                    <div class="wrap gradient">
                        <div class="description">
                            <div class="headTitle">
                                <h3>Захватные устройства и магазин инструментов</h3>
                                <div class="line"></div>
                            </div>
                            <ul>
                                <li>Разрабатываем захваты с особым вниманием к качеству компонентов, их унификации и
                                    оптимизации веса
                                </li>
                                <li>Проектируем магазины инструментов для работы со всей номеклатурой изделий</li>
                                <li>Используем автоматические системы смены инструмента для непрерывного
                                    производственного процесса
                                </li>
                            </ul>
                        </div>
                        <div class="image">
                            <img class="right bottom" src="<?php echo get_template_directory_uri(); ?>/img/service_machine/advaBanner4tab3.png" alt="">
                        </div>
                    </div>
                </div>
                <div class="item block16">
                    <div class="wrap gradient">
                        <div class="description">
                            <div class="headTitle">
                                <h3>Обработка после станка</h3>
                                <div class="line"></div>
                            </div>
                            <ul>
                                <li>Чистка и мойка</li>
                                <li>Удаление заусенцев</li>
                                <li>Крацевание и шлифовка</li>
                                <li>Прессовка</li>
                                <li>Маркировка и гравировка</li>
                                <li>Нанесение клея</li>
                            </ul>
                        </div>
                        <div class="image">
                            <img src="<?php echo get_template_directory_uri(); ?>/img/service_machine/advaBanner2tab3.png" alt="">
                        </div>
                    </div>
                </div>
                <div class="item block17">
                    <div class="wrap gradient">
                        <div class="description">
                            <div class="headTitle">
                                <h3>Маркировка</h3>
                                <div class="line"></div>
                                <p class="desc">Лазерные, ударно-точечные маркираторы и 3D принтеры покрывают спектр всевозможных видов маркировки и гравировки на любых поверхностях и в различных условиях производства</p>
                            </div>
                            <ul>
                                <li>Нанесение штрих-кода, QR-кода, кода даты, серийных номеров и логотипов</li>
                                <li>3D печать объемных надписей</li>
                                <li>Маркировка на геометрически сложных поверхностях с изменением высоты, угла наклона
                                    и т.п.
                                </li>
                            </ul>
                        </div>
                        <div class="image">
                            <img src="<?php echo get_template_directory_uri(); ?>/img/service_machine/advaBanner5tab3.png" alt="">
                        </div>
                    </div>
                </div>
                <div class="item block18">
                    <div class="wrap gradient">
                        <div class="description">
                            <div class="headTitle">
                                <h3>Контроль и измерение</h3>
                                <div class="line"></div>
                                <p class="desc">Выполнение точных измерений и контроля технологического процесса</p>
                            </div>
                            <ul>
                                <li>Контроль геометрических размеров</li>
                                <li>Измерение качества поверхности</li>
                                <li>Проверка отклонения параметров от заданных значений</li>
                            </ul>
                        </div>
                        <div class="image">
                            <img class="top" src="<?php echo get_template_directory_uri(); ?>/img/service_machine/advaBanner3tab3.png" alt="">
                        </div>
                    </div>
                </div>
                <div class="item block19">
                    <div class="wrap gradient">
                        <div class="description">
                            <div class="headTitle">
                                <h3>Сборка, сортировка и упаковка</h3>
                                <div class="line"></div>
                                <p class="desc">После формовочных машин изделие проходит путь от сборки до упаковки. Мы сокращаем этот путь и уменьшаем технологический передел.</p>
                            </div>
                            <ul>
                                <li>Сортировка и позиционирование деталей</li>
                                <li>Контроль деталей перед сборкой</li>
                                <li>Использование технологических оснасток и автоматики</li>
                                <li>Сборка от простой клейки до многоступенчатых операций</li>
                                <li>Укладка полуфабрикатов на конвейеры, в паллеты и стеллажи</li>
                                <li>Упаковка готовой продукции в тару и складирование на поддоны</li>
                            </ul>
                        </div>
                        <div class="image">
                            <img src="<?php echo get_template_directory_uri(); ?>/img/service_machine/advaBanner6tab3.png" alt="">
                        </div>
                    </div>
                </div>
                <div class="item block20">
                    <div class="wrap gradient">
                        <div class="description">
                            <div class="headTitle">
                                <h3>Стеллажи и конвейерные системы</h3>
                                <div class="line"></div>
                                <p class="desc">Разрабатываем конвейерные и стеллажные системы различного уровня сложности</p>
                            </div>
                            <ul>
                                <li>Автоматизация процесса перемещения и хранения</li>
                                <li>Минимизация «человеческого фактора»</li>
                                <li>Увеличение межоперационной скорости</li>
                                <li>Снижение рисков повреждения изделий</li>
                            </ul>
                        </div>
                        <div class="image">
                            <img class="right bottom" src="<?php echo get_template_directory_uri(); ?>/img/service_machine/advaBanner3.png" alt="">
                        </div>
                    </div>
                </div>
                <div class="item block21">
                    <div class="wrap gradient">
                        <div class="description nomr">
                            <div class="headTitle head-max-width">
                                <h3>Система мониторинга и документирования</h3>
                                <div class="line"></div>
                                <p class="desc">Система мониторинга и документирования работы автоматических систем и роботизированных комплексов</p>
                            </div>
                            <ul>
                                <li>Мониторинг состояния оборудования от датчиков до контроллеров</li>
                                <li>Документирование технологических процессов с генерацией отчетов</li>
                                <li>Экспорт статистики работы автоматических систем</li>
                                <li>Хранение информации на сервере с возможностью быстрого доступа к данным</li>
                                <li>Администрирование работы оборудования с различных устройств: ПК, планшет, смартфон</li>
                            </ul>
                        </div>
                        <div class="image">
                            <img class="bottom" src="<?php echo get_template_directory_uri(); ?>/img/advaCont3.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="wrapper wrapperProfit">
            <canvas id="profitrolCanvas"></canvas>
            <header>
                <h3 class="orange"><span class="swapTab" data-tab="all">Выгода от внедрения</span></h3>
                <p><span class="swapTab" data-tab="all">Перевод на новый уровень развития производства</span></p>
            </header>
            <div class="advaWrap swapWrapper">
                <div class="advantages vigoda swapTab" data-tab="all">
                    <div class="advaBlock block1">
                        <div class="icon">
                            <img src="<?php echo get_template_directory_uri(); ?>/img/gotoverh.png" alt="">
                        </div>
                        <div class="desc">
                            <p>Рост объемов</p>
                        </div>
                    </div>
                    <div class="advaBlock block2">
                        <div class="icon">
                            <img src="<?php echo get_template_directory_uri(); ?>/img/medal_of_kokokonor.png" alt="">
                        </div>
                        <div class="desc">
                            <p>Обеспечение стабильного высокого качества готового изделия</p>
                        </div>
                    </div>
                    <div class="advaBlock block3">
                        <div class="icon">
                            <img src="<?php echo get_template_directory_uri(); ?>/img/meshokrubley.png" alt="">
                        </div>
                        <div class="desc">
                            <p>Повышение рентабельности производства</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="wrapper wrapperFaster">
            <canvas id="fasterCanvas"></canvas>
            <header>
                <h3 class="orange"><span class="swapTab" data-tab="all">Быстрота внедрения</span></h3>
                <p><span class="swapTab" data-tab="all">Производство получает работающее оборудование в коротские сроки</span></p>
            </header>
            <div class="cheto">
                <canvas id="chetoCanvas"></canvas>
                <div class="wrap">
                    <div class="icon icon1 swapTab" data-tab="all">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/icons_white-1.png" alt="">
                    </div>
                    <div class="icon icon2 swapTab" data-tab="all">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/icons_white-2.png" alt="">
                        <p>Разработка по ТЗ</p>
                    </div>
                    <div class="icon icon3 swapTab" data-tab="all">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/icons_white-3.png" alt="">
                        <p>Закупка, сборка, упаковка, отправка</p>
                    </div>
                    <div class="icon icon4 swapTab" data-tab="all">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/icons_white-4.png" alt="">
                        <p>Пусконаладочные работы</p>
                    </div>
                    <div class="icon icon5 swapTab" data-tab="all">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/icons_white-5.png" alt="">
                        <p>Оборудование работает, персонал обучен</p>
                    </div>
                    <div class="icon icon6 swapTab" data-tab="all">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/icons_white-6.png" alt="">
                    </div>
                </div>
            </div>
        </div>
        <div class="wrapper wrapperReviews">
            <canvas id="reviewsCanvas"></canvas>
            <header>
                <h3 class="orange">Внедренные проекты</h3>
                <p>Проекты компании с которыми мы сотрудничаем</p>
            </header>
            <div class="reviews">
                <?php if(count($port) > 3) { ?>
                    <div class="prev"></div>
                    <div class="next"></div>
                <?php } ?>
                <div class="slider">
                    <?php foreach($port as $key => $post) { ?>
                        <?php
                            setup_postdata($post);
                            $permalink = get_the_permalink();
                            $thumb = get_the_post_thumbnail($post->ID, 'full', false, false);
                            $title = get_the_title();
                            $description = get_the_content();
                            $i = $key + 1;
                            $i = $i%3;
                        ?>
                        <div class="item <?php if($i == 1) echo 'left';
                        if($i == 2) echo 'center';
                        if($i == 0) echo 'right'; ?> visible">
                            <div class="wrapper">
                                <header><a href="<?php echo $permalink; ?>"><?php echo $thumb; ?></a></header>
                                <div class="description">
                                    <p><?php echo mb_strimwidth($title, 0, 100, "..."); ?></p>
                                </div>
                                <div class="signature">
                                    <a href="<?php echo $permalink; ?>" class="button orange">Подробнее</a>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                    <?php wp_reset_postdata(); ?>
                </div>
            </div>
        </div>
        <div class="wrapper wrapperAction">
            <canvas id="actionCanvas"></canvas>
            <header>
                <h3 class="orange">Консультация специалиста</h3>
                <p>Оставьте заявку, и мы свяжемся с вами в ближайшее время</p>
            </header>
            <form action="">
                <div class="wrapinput"><input type="text" name="name" placeholder="Имя"></div>
                <div class="wrapinput"><input type="tel" name="tel" placeholder="+7  (      )      -    -    " required></div>
                <input type="file" name="file">
                <button type="submit" class="button orange piu">Отправить</button>
            </form>
            <div class="lineAction"></div>
            <div class="man">
                <img src="<?php echo get_template_directory_uri(); ?>/img/vasya_obrez.png" alt="">
            </div>
            <div class="kuka">
                <img src="<?php echo get_template_directory_uri(); ?>/img/kuka_obrez.png" alt="">
            </div>
        </div>

<?php get_footer(); ?>
