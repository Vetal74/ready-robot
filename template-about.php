<?php /* Template Name: Страница о компании */ get_header(); ?>
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/owl.carousel.css">
<div class="wrapper wrapperMain refreshAnimation">
    <div class="mainSlider contacts">
        <header>
            <div class="breadCrumbs fade translateBottom"><a href="/">Главная</a><span class="delimer">></span><a href="">О компании</a></div>
        </header>
        <div class="slide planet" style="background-image: url(<?php echo get_template_directory_uri(); ?>/img/banner_about.jpg);">
            <div class="headTitle">
                <h1 class="fade translateBottom">О компании</h1>
                <div class="line"></div>
                <p class="fade translateBottom delay1">Ready robot Ready worker</p>
            </div>
        </div>
        <div class="bottomLine"></div>
    </div>
    <div class="calculator">
        <div class="top">
            <h3>Расчет стоимости и сроков реализации проекта</h3>
            <div class="params">
                Параметры
            </div>
        </div>
        <div class="bottom">
            <p class="order">Примерная стоимость</p>
            <p class="numbers">1 500 000 Р</p>
            <p class="duration">2,5 месяца</p>
            <a href="" class="button ultraBlue piu no-ajax">Прочитать подробнее</a>
        </div>
    </div>
</div>
<div class="wrapper pageAbout">
    <div class="textColumn">
        Компания Ready Robot разрабатывает решения для автоматизации и роботизации производства. Мы создаем робототехнологические комплексы, автоматизированные линии, системы управления (АСУ ТП) и информационные системы для мониторинга и документирования работы оборудования. Помогаем компаниям вывести производство на новый уровень технологичности, качества и эффективности.
    </div>
</div>
<div class="wrapper wrapperSection">
    <canvas id="aboutPartners"></canvas>
    <header>
        <h3 class="orange">Области применения</h3>
    </header>
    <div class="wrapSect">
        <div class="advaBlock">
            <div class="icon">
                <svg viewBox="0 0 67.58 56.72">
                    <path d="M60.37,50.75l-1.5-1.5m8-6-1.5-1.5m-26.5,2.5-1.5-1.5M39.62,35l-1.5-1.5m17.78,11s3.26-3.26,7.33-2.44M49.91,53.4s2.3-4,6.45-4.26M45.14,43.82s0-4.61-3.46-6.91m2.67,15.93s0-4.61-3.46-6.91"/>
                    <path d="M0,55.72H64.51M52.42,29.8l-2.3,12.67,5.18-6.34,1.33-9.05M34.56,55.72V48.81a1.73,1.73,0,0,0-1.73-1.73H5.18a1.73,1.73,0,0,0-1.73,1.73v6.91m7.15-8.64L8.74,36M19,15.55,26.11,9.9m19.81,1.84L56.63,27.08,52.42,29.8l-12-12.39m-9.55-.65-5.44,4.78M23.55,34l3.31,13M36.29,1a6.91,6.91,0,1,1-6.91,6.91A6.91,6.91,0,0,1,36.29,1M15,18.66A7.68,7.68,0,1,1,7.3,26.34,7.68,7.68,0,0,1,15,18.66M36.37,5a2.75,2.75,0,1,1-2.75,2.75A2.75,2.75,0,0,1,36.37,5ZM15.12,23.75a2.75,2.75,0,1,1-2.75,2.75A2.75,2.75,0,0,1,15.12,23.75Z"/>
                </svg>
            </div>
            <div class="description">
                сварка (MIG/MAG, TIG, плазменная, лазерная)
            </div>
        </div>
        <div class="advaBlock">
            <div class="icon">
                <svg viewBox="0 0 81.91 84.56">
                    <path d="M63.38,32.22l-8.92,8.92-1.8-1.8M40.74,27.42l-1.87-1.87,8.92-8.92m33.12,4.46L66.64,35.35,44,13.56,56.53,1M34.82,29.6l-.75,16.34,16.34-.75,2.25-5.85L40.74,27.42Z"/>
                    <path d="M3.11,28.27a38.63,38.63,0,0,1,37.12,38.6,38.48,38.48,0,0,1-3.79,16.7m3.19-9.89s.1-3.51,4.6-3M38,80.17a6.23,6.23,0,0,1,4.73-4M40.21,65.91a4.85,4.85,0,0,1,3-4.74m-3.63-1.3s-.46-4.89,1-6.39M1,40.45h.59A26.43,26.43,0,0,1,28,66.87a26.31,26.31,0,0,1-3.35,12.88"/>
                </svg>
            </div>
            <div class="description">
                обслуживание машин (токарные и фрезерные станки, листогибы, пресса, формовочные машины)
            </div>
        </div>
        <div class="advaBlock">
            <div class="icon">
                <svg viewBox="0 0 64.05 63.81">
                    <path d="M15.7,24,1,15.7V47.54L26.71,62.09M1,15.7,26.71,1.15l15,8.5M38,23.88l3.7-2.1M52.36,29l.05,18.57L26.71,62.09V43.39"/>
                    <polyline points="26.84 43.46 15.7 37.15 15.7 23.36 26.84 17.05 37.97 23.36 37.97 37.15 26.84 43.46"/>
                    <path d="M52.36,15.73,41.68,9.68V22.92L52.36,29M41.68,9.68,52.36,3.63,63.05,9.68M52.36,15.73l.05,0,10.64-6V22.92L52.36,29Z"/>
                </svg>
            </div>
            <div class="description">
                сборка, упаковка и сортировка
            </div>
        </div>
        <div class="advaBlock">
            <div class="icon">
                <svg viewBox="0 0 66.49 67.58">
                    <path d="M33.24,26.4l-22-12.7V41.4l22,12.7m18-10.38v-28l-36.06-4.3M38.31,51.18v-28L30.18,2.77M11.24,13.7,33.24,1l22,12.7m-22,12.7,22-12.7V41.4l-22,12.7Z"/>
                    <path d="M55.24,35l10.25,6.08L33.24,60.2,1,41.07,11.24,35M40.49,62.18V55.8m-14,6.38V55.8m6.76,10.79V60.2m25.24-8.77-4.14-3.76M8,51.44l4.14-3.76M8,51.44V45.06M1,47.45V41.07M58.49,51.44V45.06m7,2.39V41.07m0,6.38L33.24,66.58,1,47.45"/>
                </svg>
            </div>
            <div class="description">
                паллетирование и депаллетирование
            </div>
        </div>
    </div>
</div>
<div class="wrapper pagePartners about">
    <canvas id="canvasPartners"></canvas>
    <header class="about">
        <p>Мы реализуем проекты с роботами разных производителей и знаем особенности каждого из них. Это позволяет нам учитывать их сильные стороны для решения определенных задач, а также расширять стандартный функционал, создавая новые опции.</p>
    </header>
    <div class="partners owl-carousel">
        <div class="partner">
            <img src="<?php echo get_template_directory_uri(); ?>/img/advaLogo1.png" alt="">
        </div>
        <div class="partner">
            <img src="<?php echo get_template_directory_uri(); ?>/img/advaLogo2.png" alt="">
        </div>
        <div class="partner">
            <img src="<?php echo get_template_directory_uri(); ?>/img/advaLogo3.png" alt="">
        </div>
        <div class="partner">
            <img src="<?php echo get_template_directory_uri(); ?>/img/advaLogo4.png" alt="">
        </div>
        <div class="partner">
            <img src="<?php echo get_template_directory_uri(); ?>/img/advaLogo5.png" alt="">
        </div>
        <div class="partner">
            <img src="<?php echo get_template_directory_uri(); ?>/img/advaLogo6.png" alt="">
        </div>
    </div>
    <div class="arrows">
        <div class="arrow left"><img src="<?php echo get_template_directory_uri(); ?>/img/portfolio/arrow-left.png" alt=""></div>
        <div class="arrow right"><img src="<?php echo get_template_directory_uri(); ?>/img/portfolio/arrow-right.png" alt=""></div>
    </div>
</div>
<div class="wrapper wrapperDescAbout">
    <header>
        <h3 class="orange">
            Делаем роботизированные технологии доступными
        </h3>
    </header>
    <section>
        <div class="block">
            Многие задачи производства мы решаем с помощью готовых <a href="/">коробочных решений</a>, которые отличаются низкой стоимостью, быстротой изготовления и установки. Мы делаем их гибкими и продуманными до мелочей, чтобы предприятиям было удобно и просто использовать комплексы и перенастраивать их под новые задачи.
        </div>
        <div class="block">
            Мы стремимся стереть границу между роботизированными технологиями и “неподготовленным” персоналом на производстве. Для этого мы разрабатываем специальный интерактивный софт
            <a href="/">OnePad</a>, с помощью которого рабочие на производстве могут легко и быстро изучить комплекс и начать работать с ним.
        </div>
        <div class="block">
            Мы также понимаем, что и руководителю предприятия важно оценивать эффективность эксплуатации комплекса. Поэтому мы разрабатываем
            <a href="/">информационную систему мониторинга и документирования</a>, которая создает цельную картину о работе всего оборудования и персонала.
        </div>
    </section>
    <header>
        <h3 class="orange">
            Решаем нестандартные задачи
        </h3>
    </header>
    <section>
        <div class="block">
            Разрабатываем решения для сложных и неординарных задач и создаем то, чего раньше никто не делал. При этом <a href="/">индивидуальные проекты</a> мы делаем такими же простыми в изучении и эксплуатации, как коробочные решения.
        </div>
        <div class="block">
            Команда Ready Robot - это молодые профессионалы, готовые взяться за задачу любого уровня сложности и придумать классное решение. Мы не просто повторяем производственные процессы, делая их автоматическими - мы создаем новые технологии производства.
        </div>
    </section>
    <footer>
        Посмотреть на наши реализованные проекты можно <a href="/portfolio/">здесь</a>.
    </footer>
</div>
<div class="wrapper wrapperAction">
    <canvas id="actionCanvas"></canvas>
    <header>
        <h3 class="orange">Косультация специалиста</h3>
        <p>Оставьте заявку и мы свяжемся с вами в ближайшее время</p>
    </header>
    <form action="">
        <div class="wrapinput"><input type="text" name="name" placeholder="Имя"></div>
        <div class="wrapinput"><input type="tel" name="tel" placeholder="+7  (      )      -    -    " required></div>
        <input type="file" name="file">
        <button type="submit" class="button orange piu">Отправить</button>
    </form>
    <div class="lineAction"></div>
    <div class="man">
        <img src="<?php echo get_template_directory_uri(); ?>/img/vasya_obrez.png" alt="">
    </div>
    <div class="kuka">
        <img src="<?php echo get_template_directory_uri(); ?>/img/kuka_obrez.png" alt="">
    </div>
</div>


<?php get_footer(); ?>
