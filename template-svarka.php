<?php /* Template Name: Страница сварки */ get_header(); ?>

<?php
    $args = array(
        'post_type'=> 'portfolio',
        'category' => '',
        'posts_per_page' => -1,
        'offset'=> 0,
        'post_status' => 'publish'
    );

    $port = get_posts($args);
?>
    <div class="wrapper wrapperMain refreshAnimation">
            <div class="mainSlider">
                <header>
                    <?php if (function_exists('dimox_breadcrumbs')) dimox_breadcrumbs(); ?>
                </header>
                <div class="slide" style="background-image: url(<?php echo get_template_directory_uri(); ?>/img/newFon.jpg);">
                    <div class="kuka" data-zindex="50"></div>
                    <div class="iscr1"></div>
                    <div class="iscr2"></div>
                    <div class="iscr3"></div>
                    <div class="headTitle">
                        <h1 class="fade translateBottom">Решения для сварки</h1>
                        <div class="line"></div>
                        <p class="fade translateBottom delay1">Технологии MIG/MAG, TIG, CMT, Plasma, Laser и др</p>
                    </div>
                </div>
                <div class="bottomLine"></div>
            </div>
            <div class="calculator">
                <div class="top">
                    <h3>Расчет стоимости и сроков реализации проекта</h3>
                    <div class="params">
                        Параметры
                    </div>
                </div>
                <div class="bottom">
                    <p class="order">Примерная стоимость</p>
                    <p class="numbers">1 500 000 Р</p>
                    <p class="duration">2,5 месяца</p>
                    <a href="" class="button ultraBlue piu no-ajax">Прочитать подробнее</a>
                </div>
            </div>
        </div>
        <div class="wrapper wrapperDevelop">
            <canvas id="developCanvas" style="width: 100%;height: 100%;"></canvas>
            <div class="tabs">
                <div class="tab tab1" data-tab="tab1">
                    <a href="" class="no-ajax">Индивидуальные проекты</a>
                </div>
                <div class="tab tab2" data-tab="tab2">
                    <a href="" class="no-ajax">Готовые решения</a>
                </div>
                <!--<div class="tab tab3">-->
                <!--<a href="">Гибкий роботизированные комплекс</a>-->
                <!--</div>-->
                <div class="karetka hidden"></div>
            </div>
            <div class="tabsContent" data-tab="tab1">
                <div class="leftSide">
                    <div class="man"></div>
                    <div class="diagram"></div>
                </div>
                <div class="rightSide">
                    <div class="desc">
                        <h3>Разработка индивидуальных решений</h3>
                        <p>Разрабатываем и внедряем сварочные роботизированные комплексы под вашу задачу в 4 шага</p>
                        <ul>
                            <li>Шаг 1. Предварительное решение</li>
                            <li>Шаг 2. Промышленный аудит</li>
                            <li>Шаг 3. Производственный процесс</li>
                            <li>Шаг 4. Разработка рекомендаций</li>
                        </ul>
                        <a href="" class="button orange openModal no-ajax" data-modal="modalIndivid">Подробнее</a>
                    </div>
                </div>
            </div>
            <div class="tabsContent" data-tab="tab2">
                <div class="leftSide">
                    <div class="sowhat"></div>
                </div>
                <div class="rightSide">
                    <div class="desc">
                        <h3>Особенности Simple arc</h3>
                        <div class="columns">
                            <div class="column">
                                <p class="big">Простое и легкое программирование</p>
                                <p class="small">с интерактивной поддержкой на пульте робота</p>
                            </div>
                            <div class="column">
                                <p class="big">Универсальность</p>
                                <p class="small">сварка от маленького кронштейна до сложных коробочных конструкций и длинных массивных деталей</p>
                            </div>
                            <div class="column">
                                <p class="big">Быстрая и легкая модернизация</p>
                                <p class="small">на новые сварочные технологии</p>
                            </div>
                            <div class="column">
                                <p class="big">Самое современное оборудование</p>
                                <p class="small">с гарантийной поддержкой 10 лет от производителя</p>
                            </div>
                            <div class="column">
                                <p class="big">Короткий срок ввода</p>
                                <p class="small">в эксплуатацию</p>
                            </div>
                        </div>
                        <a href="" class="button orange no-ajax openModal" data-modal="mainModal">Подробнее</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="wrapper wrapperProfit">
            <canvas id="profitCanvas"></canvas>
            <header>
                <h3 class="orange swapWrapper">
                    <span class="swapTab" data-tab="tab1">Наши преимущества </span>
                    <span class="swapTab" data-tab="tab2">Преимущества Simple Arc</span>
                </h3>
                <p class="swapWrapper"><span class="swapTab" data-tab="all">Перевод на новый уровень развития производства</span></p>
            </header>
            <div class="advaWrap swapWrapper">
                <div class="advantages first swapTab" data-tab="tab1">
                    <div class="advaBlock block1 default" data-block="block1">
                        <div class="icon">
                            <svg viewBox="0 0 63.75 51.55">
                                <path d="M4.25,42A5.19,5.19,0,0,1,1,37.28V6A5,5,0,0,1,6,1H54.5a5.34,5.34,0,0,1,4.83,3.72"/>
                                <path d="M9.39,39.64H40M55.5,34a5,5,0,1,1-5-5,5,5,0,0,1,5,5M40,19.5h5.91M40,15.5H55.88M40,11.5H55.88M54,37.57v12.5c0,.23-.27.6-.43.43L50.5,47l-3.07,3.5c-.15.18-.43-.19-.43-.43V37.57m0,8.15H6.75a2.51,2.51,0,0,1-2.5-2.5v-36a2.51,2.51,0,0,1,2.5-2.5h53.5a2.51,2.51,0,0,1,2.5,2.5m0,0v36a2.51,2.51,0,0,1-2.5,2.5H54M34.26,27.34l-1.09,6,2.46-3,.63-4.3M25.79,39.64V36.36a.82.82,0,0,0-.82-.82H11.85a.82.82,0,0,0-.82.82v3.28m3.39-4.1-.88-5.24m4.88-9.72,3.37-2.68m9.4.88,5.08,7.28-2,1.29-5.68-5.88M24,21.16l-2.58,2.27m-.9,5.92,1.57,6.19m4.47-21.86A3.28,3.28,0,1,1,23.33,17a3.28,3.28,0,0,1,3.28-3.28M16.49,22.06a3.64,3.64,0,1,1-3.64,3.64,3.64,3.64,0,0,1,3.64-3.64"/>
                            </svg>
                        </div>
                        <div class="description">
                            <span>Широкие компетенции в роботах</span>
                        </div>
                    </div>
                    <div class="advaBlock block2" data-block="block2">
                        <div class="icon">
                            <svg viewBox="0 0 49.68 58.83">
                                <path d="M8.38,12c-.76.63-1.51,1.39-2.39,5.54A74,74,0,0,0,4.35,33.06c.13,5,1.64,15.55,2.58,17.06s1,1.78,2.45,2.48,1.5.89,2.88.52l.66-.16m4.95-.21c.81,0,2,.19,2.47.16m-9-37.85H35.7a.47.47,0,0,1,.47.47V48.28a.47.47,0,0,1-.47.47H11.29a.47.47,0,0,1-.47-.47V15.53A.47.47,0,0,1,11.29,15.06Z"/><path class="cls-1" d="M17.69,3.8a27.34,27.34,0,0,1,3.54.57c1.9.44,1.32,1.1,3.52.59s10-1.39,11.86-.81,2.27,3.08,2.56,4.39.51,2.49.81,2.64a2.27,2.27,0,0,0,1.76-.51,6.74,6.74,0,0,0-.22-3.3c-.37-1.68-.86-6-6.91-6.35M44.66,13.89l-3.51,1.17L42,23.7l3.88,1.39m0,0c0-.34,2.07.25,2.6-.58.23-.36.29-1.29-.19-4.33-.24-1.52-.11-2.79-.37-4.17-.16-.84-.34-1.8-1.32-2s-1.91.07-2-.15.42-.35,1-.55c1.42-.54,1.28-.92,1.16-1.54C46.51,10.48,46.23,9.23,46,8c-.73-3.51-1-3.66-1.76-4.54S40.85,1,32.94,1C26.56,1,11,4.78,5.7,9.2c-.88.73-1.76,1.61-2.78,6.44a86,86,0,0,0-1.9,18c.15,5.86,1.9,18.09,3,19.84a7.1,7.1,0,0,0,3.51,3.22c1.68.81,2.49,1.39,4.1,1,5.34-1.46,20.88-1.46,26.21,0,1.61.44,2.42-.15,4.1-1a7.1,7.1,0,0,0,3.51-3.22c1.1-1.76,2.86-14,3-19.84a60.67,60.67,0,0,0-.2-6.15c-.08-1,.09-.89-.79-1.4-.67-.38-1.56-.83-1.57-1M17.79,8.4A1.76,1.76,0,1,1,16,10.16,1.76,1.76,0,0,1,17.79,8.4ZM33.22,6.94a2.64,2.64,0,1,1-2.64,2.64,2.64,2.64,0,0,1,2.64-2.64ZM24.87,8.4a1.76,1.76,0,1,1-1.76,1.76A1.76,1.76,0,0,1,24.87,8.4Z"/>
                            </svg>
                        </div>
                        <div class="description">
                            <span>Интерактивный софт "OnePad"</span>
                        </div>
                    </div>
                    <div class="advaBlock block3" data-block="block3">
                        <div class="icon">
                            <svg viewBox="0 0 62.12 52.55">
                                <path d="M3,1H59.16a2,2,0,0,1,2,2V36a2,2,0,0,1-2,2H54.37M21.94,48.39H40.17V46.81m-13.44-9H21.94V48.39m-7.45,0H47.62v3.16H14.49V48.39M5.8,4H56.32A1.64,1.64,0,0,1,58,5.61V33.16a1.64,1.64,0,0,1-1.63,1.63h-1m-29.61,0H5.8a1.64,1.64,0,0,1-1.63-1.63V5.61A1.64,1.64,0,0,1,5.8,4M26.73,37.91H3a2,2,0,0,1-2-2V3A2,2,0,0,1,3,1"/>
                                <path d="M48.6,40.81l7.71,7.71M33.69,32.21a6.86,6.86,0,0,1,8.22-6.72m3.75,2.15a6.86,6.86,0,0,1,1.25,2m-6.2-8.57A11.07,11.07,0,1,1,29.63,32.14,11.07,11.07,0,0,1,40.7,21.07Z"/>
                            </svg>
                        </div>
                        <div class="description">
                            <span>Система мониторинга и документирования</span>
                        </div>
                    </div>
                    <div class="advaBlock block4" data-block="block4">
                        <div class="icon">
                            <svg viewBox="0 0 54.87 52.52">
                                <path d="M0,28.51H2.3c1,0,1-3.37,1.31-9.54s1.31-18,2-18,.33,11.23,1,27.51.33,23,2,23,1.64-23,1.64-23h2.3c1,0,1-3.37,1.31-9.54s1.31-18,2-18,.33,11.23,1,27.51.33,23,2,23,1.64-23,1.64-23h2.3c1,0,1-3.37,1.31-9.54S25.3,1,26,1s.33,11.23,1,27.51.33,23,2,23,1.64-23,1.64-23h2.3c1,0,1-3.37,1.31-9.54s1.31-18,2-18,.33,11.23,1,27.51.33,23,2,23,1.64-23,1.64-23H43c1,0,1-3.37,1.31-9.54s1.31-18,2-18,.33,11.23,1,27.51.33,23,2,23,1.64-23,1.64-23h3.95"/>
                            </svg>
                        </div>
                        <div class="description">
                            <span>Адаптивная система слежения за импульсной дугой</span>
                        </div>
                    </div>
                    <div class="advaBlock block5" data-block="block5">
                        <div class="icon">
                            <svg viewBox="0 0 67.58 56.72">
                                <path d="M60.37,50.75l-1.5-1.5m8-6-1.5-1.5m-26.5,2.5-1.5-1.5M39.62,35l-1.5-1.5m17.78,11s3.26-3.26,7.33-2.44M49.91,53.4s2.3-4,6.45-4.26M45.14,43.82s0-4.61-3.46-6.91m2.67,15.93s0-4.61-3.46-6.91"/>
                                <path d="M0,55.72H64.51M52.42,29.8l-2.3,12.67,5.18-6.34,1.33-9.05M34.56,55.72V48.81a1.73,1.73,0,0,0-1.73-1.73H5.18a1.73,1.73,0,0,0-1.73,1.73v6.91m7.15-8.64L8.74,36M19,15.55,26.11,9.9m19.81,1.84L56.63,27.08,52.42,29.8l-12-12.39m-9.55-.65-5.44,4.78M23.55,34l3.31,13M36.29,1a6.91,6.91,0,1,1-6.91,6.91A6.91,6.91,0,0,1,36.29,1M15,18.66A7.68,7.68,0,1,1,7.3,26.34,7.68,7.68,0,0,1,15,18.66M36.37,5a2.75,2.75,0,1,1-2.75,2.75A2.75,2.75,0,0,1,36.37,5ZM15.12,23.75a2.75,2.75,0,1,1-2.75,2.75A2.75,2.75,0,0,1,15.12,23.75Z"/>
                            </svg>
                        </div>
                        <div class="description">
                            <span>Готовое коробочное решение SimpleArc</span>
                        </div>
                    </div>
                </div>
                <div class="advantages first swapTab" data-tab="tab2">
                    <div class="advaBlock block6 default" data-block="block6">
                        <div class="icon">
                            <svg viewBox="0 0 67.58 56.72">
                                <path d="M60.37,50.75l-1.5-1.5m8-6-1.5-1.5m-26.5,2.5-1.5-1.5M39.62,35l-1.5-1.5m17.78,11s3.26-3.26,7.33-2.44M49.91,53.4s2.3-4,6.45-4.26M45.14,43.82s0-4.61-3.46-6.91m2.67,15.93s0-4.61-3.46-6.91"/>
                                <path d="M0,55.72H64.51M52.42,29.8l-2.3,12.67,5.18-6.34,1.33-9.05M34.56,55.72V48.81a1.73,1.73,0,0,0-1.73-1.73H5.18a1.73,1.73,0,0,0-1.73,1.73v6.91m7.15-8.64L8.74,36M19,15.55,26.11,9.9m19.81,1.84L56.63,27.08,52.42,29.8l-12-12.39m-9.55-.65-5.44,4.78M23.55,34l3.31,13M36.29,1a6.91,6.91,0,1,1-6.91,6.91A6.91,6.91,0,0,1,36.29,1M15,18.66A7.68,7.68,0,1,1,7.3,26.34,7.68,7.68,0,0,1,15,18.66M36.37,5a2.75,2.75,0,1,1-2.75,2.75A2.75,2.75,0,0,1,36.37,5ZM15.12,23.75a2.75,2.75,0,1,1-2.75,2.75A2.75,2.75,0,0,1,15.12,23.75Z"/>
                            </svg>
                        </div>
                        <div class="description">
                            <span>Промышленный робот "KUKA"</span>
                        </div>
                    </div>
                    <div class="advaBlock block7" data-block="block7">
                        <div class="icon">
                            <svg viewBox="0 0 49.68 58.83">
                                <path d="M8.38,12c-.76.63-1.51,1.39-2.39,5.54A74,74,0,0,0,4.35,33.06c.13,5,1.64,15.55,2.58,17.06s1,1.78,2.45,2.48,1.5.89,2.88.52l.66-.16m4.95-.21c.81,0,2,.19,2.47.16m-9-37.85H35.7a.47.47,0,0,1,.47.47V48.28a.47.47,0,0,1-.47.47H11.29a.47.47,0,0,1-.47-.47V15.53A.47.47,0,0,1,11.29,15.06Z"/><path class="cls-1" d="M17.69,3.8a27.34,27.34,0,0,1,3.54.57c1.9.44,1.32,1.1,3.52.59s10-1.39,11.86-.81,2.27,3.08,2.56,4.39.51,2.49.81,2.64a2.27,2.27,0,0,0,1.76-.51,6.74,6.74,0,0,0-.22-3.3c-.37-1.68-.86-6-6.91-6.35M44.66,13.89l-3.51,1.17L42,23.7l3.88,1.39m0,0c0-.34,2.07.25,2.6-.58.23-.36.29-1.29-.19-4.33-.24-1.52-.11-2.79-.37-4.17-.16-.84-.34-1.8-1.32-2s-1.91.07-2-.15.42-.35,1-.55c1.42-.54,1.28-.92,1.16-1.54C46.51,10.48,46.23,9.23,46,8c-.73-3.51-1-3.66-1.76-4.54S40.85,1,32.94,1C26.56,1,11,4.78,5.7,9.2c-.88.73-1.76,1.61-2.78,6.44a86,86,0,0,0-1.9,18c.15,5.86,1.9,18.09,3,19.84a7.1,7.1,0,0,0,3.51,3.22c1.68.81,2.49,1.39,4.1,1,5.34-1.46,20.88-1.46,26.21,0,1.61.44,2.42-.15,4.1-1a7.1,7.1,0,0,0,3.51-3.22c1.1-1.76,2.86-14,3-19.84a60.67,60.67,0,0,0-.2-6.15c-.08-1,.09-.89-.79-1.4-.67-.38-1.56-.83-1.57-1M17.79,8.4A1.76,1.76,0,1,1,16,10.16,1.76,1.76,0,0,1,17.79,8.4ZM33.22,6.94a2.64,2.64,0,1,1-2.64,2.64,2.64,2.64,0,0,1,2.64-2.64ZM24.87,8.4a1.76,1.76,0,1,1-1.76,1.76A1.76,1.76,0,0,1,24.87,8.4Z"/>
                            </svg>
                        </div>
                        <div class="description">
                            <span>Программное обеспечение "ONEPAD"</span>
                        </div>
                    </div>
                    <div class="advaBlock block8" data-block="block8">
                        <div class="icon">
                            <svg viewBox="0 0 42.6 59.01">
                                <path d="M38.29,48.68l-17.44,1.5L6.71,45.68V32.44l.9-.9L6.7,31.3V7.78S8.07,3,8.22,2.52,10.47,1.17,14.68,1a24.56,24.56,0,0,1,7.22.75,29.25,29.25,0,0,1,7.37-.6c4.36.15,6.62.6,7.82,2.86s3.16,8.12,2.71,10.07-1.5,3.46-1.5,9V33.5l-.45.6.45.45V48.68m-17.44,1.5L20.7,34.7,20.4,8.23,6.7,7.78m.92,23.76L20.7,34.7l17.14-.6M21.9,1.77a5.75,5.75,0,0,0-1.35,1.8c-.6,1.2-1.05,2-1.05,2H9.42m13.68-2,12.48.6s.9.6,1.8,4.21a24.45,24.45,0,0,1,.9,4.81l-12.48.15s.75-3.76-2.71-9.77ZM25.81,14.7H38.74a6.29,6.29,0,0,0-1.2,2c-.15.75-.9,2.71-2.86,2.86s-7.07,0-8.12,0-2.11,0-1.35-1.65A6.78,6.78,0,0,0,25.81,14.7Z"/>
                                <path d="M6.71,44.77,1,45.08v6l10.91,4L17.08,57l2.87,1,5.34-.37m7.42-3.58.06-2.91m-4.7,1.5V51.4m8.85,3.94-4.21-1.28m-4.64-1.41-2.78-.84m11.63,5L25.29,53.31m0,4.33V51.54l11.65-.6,0,5.89m0,0,4.68-.33V49.59l-3.31-1.2"/>
                            </svg>
                        </div>
                        <div class="description">
                            <span>Сварочный источник TPS 400i</span>
                        </div>
                    </div>
                    <div class="advaBlock block9" data-block="block9">
                        <div class="icon">
                            <svg viewBox="0 0 62 41">
                                <path d="M61,41V33H1v8m60-4H1"/>
                                <path d="M38.93,33.89V25M3.86,14H23M3.86,22H23m-.07,11.67V6.3M3.84,33.69V6.31M57.93,33.7V24.81M38.93,18v6.09h19V18ZM3.93,1V6h19V1Z"/>
                            </svg>
                        </div>
                        <div class="description">
                            <span>Линейный модуль</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="wrapper wrapperProduction">
            <canvas id="productionCanvas"></canvas>
            <div class="slider">
                <div class="mobileManipulate">
                    <div class="arrow left" data-slide="left"></div>
                    <div class="arrow right" data-slide="right"></div>
                </div>
                <div class="item block1 tab1default">
                    <div class="wrap gradient">
                        <div class="description">
                            <div class="headTitle">
                                <h3>Широкие компетенции в роботах</h3>
                            </div>
                            <ul>
                                <li>Работаем с роботами KUKA, Fanuc, Yaskawa, Nachi, Kawasaki, ABB</li>
                                <li>Выбираем производителя роботов в соответствии с особенностями задачи</li>
                                <li>Расширяем стандартные возможности роботов</li>
                            </ul>
                        </div>
                        <div class="image">
                            <div>
                                <img src="<?php echo get_template_directory_uri(); ?>/img/advaLogo1.png" alt="">
                            </div>
                            <div>
                                <img src="<?php echo get_template_directory_uri(); ?>/img/advaLogo2.png" alt="">
                            </div>
                            <div>
                                <img src="<?php echo get_template_directory_uri(); ?>/img/advaLogo3.png" alt="">
                            </div>
                            <div>
                                <img src="<?php echo get_template_directory_uri(); ?>/img/advaLogo4.png" alt="">
                            </div>
                            <div>
                                <img src="<?php echo get_template_directory_uri(); ?>/img/advaLogo5.png" alt="">
                            </div>
                            <div>
                                <img src="<?php echo get_template_directory_uri(); ?>/img/advaLogo6.png" alt="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item block2">
                    <div class="wrap gradient">
                        <div class="description">
                            <div class="headTitle">
                                <h3>Интерактивный софт "OnePAD"</h3>
                                <div class="line"></div>
                                <p class="desc">Пульт робота как единый интерфейс для всего оборудования с интерактивной поддержкой: эксплуатация, наладка, программирование, обучение</p>
                            </div>
                            <ul>
                                <li>Управление роботом, позиционером, переферийным оборудованием</li>
                                <li>Документация в адаптированном виде</li>
                                <li>Мастер быстрого обучения</li>
                                <li>Панель сварочного источника на пульте робота, полное управление</li>
                            </ul>
                        </div>
                        <div class="image">
                            <img src="<?php echo get_template_directory_uri(); ?>/img/advaCont2.png" alt="">
                        </div>
                    </div>
                </div>
                <div class="item block3">
                    <div class="wrap gradient">
                        <div class="description nomr">
                            <div class="headTitle head-max-width">
                                <h3>Система мониторинга и документирования</h3>
                                <div class="line"></div>
                                <p class="desc">Система мониторинга и документирования работы автоматических систем и роботизированных комплексов</p>
                            </div>
                            <ul>
                                <li>Мониторинг состояния оборудования от датчиков до контроллеров</li>
                                <li>Документирование технологических процессов с генерацией отчетов</li>
                                <li>Экспорт статистики работы автоматических систем</li>
                                <li>Хранение информации на сервере с возможностью быстрого доступа к данным</li>
                                <li>Администрирование работы оборудования с различных устройств: ПК, планшет, смартфон</li>
                            </ul>
                        </div>
                        <div class="image">
                            <img class="bottom" src="<?php echo get_template_directory_uri(); ?>/img/advaCont3.png" alt="">
                        </div>
                    </div>
                </div>
                <div class="item block4">
                    <div class="wrap gradient">
                        <div class="description need-max-width">
                            <div class="headTitle head-max-width">
                                <h3>Адаптивная система слежения за импульсной дугой</h3>
                                <div class="line"></div>
                                <p class="desc">Система слежения за высотой дуги для импульсной TIG сварки</p>
                            </div>
                            <ul>
                                <li>Широкие возможности настройки</li>
                                <li>Различные алгоритмы обработки сигнала: слежение по нижнему, верхнему и усредненному значению импульса</li>
                                <li>Точности слежения +-0.3 мм</li>
                                <li>Независимость от производителя сварочного источника</li>
                            </ul>
                        </div>
                        <div class="image">
                            <img src="<?php echo get_template_directory_uri(); ?>/img/advaCont4.png" alt="">
                        </div>
                    </div>
                </div>
                <div class="item block5">
                    <div class="wrap gradient">
                        <div class="description need-max-width">
                            <div class="headTitle borderless">
                                <h3>Готовое коробочное решение SimpleArc</h3>
                                <div class="line"></div>
                                <p class="desc">Готовое решение с широкими возможностями по доступной цене</p>
                            </div>
                            <ul>
                                <li>Новое поколение роботов KUKA и сварочного оборудования Fronius</li>
                                <li>Решение для сварки 80% деталей в машиностроении</li>
                                <li>Легкое программирование и интерактивная поддержка на пульте робота</li>
                                <li>Короткий срок ввода в эксплуатацию</li>
                            </ul>
                        </div>
                        <div class="image">
                            <img src="<?php echo get_template_directory_uri(); ?>/img/advaCont5.png" alt="">
                        </div>
                    </div>
                </div>
                <div class="item block6 tab2default">
                    <div class="wrap gradient">
                        <div class="description">
                            <div class="headTitle">
                                <h3>Промышленный робот KUKA</h3>
                                <div class="line"></div>
                            </div>
                            <ul>
                                <li>Ведущий бренд производителей роботов</li>
                                <li>Самое современное поколение роботов</li>
                                <li>Надежен, удобен в эксплуатации, безопасен</li>
                                <li>Лучшая точность в классе 0,04мм</li>
                                <li>Превосходные динамические показатели</li>
                                <li>Функциональный дизайн запястья</li>
                                <li>Минимальное потребление энергии</li>
                            </ul>
                        </div>
                        <div class="image">
                            <img class="bottom" src="<?php echo get_template_directory_uri(); ?>/img/adva_cont-3.png" alt="">
                        </div>
                    </div>
                </div>
                <div class="item block7">
                    <div class="wrap gradient">
                        <div class="description">
                            <div class="headTitle">
                                <h3>Интерактивный софт "OnePAD"</h3>
                                <div class="line"></div>
                                <p class="desc">Пульт робота как единый интерфейс для всего оборудования с интерактивной поддержкой: эксплуатация, наладка, программирование, обучение</p>
                            </div>
                            <ul>
                                <li>Управление роботом, позиционером, переферийным оборудованием</li>
                                <li>Документация в адаптированном виде</li>
                                <li>Мастер быстрого обучения</li>
                                <li>Панель сварочного источника на пульте робота, полное управление</li>
                            </ul>
                        </div>
                        <div class="image">
                            <img src="<?php echo get_template_directory_uri(); ?>/img/advaCont2.png" alt="">
                        </div>
                    </div>
                </div>
                <div class="item block8">
                    <div class="wrap gradient">
                        <div class="description">
                            <div class="headTitle">
                                <h3>Сварочный источник TPS 400i</h3>
                                <div class="line"></div>
                            </div>
                            <ul>
                                <li>Полное взаимодействие компонентов с самодиагностикой и авто настройкой</li>
                                <li>Документирование сварочных параметров</li>
                                <li>Инновационный режим обучения точек без подгиба проволоки</li>
                                <li>Стабильность сварки в любых положениях</li>
                                <li>Широкий диапазон подстройки</li>
                                <li>Высокоскоростная сварка</li>
                                <li>Контроль короткого замыкания</li>
                                <li>Адаптивое зажигание дуги</li>
                            </ul>
                        </div>
                        <div class="image">
                            <img src="<?php echo get_template_directory_uri(); ?>/img/adva_cont.png" alt="">
                        </div>
                    </div>
                </div>
                <div class="item block9">
                    <div class="wrap gradient">
                        <div class="description">
                            <div class="headTitle">
                                <h3>Линейный модуль</h3>
                                <div class="line"></div>
                            </div>
                            <ul>
                                <li>Готовое решение для сварки 80% деталей в машиностроении</li>
                                <li>Подвижные каретки для позиционера и поддерживающей бабки</li>
                                <li>Двухосевой позиционер на 400кг</li>
                                <li>Максимальный вес детали при работе с поддерживающей бабкой 800 кг</li>
                            </ul>
                        </div>
                        <div class="image">
                            <img src="<?php echo get_template_directory_uri(); ?>/img/adva_cont-2.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="wrapper wrapperProfit">
            <canvas id="profitrolCanvas"></canvas>
            <header>
                <h3 class="orange"><span class="swapTab" data-tab="all">Выгода от внедрения</span></h3>
                <p><span class="swapTab" data-tab="all">Перевод на новый уровень развития производства</span></p>
            </header>
            <div class="advaWrap swapWrapper">
                <div class="advantages vigoda swapTab" data-tab="tab1">
                    <div class="advaBlock block1">
                        <div class="icon">
                            <img src="<?php echo get_template_directory_uri(); ?>/img/gotoverh.png" alt="">
                        </div>
                        <div class="desc">
                            <p>Рост объемов</p>
                        </div>
                    </div>
                    <div class="advaBlock block2">
                        <div class="icon">
                            <img src="<?php echo get_template_directory_uri(); ?>/img/medal_of_kokokonor.png" alt="">
                        </div>
                        <div class="desc">
                            <p>Обеспечение стабильного высокого качества готового изделия</p>
                        </div>
                    </div>
                    <div class="advaBlock block3">
                        <div class="icon">
                            <img src="<?php echo get_template_directory_uri(); ?>/img/meshokrubley.png" alt="">
                        </div>
                        <div class="desc">
                            <p>Повышение рентабельности производства</p>
                        </div>
                    </div>
                </div>
                <div class="advantages vigoda swapTab" data-tab="tab2">
                    <div class="advaBlock block1">
                        <div class="icon">
                            <img src="<?php echo get_template_directory_uri(); ?>/img/medal_of_kokokonor.png" alt="">
                        </div>
                        <div class="desc">
                            <p>Увеличение производительности и качества готового изделия</p>
                        </div>
                    </div>
                    <div class="advaBlock block2">
                        <div class="icon">
                            <img src="<?php echo get_template_directory_uri(); ?>/img/gotoverh.png" alt="">
                        </div>
                        <div class="desc">
                            <p>Увеличение прибыли за счет быстрой переналадки на новую продукцию и сокращения затрат на персонал, расходные материалы, брак</p>
                        </div>
                    </div>
                    <div class="advaBlock block3">
                        <div class="icon">
                            <img src="<?php echo get_template_directory_uri(); ?>/img/meshokrubley.png" alt="">
                        </div>
                        <div class="desc">
                            <p>Расширение номенклатуры изделия за в рамках одного</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="wrapper wrapperFaster">
            <canvas id="fasterCanvas"></canvas>
            <header>
                <h3 class="orange"><span class="swapTab" data-tab="all">Быстрота внедрения</span></h3>
                <p><span class="swapTab" data-tab="all">Производство получает работающее оборудование в коротские сроки</span></p>
            </header>
            <div class="cheto">
                <canvas id="chetoCanvas"></canvas>
                <div class="wrap">
                    <div class="icon icon1 swapTab" data-tab="tab2">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/icons_white-1.png" alt="">
                    </div>
                    <div class="icon icon2 swapTab" data-tab="tab2">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/icons_white-2.png" alt="">
                        <p>Проект готов до контракта</p>
                    </div>
                    <div class="icon icon3 swapTab" data-tab="tab2">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/icons_white-3.png" alt="">
                        <p>Закупка, сборка, упаковка, отправка</p>
                    </div>
                    <div class="icon icon4 swapTab" data-tab="tab2">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/icons_white-4.png" alt="">
                        <p>Запуск: <br>1 специалист <br>1 неделя</p>
                    </div>
                    <div class="icon icon5 swapTab" data-tab="tab2">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/icons_white-5.png" alt="">
                        <p>Оборудование работает, персонал обучен</p>
                    </div>
                    <div class="icon icon6 swapTab" data-tab="tab2">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/icons_white-6.png" alt="">
                    </div>


                    <div class="icon icon1 swapTab" data-tab="tab1">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/icons_white-1.png" alt="">
                    </div>
                    <div class="icon icon2 swapTab" data-tab="tab1">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/icons_white-2.png" alt="">
                        <p>Разработка по ТЗ</p>
                    </div>
                    <div class="icon icon3 swapTab" data-tab="tab1">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/icons_white-3.png" alt="">
                        <p>Закупка, сборка, упаковка, отправка</p>
                    </div>
                    <div class="icon icon4 swapTab" data-tab="tab1">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/icons_white-4.png" alt="">
                        <p>Пусконаладочные работы</p>
                    </div>
                    <div class="icon icon5 swapTab" data-tab="tab1">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/icons_white-5.png" alt="">
                        <p>Оборудование работает, пурсонал обучен</p>
                    </div>
                    <div class="icon icon6 swapTab" data-tab="tab1">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/icons_white-6.png" alt="">
                    </div>
                </div>
            </div>
        </div>
        <div class="wrapper wrapperReviews">
            <canvas id="reviewsCanvas"></canvas>
            <header>
                <h3 class="orange">Внедренные проекты</h3>
                <p>Проекты компании с которыми мы сотрудничаем</p>
            </header>
            <div class="reviews">
                <?php if(count($port) > 3) { ?>
                    <div class="prev"></div>
                    <div class="next"></div>
                <?php } ?>
                <div class="slider">
                    <?php foreach($port as $key => $post) { ?>
                        <?php
                            setup_postdata($post);
                            $permalink = get_the_permalink();
                            $thumb = get_the_post_thumbnail($post->ID, 'full', false, false);
                            $title = get_the_title();
                            $description = get_the_content();
                            $i = $key + 1;
                            $i = $i%3;
                        ?>
                        <div class="item <?php if($i == 1) echo 'left';
                        if($i == 2) echo 'center';
                        if($i == 0) echo 'right'; ?> visible">
                            <div class="wrapper">
                                <header><a href="<?php echo $permalink; ?>"><?php echo $thumb; ?></a></header>
                                <div class="description">
                                    <p><?php echo mb_strimwidth($title, 0, 100, "..."); ?></p>
                                </div>
                                <div class="signature">
                                    <a href="<?php echo $permalink; ?>" class="button orange">Подробнее</a>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                    <?php wp_reset_postdata(); ?>
                </div>
            </div>
        </div>
        <div class="wrapper wrapperAction">
            <canvas id="actionCanvas"></canvas>
            <header>
                <h3 class="orange">Консультация специалиста</h3>
                <p>Оставьте заявку, и мы свяжемся с вами в ближайшее время</p>
            </header>
            <form action="">
                <div class="wrapinput"><input type="text" name="name" placeholder="Имя"></div>
                <div class="wrapinput"><input type="tel" name="tel" placeholder="+7  (      )      -    -    " required></div>
                <input type="file" name="file">
                <button type="submit" class="button orange piu">Отправить</button>
            </form>
            <div class="lineAction"></div>
            <div class="man">
                <img src="<?php echo get_template_directory_uri(); ?>/img/vasya_obrez.png" alt="">
            </div>
            <div class="kuka">
                <img src="<?php echo get_template_directory_uri(); ?>/img/kuka_obrez.png" alt="">
            </div>
        </div>

<?php get_footer(); ?>
